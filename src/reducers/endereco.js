
import { CEP_SEARCH_SUCCESS, ERROR } from '../actions/endereco'

const initialState = {
    endereco: {

    },
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CEP_SEARCH_SUCCESS:
            return {...state, ...{endereco: action.payload}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        default:
            return state
    }
}
