
import { 
  SEARCH_SUCCESS, 
  HISTORICO_SHT_SUCCESS, 
  TERMOS_USO_SUCCESS,
  ERROR } from '../actions/content'

const initialState = {
    arquivos: [],
    historico_sht: [],
    termos_uso: null,
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_SUCCESS:
            return {...state, ...{arquivos: action.payload}}
        case HISTORICO_SHT_SUCCESS:
            return {...state, ...{historico_sht: action.payload}}
        case TERMOS_USO_SUCCESS:
            return {...state, ...{termos_uso: action.payload}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        default:
            return state
    }
}
