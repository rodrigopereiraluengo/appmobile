
import { SHOW, SELECT } from '../actions/picker-list'

const initialState = {
    title: 'Selecione uma opção',
    items: [],
    selectedValue: null,
    onValueChange: () => { }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SHOW:
            return {...state, ...action.payload}
        case SELECT:
            return {...state, ...{selectedValue: action.payload}}
        default:
            return state
    }
}
