
import {
    SCROLL,
    ONLINE,
    OFFLINE,
    HTTP_REQUEST_BEGIN,
    HTTP_REQUEST_END,
    IMAGE_ONLOADSTART,
    IMAGE_ONLOADEND,
    STATE_CHANGE
} from '../actions/system'

const initialState = {
    scroll: null,
    offline: false,
    loading: false,
    imageOnLoadStart: 0,
    state: null
}

export default function (state = initialState, action) {

    let imageOnLoadStart

    switch (action.type) {
        case SCROLL:
            return {...state, scroll: action.payload}
        case ONLINE:
            return {...state, offline: false}
        case OFFLINE:
            return {...state, offline: true}
        case HTTP_REQUEST_BEGIN:
            return {...state, loading: true}
        case HTTP_REQUEST_END:
            return {...state, loading: false}
        case IMAGE_ONLOADSTART:
            imageOnLoadStart = state.imageOnLoadStart + 1
            return {...state, imageOnLoadStart, ...{loading: true}}
        case IMAGE_ONLOADEND:
            imageOnLoadStart = state.imageOnLoadStart - 1
            return {...state, imageOnLoadStart, ...{loading: imageOnLoadStart > 0}}
        case STATE_CHANGE:
            return {...state, state: action.payload}
        default:
            return state
    }
}

