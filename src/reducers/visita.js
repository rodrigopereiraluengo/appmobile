
import { ADD, CHANGED, ERROR, SAVE_SUCCESS } from '../actions/visita'

const initialState = {
    visita: {
        creci: null,
        empreendimento_id: null,
        empreendimento_nome: '',
        data_visita: null,
        cliente_nome: null,
        cliente_rg: null,
        cliente_cpf: null,
        msg: null,
    },
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD:
            const { empreendimento_id, empreendimento_nome, msg } = action.payload
            return {...state, ...{visita: {...state.visita, ...{empreendimento_id, empreendimento_nome, msg}}}}
        case CHANGED:
            return {...state, ...{visita: {...state.visita, ...action.payload}}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case SAVE_SUCCESS:
            return initialState
        default:
            return state
    }
}
