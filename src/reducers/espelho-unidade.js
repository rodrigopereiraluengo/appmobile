
import {
    CHANGED_PARAMETERS,
    SEARCH,
    SEARCH_SUCCESS,
    ERROR
} from '../actions/espelho-unidade'

import EmptyDataSource from '../resources/empty_data_source'

const initialState = {
    parameters: {
        bloco_id: null
    },
    unidades: EmptyDataSource,
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGED_PARAMETERS:
        case SEARCH:
            return {...state, ...{parameters: {...state.parameters, ...action.payload}}}
        case SEARCH_SUCCESS:
            if (action.payload.current_page > 1) {
                action.payload.data = state.unidades.data.concat(action.payload.data)
            }
            return {...state, ...{unidades: action.payload}}
        default:
            return state
    }
}
