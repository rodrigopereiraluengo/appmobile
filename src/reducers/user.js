import _ from 'lodash'

import {
    ADD,
    CHANGED,
    SAVE_SUCCESS,
    ERROR,
    IMOBILIARIAS_SUCCESS,
    EDIT,
    NOTIFICATIONS,
    NOTIFICATIONS_SUCCESS,
    NOTIFICATION_TOTAL_SUCCESS, REGIAO_ARQUIVOS_SUCCESS
} from '../actions/user'
import empty_data_source from '../resources/empty_data_source'

const initialState = {
    user: {
        id: null,
        name: null,
        sobrenome: null,
        genero: null,
        uf: null,
        creci_uf: null,
        tipocadastro: null,
        id_imobiliaria: null,
        cidades_atuacao: null,
        corretor_resp: null,
        por_onde_nos_conheceu: null,
        nascimento: null,
        razaosocial: null,
        cnpj: null,
        email: null,
        senha: null,
        rg: null,
        cpf: null,
        telefone: null,
        celular: null,
        cep: null,
        endereco: null,
        bairro: null,
        cidade: null,
        numero: null,
        complemento: null,
        creci: null,
        state: null,
        asset_id: null,
        ordering: null,
        data_registro_sistema: null,
        novo_registro: null,
        precadastro: null,
        data_update: null,
        foto: null,
        pertence_imobiliaria: false,
        termos_uso_aceito: false
    },
    imobiliarias: [],
    notifications: {
        q: null,
        data: empty_data_source
    },
    total_notifications: 0,
    regiao_arquivos: null,
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD:
            return initialState
        case EDIT:
            const user = {...action.payload}
            user.pertence_imobiliaria = (user.tipocadastro === 'corretor' || user.tipocadastro === 'estagiario') && _.isEmpty(user.razaosocial) === false
            user.email_confirmation = user.email
            return {...state, ...{user, errors: {}}}
        case CHANGED:
            return {...state, ...{user: {...state.user, ...action.payload}}}
        case SAVE_SUCCESS:
            return {...state, ...{user: {...state.user, ...action.payload}, errors: {}}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case IMOBILIARIAS_SUCCESS:
            return {...state, ...{imobiliarias: action.payload}}
        case NOTIFICATIONS:
            return {...state, ...{notifications: {...state.notifications, ...action.payload}}}
        case NOTIFICATIONS_SUCCESS:
            if (action.payload.current_page > 1) {
                action.payload.data = state.notifications.data.concat(action.payload.data)
            }
            return {...state, ...{notifications: {...state.notifications, ...{data: action.payload}}}}
        case NOTIFICATION_TOTAL_SUCCESS:
            return {...state, ...{total_notifications: action.payload.total}}
        case REGIAO_ARQUIVOS_SUCCESS:
            return {...state, ...{regiao_arquivos: action.payload.arquivos}}
        default:
            return state
    }
}
