
import { SEARCH_SUCCESS, ERROR } from '../actions/regiao'

const initialState = {
    regioes: [],
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_SUCCESS:
            return {...state, ...{regioes: action.payload}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        default:
            return state
    }
}
