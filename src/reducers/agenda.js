
import moment from 'moment-timezone'

import EmptyDataSource from '../resources/empty_data_source'

import {
    SEARCH,
    SEARCH_SUCCESS,
    ERROR,
    ADD,
    EDIT,
    CHANGED,
    SAVE,
    SAVE_SUCCESS,
    REMOVE_SUCCESS,
    DIAS_SUCCESS,
    DIAS
} from '../actions/agenda'

const initialState = {
    parameters: {
        date: null,
        query: '',
        day: null,
    },
    agenda: {
        id: null,
        empreendimento_id: null,
        titulo: null,
        data_de: null,
        data_ate: null,
        comentarios: null
    },
    dias: [],
    compromissos: EmptyDataSource,
    searched: false,
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case DIAS:

            let date = action.payload
            let day = null

            if (!date) {
                date = moment().format('YYYY-MM-DD')
                day = date
            }

            return {...state, ...{parameters: {...state.parameters, date, day}}}
        case DIAS_SUCCESS:
            return {...state, ...{dias: action.payload, searched: true}}
        case SEARCH:
            return {...state, ...{parameters: {...state.parameters, ...action.payload}}}
        case SEARCH_SUCCESS:
            return {...state, ...{compromissos: action.payload, searched: true}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case ADD:
        case REMOVE_SUCCESS:
            return {...state, ...{agenda: initialState.agenda, errors: {}}}
        case EDIT:
            return {...state, ...{agenda: action.payload, errors: {}}}
        case CHANGED:
        case SAVE:
        case SAVE_SUCCESS:
            return {...state, ...{agenda: {...state.agenda, ...action.payload}}}
        default:
            return state
    }
}
