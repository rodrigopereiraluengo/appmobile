
import { ADD, CHANGED, ERROR, SAVE_SUCCESS } from '../actions/proposta'

const initialState = {
    proposta: {
        empreendimento_id: null,
        empreendimento_nome: '',
        valor: null,
        cliente_nome: null,
        cliente_rg: null,
        cliente_cpf: null,
        assunto: null,
        msg: null,
    },
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD:
            const {empreendimento_id, empreendimento_nome} = action.payload
            return {...state, ...{proposta: {...state.proposta, ...{empreendimento_id, empreendimento_nome}}}}
        case CHANGED:
            return {...state, ...{proposta: {...state.proposta, ...action.payload}}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case SAVE_SUCCESS:
            return initialState
        default:
            return state
    }
}
