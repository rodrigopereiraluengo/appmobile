
import {
    LOGIN,
    TOKEN_REFRESH,
    LOGIN_SUCCESS,
    ERROR,
    CHANGED,
    LOGOUT,
    UPDATE_USER
} from '../actions/auth'

const initialState = {
    authenticated: false,
    auth: {
        token: null,
        user: {
            email: null,
            password: null
        }
    },
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGED:
        case LOGIN:
            return {...state, ...{auth: {...state.auth, ...{user: {...state.auth.user, ...action.payload}}}}}
        case LOGIN_SUCCESS:
            const password = state.auth.user.password
            let auth = action.payload
            auth.user.password = password
            return {...state, ...{authenticated: true, auth}, errors: {}}
        case TOKEN_REFRESH:
            return {...state, ...{authenticated: true, auth: {...state.auth, token: action.payload}}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case LOGOUT:
            return {...state, ...{authenticated: false, auth: {...state.auth, token: null, user: {
                email: state.auth.user.email,
                password: state.auth.user.password,
            }}}}
        case UPDATE_USER:
            const user = action.payload.data
            user.password = user.senha ? user.senha : state.auth.user.password
            auth = {...state.auth, user}
            return {...state, auth}
        default:
            return state
    }
}
