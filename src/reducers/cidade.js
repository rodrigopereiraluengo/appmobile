
import { SEARCH_SUCCESS, ERROR } from '../actions/cidade'

const initialState = {
    cidades: [],
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_SUCCESS:
            return {...state, ...{cidades: action.payload}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        default:
            return state
    }
}
