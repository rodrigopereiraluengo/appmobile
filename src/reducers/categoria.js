
import { SEARCH_SUCCESS, ERROR } from '../actions/categoria'

const initialState = {
    categorias: [],
    errors: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_SUCCESS:
            return {...state, ...{categorias: action.payload}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        default:
            return state
    }
}
