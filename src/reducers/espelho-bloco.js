
import { SELECT } from '../actions/espelho-bloco'

const initialState = {
    bloco: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SELECT:
            return {...state, ...{bloco: action.payload}}
        default:
            return state
    }
}
