
import {
    CHANGED,
    CHANGED_PARAMETERS,
    SEARCH,
    SEARCH_SUCCESS,
    ERROR,
    GALLERY,
    SELECT,
    SELECT_SUCCESS,
    FILTRO_SUCCESS,
    EMPREENDIMENTOS_SUCCESS,
    SEARCH_TABELAS_SUCCESS, SEARCH_TABELAS
} from '../actions/empreendimento'

import EmptyDataSource from '../resources/empty_data_source'

const initialState = {
    parameters: {
        query: '',
        queryTabelas: '',
        valor_min: null,
        valor_max: null,
        area_util_min: null,
        area_util_max: null,
        categoria: null,
        cidade: null
    },
    searched: false,
    searchedTabelas: false,
    empreendimentos: EmptyDataSource,
    empreendimentoList: [],
    empreendimento: {},
    filtros: {
        fase_construcao: [],
        dormitorio: [],
        suite: [],
        vaga: [],
    },
    errors: {},
    gallery: {
        name: '',
        photos: []
    },
    tabelas: EmptyDataSource,
    cidade: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGED:
            return {...state, ...action.payload}
        case CHANGED_PARAMETERS:
            return {...state, ...{parameters: {...state.parameters, ...action.payload}}}
        case SEARCH:
            return {...state, ...{parameters: action.payload}}
        case SEARCH_SUCCESS:
            if (action.payload.meta.current_page > 1) {
                action.payload.data = state.empreendimentos.data.concat(action.payload.data)
            }
            return {...state, ...{empreendimentos: action.payload, searched: true}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case SELECT:
            return {...state, ...{empreendimento: action.payload}}
        case SELECT_SUCCESS:
            return {...state, ...{empreendimento: action.payload}}
        case GALLERY:
            return {...state, ...{gallery: action.payload}}
        case FILTRO_SUCCESS:
            return {...state, ...{filtros: action.payload}}
        case EMPREENDIMENTOS_SUCCESS:
            return {...state, ...{empreendimentoList: action.payload}}
        case SEARCH_TABELAS_SUCCESS:
            if (action.payload.meta.current_page > 1) {
                action.payload.data = state.tabelas.data.concat(action.payload.data)
            }
            return {...state, ...{tabelas: action.payload, searchedTabelas: true}}
        default:
            return state
    }
}
