
import {
    CHANGED_PARAMETERS,
    SEARCH,
    SEARCH_SUCCESS,
    ERROR,
    SELECT,
} from '../actions/espelho-empreendimento'

import EmptyDataSource from '../resources/empty_data_source'

const initialState = {
    parameters: {
        query: ''
    },
    empreendimentos: EmptyDataSource,
    empreendimento: {},
    errors: {},
    searched: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGED_PARAMETERS:
            return {...state, ...{parameters: {...state.parameters, ...action.payload}}}
        case SEARCH:
            return {...state, ...{parameters: action.payload}}
        case SEARCH_SUCCESS:
            if (action.payload.current_page > 1) {
                action.payload.data = state.empreendimentos.data.concat(action.payload.data)
            }
            return {...state, ...{empreendimentos: action.payload, searched: true}}
        case ERROR:
            return {...state, ...{errors: action.payload}}
        case SELECT:
            return {...state, ...{empreendimento: action.payload}}
        default:
            return state
    }
}
