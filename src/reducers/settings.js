
import { GET_SUCCESS, ERROR } from '../actions/settings';

const initialState = {
  settings: {
    helbor_botoes: false
  },
  errors: {}
}

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_SUCCESS:
      return { ...state, ...{ settings: action.payload } }
    case ERROR:
      return { ...state, ...{ errors: action.payload } }
    default:
      return state
  }
}
