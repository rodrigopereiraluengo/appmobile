import { combineReducers } from 'redux'
import system from './system'
import auth from './auth'
import cidade from './cidade'
import categoria from './categoria'
import empreendimento from './empreendimento'
import espelhoEmpreendimento from './espelho-empreendimento'
import espelhoBloco from './espelho-bloco'
import espelhoUnidade from './espelho-unidade'
import visita from './visita'
import proposta from './proposta'
import agenda from './agenda'
import user from './user'
import regiao from './regiao'
import endereco from './endereco'
import pickerList from './picker-list'
import content from './content'
import settings from './settings'

export default combineReducers({
  system,
  auth,
  cidade,
  categoria,
  empreendimento,
  espelhoEmpreendimento,
  espelhoBloco,
  espelhoUnidade,
  visita,
  proposta,
  agenda,
  user,
  regiao,
  endereco,
  pickerList,
  content,
  settings
})
