import React from 'react'
import { Alert as RNAlert } from 'react-native'
import { Appearance } from 'react-native-appearance'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Default from '../../ui/Default'
import {Button, Form, Input, Item, Label, Text} from 'native-base'

import {Col, Grid, Row} from 'react-native-easy-grid'

import {
    changed,
    dias as searchDias,
    ERROR,
    remove,
    REMOVE_SUCCESS,
    save,
    SAVE_SUCCESS,
    search
} from '../../../actions/agenda'
import moment from 'moment-timezone'
import DateTimePicker from 'react-native-modal-datetime-picker'
import store from '../../../store'
import Alert from "../../ui/Alert"
import HeaderDefault from "../../ui/HeaderDefault"
import FooterDefault from "../../ui/FooterDefault"
import Contato from "../../ui/Contato"
import Separator from "../../ui/Separator"
import {styleColumnFirst, styleColumnSecond} from "../../../theme/corretoron"

import {CompromissoStyle as style} from "../../../styles"
import ImageBackground from "../../ui/ImageBackground";

class Compromisso extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            dateTimeVisible: false,
            dateTimeValue: moment().toDate(),
            dateTimeMode: 'date',
            dateTimeOnConfirm: () => {}
        }

        this.remove = this.remove.bind(this)

        this.isDarkModeEnabled = Appearance.getColorScheme() === 'dark'
    }

    componentDidMount() {
        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case SAVE_SUCCESS:
                    Alert.success('Compromisso salvo com sucesso.')
                    this.props.searchDias()
                    this.props.navigation.goBack()
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break
                case REMOVE_SUCCESS:
                    Alert.success('Compromisso excluido com sucesso.')
                    this.props.searchDias()
                    this.props.navigation.goBack()
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    showDateTimePicker({dateTimeValue, dateTimeMode, dateTimeOnConfirm}) {

        if (typeof dateTimeValue === 'string') {
            dateTimeValue = moment(dateTimeValue).toDate()
        }

        const now = moment().add(30, 'minutes')
        now.minutes(Math.ceil(now.minutes()/5)*5)

        this.setState({
            dateTimeVisible: true,
            dateTimeValue: dateTimeValue ? dateTimeValue : now.toDate(),
            dateTimeMode,
            dateTimeOnConfirm: value => {
                dateTimeOnConfirm(moment(value).format('YYYY-MM-DD HH:mm'))
                this.setState({dateTimeVisible: false})
            }
        })
    }

    remove(agenda) {
        RNAlert.alert(
            'Compromisso',
            'Deseja realmente excluir esse compromisso?',
            [
                {
                    text: 'Cancelar',
                    onPress: () => {  },
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => { this.props.remove(agenda) }},
            ],
            {cancelable: false},
        )
    }

    render () {

        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goBack />
                ]}
                footer={[
                    <FooterDefault />
                ]}>

                <Separator title={this.props.agenda.id ? 'EDITAR COMPROMISSO' : 'NOVO COMPROMISSO'} />

                <Form>

                    <Label>Título</Label>
                    <Item
                        regular
                        invalidated={'titulo' in this.props.errors}>
                        <Input
                            value={this.props.agenda.titulo}
                            onChangeText={titulo => this.props.changed({titulo})}
                        />
                    </Item>

                    <Grid>
                        <Col style={styleColumnFirst}>
                            <Label>Data de</Label>
                            <Item
                                regular
                                invalidated={'data_de' in this.props.errors}
                                onPress={() => this.showDateTimePicker({
                                    dateTimeMode: 'date',
                                    dateTimeValue: this.props.agenda.data_de,
                                    dateTimeOnConfirm: data_de => this.props.changed({data_de})
                                })}>
                                <Text staticinput>{this.props.agenda.data_de ? moment(this.props.agenda.data_de, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY') : 'Selecione...'}</Text>

                            </Item>
                        </Col>
                        <Col style={styleColumnSecond}>
                            <Label>Hora de</Label>
                            <Item
                                regular
                                invalidated={'data_de' in this.props.errors}
                                onPress={() => this.showDateTimePicker({
                                    dateTimeMode: 'time',
                                    dateTimeValue: this.props.agenda.data_de,
                                    dateTimeOnConfirm: data_de => this.props.changed({data_de})
                                })}>
                                <Text staticinput>{this.props.agenda.data_de ? moment(this.props.agenda.data_de, 'YYYY-MM-DD HH:mm').format('HH:mm') : 'Selecione...'}</Text>
                            </Item>
                        </Col>
                    </Grid>

                    <Grid>
                        <Col style={styleColumnFirst}>
                            <Label>Data até</Label>
                            <Item
                                regular
                                invalidated={'data_de' in this.props.errors}
                                onPress={() => this.showDateTimePicker({
                                    dateTimeMode: 'date',
                                    dateTimeValue: this.props.agenda.data_ate,
                                    dateTimeOnConfirm: data_ate => this.props.changed({data_ate})
                                })}>
                                <Text staticinput>{this.props.agenda.data_ate ? moment(this.props.agenda.data_ate, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY') : 'Selecione...'}</Text>
                            </Item>
                        </Col>
                        <Col style={styleColumnSecond}>
                            <Label>Hora até</Label>
                            <Item
                                regular
                                invalidated={'data_ate' in this.props.errors}
                                onPress={() => this.showDateTimePicker({
                                    dateTimeMode: 'time',
                                    dateTimeValue: this.props.agenda.data_ate,
                                    dateTimeOnConfirm: data_ate => this.props.changed({data_ate})
                                })}>
                                <Text staticinput>{this.props.agenda.data_ate ? moment(this.props.agenda.data_ate, 'YYYY-MM-DD HH:mm').format('HH:mm') : 'Selecione...'}</Text>
                            </Item>
                        </Col>
                    </Grid>


                    <Label>Comentários</Label>
                    <Item
                        regular
                        invalidated={'comentarios' in this.props.errors}>

                        <Input
                            multiline={true}
                            numberOfLines={8}
                            value={this.props.agenda.comentarios}
                            onChangeText={comentarios => this.props.changed({comentarios})}
                        />
                    </Item>



                    { this.props.agenda.id === null ? <Button style={[style.Button, style.ButtonLeft]}
                        onPress={() => { this.props.save(this.props.agenda) }}>
                        <Text>{this.props.agenda.id ? 'ATUALIZAR' : 'AGENDAR'}</Text>
                    </Button> : <Grid>
                        <Row>
                            <Col>
                                <Button
                                    block
                                    style={[style.Button, style.ButtonLeft]}
                                    onPress={() => { this.props.save(this.props.agenda) }}>
                                    <Text>{this.props.agenda.id ? 'ATUALIZAR' : 'AGENDAR'}</Text>
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    style={[style.Button, style.ButtonRight]}
                                    danger
                                    block
                                    onPress={() => { this.remove(this.props.agenda) }}>
                                    <Text>EXCLUIR</Text>
                                </Button>
                            </Col>
                        </Row>
                    </Grid>}

                </Form>

                <DateTimePicker
                    isVisible={this.state.dateTimeVisible}
                    mode={this.state.dateTimeMode}
                    onConfirm={this.state.dateTimeOnConfirm}
                    onCancel={() => this.setState({dateTimeVisible: false})}
                    date={this.state.dateTimeValue}
                    isDarkModeEnabled={this.isDarkModeEnabled}
                />

            </Default>
        )

    }

}

const mapStateToProps = state => ({
    agenda: state.agenda.agenda,
    errors: state.agenda.errors
})

const mapDispatchToProps = dispatch => bindActionCreators({
    changed,
    save,
    search,
    searchDias,
    remove
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Compromisso)
