import React, { useEffect } from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Left, List, ListItem, Right, Text, View } from 'native-base'

import moment from 'moment-timezone'

import { Calendar, LocaleConfig } from 'react-native-calendars'
import Default from '../../ui/Default'

import { add, ADD, dias as diasSearch, DIAS_SUCCESS, EDIT, edit, search } from '../../../actions/agenda'
import { add as addVisita } from '../../../actions/visita'

import store from '../../../store'

import {
    calendarYellow,
    darkColor,
    darkMonoColor,
    fontFamily,
    fontFamilyBold,
    fontSize,
    intermediateColor, lightColor,
    lightMonoColor
} from '../../../theme/corretoron'

import { AgendaStyle as style } from '../../../styles'

import Separator from '../../ui/Separator'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import ImageBackground from "../../ui/ImageBackground";

LocaleConfig.locales['pt-BR'] = {
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['D','S','T','Q','Q','S','S'],
    today: 'Hoje'
}
LocaleConfig.defaultLocale = 'pt-BR'

function CompromissoList(props) {

    return (
        <ListItem button onPress={ () => { props.edit(props) } } style={style.ListItem}>
            <Left>
                <Text style={[{fontWeight: 'bold'}, props.textDisabled]}>{props.day === null ? props.hora_de : 'Dia '.concat(props.day, ' às ', props.hora_de)}</Text>
                <Text style={props.textDisabled}> - {props.titulo}</Text>
            </Left>
            <Right>
                <Text style={props.textDisabled}>...</Text>
            </Right>
        </ListItem>
    )
}

class AgendaCompromisso extends React.Component {

    constructor(props) {
        super(props)
        this._calendar = null
    }

    componentDidMount() {
        this.unsubscribe = store.subscribe(action => {
            switch (action.type) {
                case ADD:
                case EDIT:
                    this.props.navigation.navigate('Compromisso')
                    break
                case DIAS_SUCCESS:
                    this.props.search({page: null, ...this.props.parameters})
                    break
            }
        })

        this.props.diasSearch()
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    onScrollEnd() {
        if (this.props.compromissos.next_page_url) {
            const page = this.props.compromissos.current_page + 1
            this.props.search({page})
        }
    }

    markedDates() {

        const markedDates = {}

        const dias = this.props.dias.reduce((obj, item) => {
            return {
                ...obj,
                [item['data_ate']]: {
                    customStyles: {
                        container: {
                            backgroundColor: calendarYellow,
                            elevation: 2
                        },
                        text: {
                            color: lightMonoColor
                        }
                    }
                },
            }
        }, markedDates)

        dias[this.props.parameters.day] =  {customStyles: {
                container: {
                    backgroundColor: intermediateColor,
                    elevation: 2
                },
                text: {
                    color: lightMonoColor
                }
            }
        }

        const date = moment(this.props.parameters.date)
        let sunday = date.clone().day(1).day('Sunday').add(-1, 'w')
        let lastSunday = date.endOf('month').day('Sunday')

        do {
            let day = sunday.format('YYYY-MM-DD')
            if (!(day in dias)) {
                dias[day] =  {customStyles: {
                        text: {
                            color: calendarYellow
                        }
                    }
                }
            }
        } while ((sunday = sunday.add(1, 'w')).isBefore(lastSunday))

        return dias
    }

    onMonthChange (month) {
        this.props.diasSearch(month.dateString)
    }

    onDayPress (day) {

        this.props.search({...this.props.parameters, page: null, day: day.dateString})
    }

    render () {
        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goHome />
                ]}
                footer={[
                    <FooterDefault/>
                ]}
                onScrollEnd={this.onScrollEnd.bind(this)}>

                <Separator
                    title='AGENDA'
                    button='Adicionar compromisso'
                    onPress={ () => this.props.add() }
                    titleWidth='50%'
                    buttonWidth='50%'
                />

                <Calendar
                    current={this.props.parameters.date}
                    markedDates={this.markedDates()}
                    markingType={'custom'}
                    onDayPress={this.onDayPress.bind(this)}
                    monthFormat={'MMM \'de\' yyyy'}
                    onMonthChange={this.onMonthChange.bind(this)}
                    ref={element => this._calendar = element}
                    theme={{
                        backgroundColor: 'transparent',
                        calendarBackground: 'transparent',
                        arrowColor: darkColor,
                        disabledArrowColor: '#d9e1e8',
                        textSectionTitleColor: darkColor,
                        dayTextColor: lightColor,
                        textDayFontFamily: fontFamily,
                        textMonthFontFamily: fontFamilyBold,
                        textDayHeaderFontFamily: fontFamilyBold,
                        textDayFontSize: fontSize,
                        textMonthFontSize: fontSize,
                        textDayHeaderFontSize: fontSize
                    }}
                />

                {this.props.compromissos.data.length === 0 ? <Text registerNotFound>{this.props.searched ? 'Nenhum compromisso encontrato.' : ''}</Text> :
                    <React.Fragment>
                        <View style={style.ViewLine} />
                        <Text title style={style.TextCompromissoDia}>COMPROMISSOS{this.props.parameters.day ? ' DO DIA '.concat(moment(this.props.parameters.day, 'YYYY-MM-DD').format('DD')) : ''}</Text>
                        <List style={style.List}>
                        {this.props.compromissos.data.map(compromisso => {

                                const data_de = moment(compromisso.data_de, 'YYYY-MM-DD HH:mm')
                                const data_ate = moment(compromisso.data_ate, 'YYYY-MM-DD HH:mm')

                                const data = data_de.format('DD/MM/YYYY') === data_ate.format('DD/MM/YYYY') ?
                                    data_de.format('DD/MM/YYYY').concat(' ', data_de.format('HH:mm'), ' - ', data_ate.format('HH:mm')) :
                                    data_de.format('DD/MM/YYYY').concat(' ', data_de.format('HH:mm'), ' - ', data_ate.format('DD/MM/YYYY'), ' ', data_ate.format('HH:mm'))

                                const textDisabled = compromisso.done ? style.textDisabled : {}

                                return <CompromissoList {...compromisso}
                                                        day={this.props.parameters.day === null ? data_de.format('DD') : null}
                                                        key={compromisso.id}
                                                        edit={this.props.edit}
                                                        textDisabled={textDisabled}
                                                        data={data}
                                                        titulo={compromisso.titulo}
                                />
                            }
                        )}
                        </List>
                    </React.Fragment>}

            </Default>
        )
    }
}

const mapStateToProps = state => ({
    dias: state.agenda.dias,
    parameters: state.agenda.parameters,
    compromissos: state.agenda.compromissos,
    searched: state.agenda.searched
})

const mapDispatchToProps = dispatch => bindActionCreators({
    diasSearch,
    search,
    add,
    edit,
    addVisita
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AgendaCompromisso)
