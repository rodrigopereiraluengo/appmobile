import React, { useEffect, useState } from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import moment from 'moment/min/moment-with-locales'

import { notifications, notification_action, total_notifications, NOTIFICATIONS_SUCCESS } from '../../../actions/user'
import { detail } from '../../../actions/empreendimento'

import store from '../../../store'

import { withNavigation } from 'react-navigation'

import { List, ListItem, Text, Icon } from 'native-base'
import { Col, Grid, Row } from 'react-native-easy-grid'

import Default from '../../ui/Default'
import HeaderDefault from '../../ui/HeaderDefault'
import HeaderSearch from '../../ui/HeaderSearch'
import FooterDefault from '../../ui/FooterDefault'
import Separator from '../../ui/Separator'
import Contato from '../../ui/Contato'

import { NotificationSearchStyle as style } from '../../../styles'
import Navigation from "../../../services/Navigation";
import { Linking } from "react-native";

moment.locale('pt-BR')

function Search (props) {

    const { search, q } = props

    const [searched, setSearched] = useState(false)
    
    useEffect(() => {

        search({q, page: 1})

        const subscribe = store.subscribe(action => {
            if (action.type === NOTIFICATIONS_SUCCESS) {
                setSearched(true)
            }
        })

        return subscribe
    }, [search, q])
 
    const onScrollEnd = () => {
        if (props.notifications.next_page_url) {
            const page = props.notifications.current_page + 1
            search({q, page})
        }
    }

    const handleNotification = notification => {

        props.notification_action(notification)
        if ('data' in notification && 'action' in notification.data && notification.data.action) {
            switch (notification.data.action) {
                case 'screen':
                    return Navigation.navigate(notification.data.screen)
                case 'link':
                    return Linking.openURL(notification.data.link)
                default:
                    return setTimeout(() => props.detail(notification.data.imovel_id), 500)
            }
        }

    }

    return (
        <Default
            header={[
                <HeaderDefault goHome />,
                <HeaderSearch value={q} onChangeText={q => props.search({q, page: 1})} />
            ]}

            footer={[
                <FooterDefault />,
                <Contato />
            ]}

            onScrollEnd={onScrollEnd}>

            <Separator title='NOTIFICAÇÕES' />
            { props.notifications.data.length === 0 ?
                <Text registerNotFound>{searched ? 'Desculpe, mas não há resultados para sua pesquisa.' : ''}</Text> :
                <List>
                    {props.notifications.data.map((notification, i) => (
                        <ListItem expanded key={i} button onPress={ () => { handleNotification(notification) } }>
                            <Grid>
                                <Row>
                                    <Col  style={style.ColIcon}>
                                        { notification.notification_status !== 'actioned' && <Icon
                                            type="FontAwesome" 
                                            name="circle" 
                                            style={style.Icon} /> }
                                    </Col>
                                    <Col  style={style.ColLeft}>
                                        <Grid>
                                            <Row>
                                                <Col>
                                                    <Text style={style.TextTitle}>{notification.title}</Text>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Text style={style.TextBody}>{notification.body}</Text>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </Col>
                                    <Col style={style.ColRight}>
                                        <Text style={[style.TextDate]}>
                                            {formatDate(notification.notification_ok_date, notification.notification_received_date)}
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </ListItem>
                    ))}
                </List>
            }

        </Default>
    )
}

function formatDate (notification_ok_date, notification_received_date) {

    const now = moment(), date = moment(notification_received_date ?? notification_ok_date, 'YYYY-MM-DD HH:mm:ssSS')
    
    if (now.isSame(date, 'day')) {
        return date.format('HH:mm')
    }

    if (now.subtract(1, 'day').isSame(date, 'day')) {
        return 'Ontem'
    }

    if (now.isSame(date, 'week')) {
        return date.format('dddd')
    }

    if (now.isSame(date, 'year')) {
        return date.format('DD/MMM')
    }
    
    return date.format('MMM/YYYY')
}

const mapStateToProps = state => ({
    notifications: state.user.notifications.data,
    q: state.user.notifications.q
})

const mapDispatchToProps = dispatch => bindActionCreators({
    search: notifications,
    notification_action,
    total_notifications,
    detail
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Search))
