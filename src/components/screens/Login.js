import React from 'react'

import {Alert as RNAlert, Animated, TouchableOpacity} from 'react-native'
import {Button, Container, Content, Form, Icon, Input, Item, Text} from 'native-base'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import store from '../../store'
import {
  changed, 
  login,
  forgotPassword,
  LOGIN_SUCCESS, 
  UNAUTHORIZED, 
  ERROR, 
  RESET_PASSWORD_SUCCESS
} from '../../actions/auth'
import {add} from '../../actions/user'

import ImageBackground from '../ui/ImageBackground'

import Splash from '../ui/Splash'
import Alert from '../ui/Alert'
import Loading from '../ui/Loading'
import OffLine from '../ui/OffLine'
import StatusBarColor from '../ui/StatusBarColor'
import {darkInputPlaceholderTextColor} from '../../theme/corretoron'
import {LoginStyle as style} from '../../styles'

class Login extends React.Component {

    constructor(props) {
        super(props)

        this.moveAnimation = new Animated.Value(450)
        this.fadeAnimation = new Animated.Value(0)
    }

    _moveAnimation() {
        Animated.spring(this.moveAnimation, {
            toValue: 0,
            tension: .5,
            useNativeDriver: false
        }).start()

        setTimeout(() => {
            Animated.timing(this.fadeAnimation, {
                toValue: 1,
                duration: 1500,
                useNativeDriver: false
            }).start()
        }, 200)
    }

    componentDidMount() {

        this._moveAnimation()

        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case LOGIN_SUCCESS:
                    Alert.success('Login realizado com sucesso.')
                    this.props.navigation.navigate('Home')
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break
                case UNAUTHORIZED:
                    Alert.error('Acesso não autorizado.')
                    break
                case RESET_PASSWORD_SUCCESS:
                  Alert.success('O e-mail contendo uma nova senha foi enviado com sucesso.')
                  break
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render() {

        return (
            <Container>
                
                <Animated.View style={{opacity: this.fadeAnimation}}>
                    <ImageBackground />
                </Animated.View>

                <Loading />
                <OffLine />
                <StatusBarColor />

                <Animated.ScrollView style={[{top: this.moveAnimation}]}>
                    <Splash />

                    <Animated.View style={{opacity: this.fadeAnimation}}>
                        <Content style={style.Content}>

                            <Text static style={style.TextBemVindo}>Bem-vindo,</Text>
                            <Text static style={style.TextFacaLogin}>faça seu login para continuar.</Text>

                            <Form>

                                <Item
                                    rounded
                                    dark
                                    invalidated={'email' in this.props.errors}>
                                    <Icon active name="md-person" />
                                    <Input
                                        placeholder="E-mail"
                                        placeholderTextColor={darkInputPlaceholderTextColor}
                                        keyboardType="email-address"
                                        autoCapitalize="none"
                                        value={this.props.user.email}
                                        onChangeText={email => {
                                            this.props.changed({email})
                                        }}
                                    />
                                </Item>

                                <Item
                                    rounded
                                    dark
                                    invalidated={'password' in this.props.errors}>
                                    <Icon active name="md-lock" />
                                    <Input
                                        placeholder="Senha"
                                        placeholderTextColor={darkInputPlaceholderTextColor}
                                        secureTextEntry
                                        value={this.props.user.password}
                                        onChangeText={password => {
                                            this.props.changed({password})
                                        }}
                                    />
                                </Item>

                                <Button
                                    block
                                    style={style.ButtonEntrar}
                                    onPress={() => this.props.login(this.props.user) }>
                                    <Text style={style.TextEntrar}>ENTRAR</Text>
                                </Button>

                                <TouchableOpacity onPress={() => { this.esqueciMinhaSenha() }}>
                                    <Text static style={style.TextEsqueci}>Esqueci minha senha</Text>
                                </TouchableOpacity>

                                <Button block transparent onPress={() => this.props.add() }>
                                    <Text style={style.TextCriar}>Criar uma conta</Text>
                                </Button>

                            </Form>
                        </Content>
                    </Animated.View>
                    
                </Animated.ScrollView>
                
            </Container>
        )

    }

    esqueciMinhaSenha() {
        RNAlert.alert(
            'Recuperar a senha',
            'Enviaremos uma nova senha em seu e-mail\nDeseja fazer isso agora?',
            [
                {
                    text: 'NÃO',
                    onPress: () => {  },
                    style: 'cancel',
                },
                {text: 'SIM', onPress: () => { 
                  this.props.forgotPassword({email: this.props.user.email})
                }},
            ],
            {cancelable: false},
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.auth.user,
    errors: state.auth.errors,
    authenticated: state.auth.authenticated
})

const mapDispatchToProps = dispatch => bindActionCreators({ 
  changed, 
  login, 
  add, 
  forgotPassword 
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login)
