import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Default from '../../ui/Default'
import { List, ListItem, Text } from 'native-base'

import { Col, Grid, Row } from 'react-native-easy-grid'

import { changedParameters, search, select, SELECT } from '../../../actions/espelho-empreendimento'
import store from '../../../store'
import { select as selectBloco } from '../../../actions/espelho-bloco'
import { search as searchUnidade, SEARCH_SUCCESS as UNIDADE_SEARCH_SUCCESS } from '../../../actions/espelho-unidade'
import acessoResource, { ACAO_ESPELHO } from '../../../resources/acesso'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'
import HeaderSearchEspelho from '../../ui/HeaderSearchEspelho'

import { EspelhoSearchStyle as style } from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Search extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.onScrollEnd = this.onScrollEnd.bind(this)
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(action => {

      switch (action.type) {
        case SELECT:

          const empreendimento_id = action.payload.id

          acessoResource.post({
            acao: ACAO_ESPELHO,
            metadata: {
              empreendimento_id
            }
          })

          if (action.payload.blocos.length === 1) {
            const bloco = action.payload.blocos[0]
            this.props.selectBloco(bloco)
            this.props.searchUnidade({ espelho_bloco_id: bloco.id })
          } else {
            this.props.navigation.navigate('EspelhoBlocos')
          }

          break
        case UNIDADE_SEARCH_SUCCESS:
          this.props.navigation.navigate('EspelhoUnidades')
          break
        case 'SYSTEM_ONLINE':
          this.props.search({ page: null })
          break
      }

    })
    this.props.search({ page: null })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  onScrollEnd() {
    if (this.props.empreendimentos.next_page_url) {
      const page = this.props.empreendimentos.current_page + 1
      this.props.search({ ...this.props.parameters, page })
    }
  }

  render() {

    return (
      <Default
        header={[
          <ImageBackground top />,
          <HeaderDefault goHome />,
          <HeaderSearchEspelho />
        ]}

        footer={[
          <FooterDefault />
        ]}

        onScrollEnd={this.onScrollEnd}>

        <Separator title='ESPELHO DE VENDAS' />
        { this.props.empreendimentos.data.length === 0 ?
          <Text registerNotFound>{this.props.searched ? 'Desculpe, mas não há resultados para sua pesquisa.' : ''}</Text> :
          <List>
            {this.props.empreendimentos.data.map((empreendimento, i) => (
              <ListItem expanded key={i} button onPress={() => { this.props.select(empreendimento) }}>
                <Grid>
                  <Row>
                    <Col style={style.ColLeft}>
                      <Grid>
                        <Row>
                          <Col>
                            <Text style={style.TextNome}>{empreendimento.nome}</Text>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Text style={style.TextUnidadesLivres}>Unidade(s) livre(s)</Text>
                          </Col>
                        </Row>
                      </Grid>
                    </Col>
                    <Col style={style.ColRight}>
                      <Text style={[style.TextUnidadesDisponiveis, empreendimento.unidades_disponiveis == 0 ? style.TextUnidadesNaoDisponiveis : null]}>
                        {empreendimento.unidades_disponiveis}
                      </Text>
                    </Col>
                  </Row>
                </Grid>
              </ListItem>
            ))}
          </List>
        }


      </Default>
    )
  }

}

const mapStateToProps = state => ({
  empreendimentos: state.espelhoEmpreendimento.empreendimentos,
  empreendimento: state.espelhoEmpreendimento.empreendimento,
  parameters: state.espelhoEmpreendimento.parameters,
  errors: state.espelhoEmpreendimento.errors,
  searched: state.espelhoEmpreendimento.searched
})
const mapDispatchToProps = dispatch => bindActionCreators({
  search,
  changedParameters,
  select,
  selectBloco,
  searchUnidade
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Search)
