import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import Default from '../../ui/Default'
import {Text} from 'native-base'
import Numeral from '../../ui/Numeral'

import {search} from '../../../actions/espelho-unidade'
import HeaderDefault from '../../ui/HeaderDefault'
import HeaderSearchEspelho from '../../ui/HeaderSearchEspelho'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'
import {Col, Grid, Row} from 'react-native-easy-grid'

import {EspelhoUnidadesStyle as style} from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Unidades extends React.Component {

    backgroundColor(status) {

        switch (status) {
            case 'Escriturada':
                return '#ff6666'
            case 'Em Negociação':
                return '#c3e1ff'
            case 'Disponível':
                return '#98ffbb'
            case 'Indisponível':
                return '#ffd1d1'
            case 'Suspensão Helbor':
                return '#ffeda3'
            case 'Reservada com Proposta':
                return '#ff944c'
            case 'Pré-Escritura':
                return '#dfdfff'
            case 'Permutada':
                return '#ff8686'
            default:
                return '#ffffff'
        }

    }

    onScrollEnd() {
        if (this.props.unidades.next_page_url) {
            const page = this.props.unidades.current_page + 1
            this.props.search({...this.props.parameters, page})
        }
    }

    render () {
        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goBack />,
                    <HeaderSearchEspelho />
                ]}
                footer={[
                    <FooterDefault />
                ]}
                onScrollEnd={this.onScrollEnd.bind(this)}>

                <Separator title='ESPELHO DE VENDAS' />

                <Grid>
                    <Col>
                        <Text style={style.TextEmpreendimentoNome}>{this.props.empreendimento.nome}</Text>
                        <Text style={style.TextUnidadesDisponivels}>Unidades(s) livre(s): {this.props.empreendimento.unidades_disponiveis}</Text>
                    </Col>
                </Grid>

                <Grid style={style.Grid}>
                    <Col>
                        <Text style={style.TextEmpreendimentoNome}>{this.props.bloco.nome}</Text>
                        <Text style={style.TextUnidadesDisponivels}>Unidades(s) livre(s): {this.props.bloco.unidades_disponiveis}</Text>
                    </Col>
                </Grid>

                <Grid style={style.GridUnidades}>
                    { this.props.unidades.data.map(unidade => (
                        <Row key={unidade.id} style={{backgroundColor: this.backgroundColor(unidade.status)}}>
                            <Col style={[style.ColUnidade, style.ColUnidadeNome]}>
                                <Text style={style.TextUnidade}>{unidade.nome || ' - '}</Text>
                            </Col>
                            <Col style={[style.ColUnidade, style.ColUnidadeStatus]}>
                                <Text style={style.TextUnidade}>
                                    {unidade.status}
                                    {unidade.reserva && (' (' + unidade.reserva + ')')}
                                </Text>
                            </Col>
                            <Col style={[style.ColUnidade, style.ColUnidadeArea]}>
                                {unidade.area && <Text style={[style.TextUnidade, style.TextUnidadeArea]}>{(new Numeral(unidade.area)).format('0.00')}m²</Text>}
                            </Col>
                        </Row>
                    )) }
                </Grid>

            </Default>
        )
    }

}

const mapStateToProps = state => ({
    empreendimento: state.espelhoEmpreendimento.empreendimento,
    bloco: state.espelhoBloco.bloco,
    unidades: state.espelhoUnidade.unidades,
    parameters: state.espelhoUnidade.parameters
})

const mapDispatchToProps = dispatch => bindActionCreators({ search }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Unidades)
