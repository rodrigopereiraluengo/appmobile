import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Col, Grid} from 'react-native-easy-grid'

import Default from '../../ui/Default'
import {Left, List, ListItem, Text} from 'native-base'

import {select, SELECT} from '../../../actions/espelho-bloco'
import {search as searchUnidade, SEARCH_SUCCESS} from '../../../actions/espelho-unidade'

import store from '../../../store'
import HeaderDefault from '../../ui/HeaderDefault'
import HeaderSearchEspelho from '../../ui/HeaderSearchEspelho'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'

import {EspelhoBlocoStyle as style} from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Blocos extends React.Component {

    componentDidMount() {
        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case SELECT:
                    this.props.searchUnidade({espelho_bloco_id: action.payload.id})
                    break
                case SEARCH_SUCCESS:
                    this.props.navigation.navigate('EspelhoUnidades')
                    break
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render () {

        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goBack />,
                    <HeaderSearchEspelho />
                ]}
                footer={[
                    <FooterDefault />
                ]}
            >

                <Separator title='ESPELHO VENDAS' />

                <Grid>
                    <Col>
                        <Text style={style.TextEmpreendimentoNome}>{this.props.empreendimento.nome}</Text>
                        <Text style={style.TextUnidadesDisponivels}>Unidades(s) livre(s): {this.props.empreendimento.unidades_disponiveis}</Text>
                    </Col>
                </Grid>

                <List>
                    {this.props.empreendimento.blocos.map(bloco => (
                        <ListItem
                            style={[style.ListItem, bloco.unidades_disponiveis == 0 ? style.ListItemUnidadesNaoDisponiveis : null]}
                            expanded
                            key={bloco.id}
                            onPress={() => { this.props.select(bloco) }}>
                            <Left>
                                <Text>{bloco.nome}</Text>
                                {bloco.unidades_disponiveis > 0 && <Text> - {bloco.unidades_disponiveis} unidade(s) livre(s)</Text>}
                            </Left>
                        </ListItem>
                    ))}
                </List>

            </Default>
        )
    }

}

const mapStateToProps = state => ({
    empreendimento: state.espelhoEmpreendimento.empreendimento,
    errors: state.espelhoEmpreendimento.errors
})
const mapDispatchToProps = dispatch => bindActionCreators({ select, searchUnidade }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Blocos)
