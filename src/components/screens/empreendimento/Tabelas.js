import React from 'react'

import {Linking} from 'react-native'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Col, Grid, Row} from 'react-native-easy-grid'
import {changedParameters, tabelas as tabelasSearch} from '../../../actions/empreendimento'

import acessoResource, {ACAO_TABELA} from '../../../resources/acesso'

import {ActionSheet, Button, Card, Icon, Text} from 'native-base'
import Default from '../../ui/Default'

import store from '../../../store'
import HeaderDefault from '../../ui/HeaderDefault'
import HeaderSearch from '../../ui/HeaderSearch'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'

import {EmpreendimentoSearchStyle, TabelaStyle as style} from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Tabelas extends React.Component {

    componentDidMount() {

        if (!this.props.queryTabelas && this.props.tabelas.data.length === 0) {
            this.props.tabelasSearch({query: this.props.queryTabelas, page: null})
        }

        this.unsubscribe = store.subscribe(action => {
            if (action.type === 'SYSTEM_ONLINE') {
                this.props.tabelasSearch({query: this.props.queryTabelas, page: null})
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    onScrollEnd () {
        if (this.props.tabelas.links.next) {
            const page = this.props.tabelas.meta.current_page + 1
            this.props.tabelasSearch({query: this.props.queryTabelas, page})
        }
    }

    render() {

        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goHome />,
                    <HeaderSearch
                        value={this.props.queryTabelas}
                        onChangeText={this.onChangeTextSearch.bind(this)}
                    />
                ]}

                footer={[
                    <FooterDefault />
                ]}

                onScrollEnd={this.onScrollEnd.bind(this)}>

                <Separator title='TABELAS HELBOR' />

                { this.props.tabelas.data.length === 0 ? <Text registerNotFound>{this.props.searchedTabelas ? 'Desculpe, mas não há resultados para sua pesquisa.' : ''}</Text> :
                    this.props.tabelas.data.map((tabela, index) => (
                        <Card key={index} style={style.Card}>
                            <Grid>
                                <Row>
                                    <Col>
                                        <Text style={EmpreendimentoSearchStyle.TextNome}>{tabela.nome}</Text>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Grid>
                                            <Col style={EmpreendimentoSearchStyle.ColIcon}>
                                                <Icon
                                                    name='map-marker'
                                                    type='FontAwesome'
                                                    style={EmpreendimentoSearchStyle.IconCidadeEstado}
                                                />
                                            </Col>
                                            <Col style={EmpreendimentoSearchStyle.ColCidadeEstado}>
                                                <Text style={EmpreendimentoSearchStyle.TextCidadeEstado}>
                                                    {tabela.cidade_estado}
                                                </Text>
                                            </Col>
                                        </Grid>
                                        <Text style={EmpreendimentoSearchStyle.TextMetragem}>{tabela.metragem}</Text>
                                    </Col>
                                    <Col style={style.ColRight}>
                                        <Button
                                            block
                                            small
                                            disabled={tabela.tabelas.length === 0}
                                            style={EmpreendimentoSearchStyle.Button}
                                            onPress={() => { this.onPressDownload(tabela) }}>
                                            <Text>DOWNLOAD</Text>
                                        </Button>
                                    </Col>
                                </Row>
                            </Grid>
                        </Card>
                    ))
                }
            </Default>
        )
    }

    onChangeTextSearch (queryTabelas) {
        this.props.changedParameters({queryTabelas})
        this.props.tabelasSearch({query: queryTabelas, page: null})
    }

    onPressDownload (tabela) {
        this.actionSheet({
            name: 'TABELAS',
            sheets: tabela.tabelas,
            acesso: {
                acao: ACAO_TABELA,
                imovel_id: tabela.id
            }
        })
    }

    actionSheet(sheet) {

        if (sheet.sheets.length === 0) {
            return alert('Nenhum arquivo encontrado.')
        }

        if (sheet.sheets.length === 1) {
            acessoResource.post(sheet.acesso)
            return Linking.openURL(sheet.sheets[0])
        }

        ActionSheet.show({
            options: sheet.sheets.map(value => value.split('/').pop()),
            title: sheet.name
        }, value => {
            if (value !== undefined) {
                acessoResource.post(sheet.acesso)
                Linking.openURL(sheet.sheets[value])
            }
        })
    }

}

const mapStateToProps = state => ({
    tabelas: state.empreendimento.tabelas,
    searchedTabelas: state.empreendimento.searchedTabelas,
    queryTabelas: state.empreendimento.parameters.queryTabelas
})

const mapDispatchToProps = dispatch => bindActionCreators({ tabelasSearch, changedParameters }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Tabelas)
