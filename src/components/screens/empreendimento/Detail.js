import _ from 'lodash'
import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Linking } from 'react-native'
import { ActionSheet, Button, Text } from 'native-base'

import { Col, Grid, Row } from 'react-native-easy-grid'

import ViewMoreText from 'react-native-view-more-text'

import MapView, { Marker } from 'react-native-maps'

import Default from '../../ui/Default'
import { gallery } from '../../../actions/empreendimento'
import { add as addVisita } from '../../../actions/visita'
import { add as addProposta } from '../../../actions/proposta'

import acessoResource, {
  ACAO_ASSESSORIAS,
  ACAO_DOCUMENTO,
  ACAO_EMAIL_MKT, ACAO_FOTOS,
  ACAO_PAPER,
  ACAO_PLANTAS,
  ACAO_REGULAMENTOS,
  ACAO_FOLHETOS,
  ACAO_TABELA,
  ACAO_TOUR_VIRTUAL
} from '../../../resources/acesso'
import HeaderDefault from "../../ui/HeaderDefault"
import HeaderSearchEmpreendimento from "../../ui/HeaderSearchEmpreendimento"
import FooterDefault from "../../ui/FooterDefault"
import Contato from "../../ui/Contato"

import { EmpreendimentoDetailStyle as style, EmpreendimentoSearchStyle } from '../../../styles'
import ImageSlideShow from "../../ui/ImageSlideShow"
import ImageBackground from "../../ui/ImageBackground";


class Description extends React.Component {

  renderViewMore(onPress) {
    return (
      <Text onPress={onPress} style={style.ViewMore}>Ver mais &gt;</Text>
    )
  }

  renderViewLess(onPress) {
    return (
      <Text onPress={onPress} style={style.ViewMore}>Ver menos &gt;</Text>
    )
  }

  render() {
    return (
      <ViewMoreText
        numberOfLines={4}
        renderViewMore={this.renderViewMore}
        renderViewLess={this.renderViewLess}>
        <Text>{this.props.value}</Text>
      </ViewMoreText>
    )
  }

}


class Detail extends React.Component {

  constructor(props) {
    super(props)

    this.FOTOS = 'Fotos'
    this.PLANTAS = 'Plantas'
    this.PAPER = 'Paper'
    this.EMAIL_MARKETING = 'E-mail mkt'
    this.TABELA = 'Tabela'
    this.DOCUMENTO = 'Documento'
    this.TOUR_VIRTUAL = 'Tour Virtual'
    this.ASSESSORIAS = 'Assessorias'
    this.REGULAMENTOS = 'Regulamentos'
    this.FOLHETOS = 'Folhetos'

    this.ACOES = {}
    this.ACOES[this.FOTOS] = ACAO_FOTOS
    this.ACOES[this.PLANTAS] = ACAO_PLANTAS
    this.ACOES[this.PAPER] = ACAO_PAPER
    this.ACOES[this.EMAIL_MARKETING] = ACAO_EMAIL_MKT
    this.ACOES[this.TABELA] = ACAO_TABELA
    this.ACOES[this.DOCUMENTO] = ACAO_DOCUMENTO
    this.ACOES[this.TOUR_VIRTUAL] = ACAO_TOUR_VIRTUAL
    this.ACOES[this.ASSESSORIAS] = ACAO_ASSESSORIAS
    this.ACOES[this.REGULAMENTOS] = ACAO_REGULAMENTOS
    this.ACOES[this.FOLHETOS] = ACAO_FOLHETOS
  }


  render() {
    return (
      <Default
        header={[
          <ImageBackground top />,
          <HeaderDefault goBack />,
          <HeaderSearchEmpreendimento />
        ]}
        footer={[
          <FooterDefault />
        ]}>

        <Text title>{this.props.empreendimento.nome}</Text>
        <Text subtitle>Sobre o Imóvel</Text>
        <Description value={this.props.empreendimento.descricao} />

        <Text subtitle>FOTOS</Text>
        <ImageSlideShow images={this.props.empreendimento.galeria_thumb_400} />

        <Text subtitle>PLANTAS</Text>
        <ImageSlideShow images={this.props.empreendimento.plantas_thumb_400} />

        <Text subtitle>Ficha Técnica</Text>

        <Grid>
          {_.isEmpty(this.props.empreendimento.dorms) === false && parseInt(this.props.empreendimento.dorms) > 0 && <Row>
            <Col style={style.ColLeft}><Text>Dormitórios:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.dorms}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.suites) === false && parseInt(this.props.empreendimento.suites) > 0 && <Row>
            <Col style={style.ColLeft}><Text>Suites:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.suites}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.area) === false && <Row>
            <Col style={style.ColLeft}><Text>Metragem:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.area}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.terreno) === false && <Row>
            <Col style={style.ColLeft}><Text>Terreno:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.terreno}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.vagas) === false && <Row>
            <Col style={style.ColLeft}><Text>Vagas:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.vagas}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.torres) === false && <Row>
            <Col style={style.ColLeft}><Text>Torres:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.torres}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.andares) === false && <Row>
            <Col style={style.ColLeft}><Text>Andares:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.andares}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.unidades_andar) === false && <Row>
            <Col style={style.ColLeft}><Text>Unid. por Andar:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.unidades_andar}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.total_unidades) === false && <Row>
            <Col style={style.ColLeft}><Text>Total de Unid:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.total_unidades}</Text></Col>
          </Row>}

        </Grid>

        <Grid style={style.GridIncorporadora}>

          {_.isEmpty(this.props.empreendimento.incorporadora) === false && <Row>
            <Col style={style.ColLeft}><Text>Incorporadora:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.incorporadora}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.projetopaisagistico) === false && <Row>
            <Col style={style.ColLeft}><Text>Paisagismo:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.projetopaisagistico}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.projetoarquitetura) === false && <Row>
            <Col style={style.ColLeft}><Text>Arquitetura:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.projetoarquitetura}</Text></Col>
          </Row>}

          {_.isEmpty(this.props.empreendimento.projetodecoracao) === false && <Row>
            <Col style={style.ColLeft}><Text>Decoração:</Text></Col>
            <Col style={style.ColRight}><Text style={style.TextDescription}>{this.props.empreendimento.projetodecoracao}</Text></Col>
          </Row>}

        </Grid>

        <Text subtitle>Localização</Text>
        <Text>{this.props.empreendimento.endereco}</Text>

        <MapView
          region={{
            latitude: parseFloat(this.props.empreendimento.latitude),
            longitude: parseFloat(this.props.empreendimento.longitude),
            latitudeDelta: 0.0922 / 8,
            longitudeDelta: 0.0421 / 8,
          }}
          style={style.MapView}>

          <Marker
            coordinate={{
              latitude: parseFloat(this.props.empreendimento.latitude),
              longitude: parseFloat(this.props.empreendimento.longitude)
            }}
            title={this.props.empreendimento.name}
            description={this.props.empreendimento.endereco}
          />

        </MapView>

        <Grid style={style.GridButton}>
          <Row>
            <Col>
              <Button
                block
                small
                style={[
                  EmpreendimentoSearchStyle.Button,
                  EmpreendimentoSearchStyle.ButtonLeft,
                  EmpreendimentoSearchStyle.ButtonTop
                ]}
                onPress={this.onPressDownload.bind(this)}>
                <Text>DOWNLOAD</Text>
              </Button>
            </Col>
            <Col>
              <Button
                block
                small
                disabled={this.props.empreendimento.tourvirtual === null}
                style={[
                  EmpreendimentoSearchStyle.Button,
                  EmpreendimentoSearchStyle.ButtonRight,
                  EmpreendimentoSearchStyle.ButtonTop
                ]}
                onPress={this.onPressTourVirtual.bind(this)}>
                <Text>TOUR VIRTUAL</Text>
              </Button>
            </Col>
          </Row>

          {this.props.settings.helbor_botoes && <React.Fragment>
            <Row>
              <Col>
                <Button
                  block
                  small
                  disabled={this.props.empreendimento.assessorias.length === 0}
                  style={[
                    EmpreendimentoSearchStyle.Button,
                    EmpreendimentoSearchStyle.ButtonLeft,
                    EmpreendimentoSearchStyle.ButtonBottom
                  ]}
                  onPress={() => this.props.onPressAssessorias.bind(this)}
                >
                  <Text>ASSESSORIAS</Text>
                </Button>
              </Col>
              <Col>
                <Button
                  block
                  small
                  disabled={this.props.empreendimento.regulamentos.length === 0}
                  style={[
                    EmpreendimentoSearchStyle.Button,
                    EmpreendimentoSearchStyle.ButtonRight,
                    EmpreendimentoSearchStyle.ButtonBottom
                  ]}
                  onPress={this.onPressRegulamentos.bind(this)}>
                  <Text>REGULAMENTOS</Text>
                </Button>
              </Col>
            </Row>
          </React.Fragment>}
        </Grid>
      </Default>
    )
  }

  onPressDownload() {

    const {
      galeria_imagens,
      galeria_plantas,
      treinamentos,
      nome,
      emailmkt,
      tabelas,
      documentos,
      assessorias,
      regulamentos,
      folhetos
    } = this.props.empreendimento

    const options = []

    if (galeria_imagens.length) {
      options.push(this.FOTOS)
    }

    if (galeria_plantas.length) {
      options.push(this.PLANTAS)
    }

    if (treinamentos.length) {
      options.push(this.PAPER)
    }

    if (emailmkt.length) {
      options.push(this.EMAIL_MARKETING)
    }

    if (tabelas.length) {
      options.push(this.TABELA)
    }

    if (documentos.length) {
      options.push(this.DOCUMENTO)
    }

    if (assessorias.length) {
      options.push(this.ASSESSORIAS)
    }

    if (regulamentos.length) {
      options.push(this.REGULAMENTOS)
    }

    if (folhetos.length) {
      options.push(this.FOLHETOS)
    }

    ActionSheet.show({
      options,
      title: 'Download'
    }, value => {

      switch (options[value]) {
        case this.FOTOS:

          acessoResource.post({
            acao: ACAO_FOTOS,
            imovel_id: this.props.empreendimento.id
          })

          return this.props.gallery({
            name: nome,
            photos: galeria_imagens
          })

        case this.PLANTAS:

          acessoResource.post({
            acao: ACAO_PLANTAS,
            imovel_id: this.props.empreendimento.id
          })

          return this.props.gallery({
            name: nome,
            photos: galeria_plantas
          })

        case this.PAPER:
          return this.actionSheet({
            name: 'Paper',
            sheets: treinamentos
          })
        case this.EMAIL_MARKETING:

          acessoResource.post({
            acao: ACAO_EMAIL_MKT,
            imovel_id: this.props.empreendimento.id
          })

          return this.props.gallery({
            name: nome,
            photos: emailmkt
          })
        case this.TABELA:
          return this.actionSheet({
            name: 'Tabela',
            sheets: tabelas
          })
        case this.DOCUMENTO:
          return this.actionSheet({
            name: 'Documento',
            sheets: documentos
          })
        case this.ASSESSORIAS:
          return this.actionSheet({
            name: 'Assessorias',
            sheets: assessorias
          })
        case this.REGULAMENTOS:
          return this.actionSheet({
            name: 'Regulamentos',
            sheets: regulamentos
          })
        case this.FOLHETOS:
          return this.actionSheet({
            name: 'Folhetos',
            sheets: folhetos
          })
      }

    })
  }

  onPressTourVirtual() {

    if (this.props.empreendimento.tourvirtual) {
      acessoResource.post({
        acao: ACAO_TOUR_VIRTUAL,
        imovel_id: this.props.empreendimento.id
      })

      Linking.openURL(this.props.empreendimento.tourvirtual)
    }

  }

  actionSheet(sheet) {

    setTimeout(() => {

      if (sheet.sheets.length === 0) {
        return alert('Nenhum arquivo encontrado.')
      }

      if (sheet.sheets.length === 1) {
        acessoResource.post({
          acao: this.ACOES[sheet.name],
          imovel_id: this.props.empreendimento.id
        })
        return Linking.openURL(sheet.sheets[0])
      }

      ActionSheet.show({
        options: sheet.sheets.map(value => value.split('/').pop()),
        title: sheet.name
      }, value => {
        if (value !== undefined) {

          acessoResource.post({
            acao: this.ACOES[sheet.name],
            imovel_id: this.props.empreendimento.id
          })

          Linking.openURL(sheet.sheets[value])
        }
      })
    }, 100)
  }

  onPressAssessorias() {
    this.actionSheet({
      name: 'Assessorias',
      sheets: this.props.empreendimento.assessorias
    })
  }

  onPressRegulamentos() {
    this.actionSheet({
      name: 'Regulamentos',
      sheets: this.props.empreendimento.regulamentos
    })
  }

}

const mapStateToProps = state => ({
  empreendimento: state.empreendimento.empreendimento,
  settings: state.settings.settings
})

const mapDispatchToProps = dispatch => bindActionCreators({
  gallery,
  addVisita,
  addProposta
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Detail)
