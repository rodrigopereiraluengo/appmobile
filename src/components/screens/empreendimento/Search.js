import React from 'react'
import { Image, Linking, TouchableOpacity } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { ActionSheet, Button, Card, Icon, Text } from 'native-base'

import { Col, Grid } from 'react-native-easy-grid'

import Default from '../../ui/Default'

import { detail, gallery, search, select } from '../../../actions/empreendimento'
import { add as addVisita } from '../../../actions/visita'
import { add as addProposta } from '../../../actions/proposta'

import acessoResource, {
  ACAO_ASSESSORIAS,
  ACAO_PAPER,
  ACAO_PLANTAS, ACAO_REGULAMENTOS,
  ACAO_TABELA,
  ACAO_TOUR_VIRTUAL
} from '../../../resources/acesso'

import store from '../../../store'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import HeaderSearchEmpreendimento from '../../ui/HeaderSearchEmpreendimento'
import LinkButton from '../../ui/LinkButton'
import Separator from '../../ui/Separator'

import { EmpreendimentoSearchStyle as style } from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Search extends React.Component {

  constructor(props) {
    super(props)
    this.state = { actionSheet: null }
  }

  componentDidMount() {
    if (!this.props.parameters.query && this.props.empreendimentos.data.length === 0) {
      this.props.search({ ...this.props.parameters, page: null })
    }

    this.unsubscribe = store.subscribe(action => {
      if (action.type === 'SYSTEM_ONLINE') {
        this.props.search({ ...this.props.parameters, page: null })
      }
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  onScrollEnd() {
    if (this.props.empreendimentos.links.next) {
      const page = this.props.empreendimentos.meta.current_page + 1
      this.props.search({ ...this.props.parameters, page })
    }
  }

  render() {
    return (
      <Default
        header={[
          <ImageBackground top />,
          <HeaderDefault goHome />,
          <HeaderSearchEmpreendimento />
        ]}
        footer={[
          <FooterDefault />
        ]}
        onScrollEnd={this.onScrollEnd.bind(this)}>

        <Separator
          title='PRODUTOS HELBOR'
          button='Filtrar'
          onPress={() => { this.props.navigation.navigate('EmpreendimentoFilter') }}
        />

        { this.props.empreendimentos.data.length === 0 ?
          <Text registerNotFound>{this.props.searched ? 'Desculpe, mas não há resultados para sua pesquisa.' : ''}</Text> :
          this.props.empreendimentos.data.map((empreendimento, index) => {
            return (
              <Card key={index} style={style.Card}>

                <TouchableOpacity
                  key={index}
                  onPress={() => { this.props.detail(empreendimento.id) }}
                  activeOpacity={.85}>
                  <Grid>
                    <Col style={style.ColLeft}>
                      <Image source={{ uri: empreendimento.foto_capa_thumb }} style={style.Image} />
                    </Col>
                    <Col style={style.ColDescricoes}>
                      <Text style={style.TextNome}>{empreendimento.nome}</Text>
                      <Grid>
                        <Col style={style.ColIcon}>
                          <Icon
                            name='map-marker'
                            type='FontAwesome'
                            style={style.IconCidadeEstado}
                          />
                        </Col>
                        <Col style={style.ColCidadeEstado}>
                          <Text style={style.TextCidadeEstado}>
                            {empreendimento.cidade_estado}
                          </Text>
                        </Col>
                      </Grid>
                      <Text style={style.TextMetragem}>{empreendimento.metragem_dormitorios}</Text>
                      <LinkButton
                        title='Ver mais detalhes'
                        align='start'
                        textStyle={style.LinkButtonTextStyle}
                        onPress={() => { this.props.detail(empreendimento.id) }}
                      />
                    </Col>
                  </Grid>
                </TouchableOpacity>

                <Grid>
                  <Col>
                    <Button
                      block
                      small
                      disabled={empreendimento.galeria_plantas.length === 0}
                      style={[style.Button, style.ButtonLeft, style.ButtonTop]}
                      onPress={() => { this.onPressPlantas(empreendimento) }}>
                      <Text>PLANTAS</Text>
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      block
                      small
                      disabled={empreendimento.treinamentos.length === 0}
                      style={[style.Button, style.ButtonRight, style.ButtonTop]}
                      onPress={() => { this.onPressPaper(empreendimento) }}>
                      <Text>PAPER</Text>
                    </Button>
                  </Col>
                </Grid>

                <Grid>
                  <Col>
                    <Button
                      block
                      small
                      disabled={empreendimento.tourvirtual === null}
                      style={[style.Button, style.ButtonLeft]}
                      onPress={() => { this.onPressTourVirtual(empreendimento) }}>
                      <Text>TOUR VIRTUAL</Text>
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      block
                      small
                      disabled={empreendimento.tabelas.length === 0}
                      style={[style.Button, style.ButtonRight]}
                      onPress={() => { this.onPressTabela(empreendimento) }}>
                      <Text>TABELA</Text>
                    </Button>
                  </Col>
                </Grid>
                {this.props.settings.helbor_botoes && <React.Fragment>
                  <Grid>
                    <Col>
                      <Button
                        block
                        small
                        disabled={empreendimento.assessorias.length === 0}
                        style={[style.Button, style.ButtonLeft, style.ButtonBottom]}
                        onPress={() => { this.onPressAssessoria(empreendimento) }}>
                        <Text>ASSESSORIAS</Text>
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        block
                        small
                        disabled={empreendimento.regulamentos.length === 0}
                        style={[style.Button, style.ButtonRight, style.ButtonBottom]}
                        onPress={() => { this.onPressRegulamento(empreendimento) }}>
                        <Text>REGULAMENTOS</Text>
                      </Button>
                    </Col>
                  </Grid>
                </React.Fragment>}
              </Card>
            )
          })
        }
      </Default>
    )
  }

  actionSheet(sheet) {

    if (sheet.sheets.length === 0) {
      return alert('Nenhum arquivo encontrado.')
    }

    if (sheet.sheets.length === 1) {

      acessoResource.post(sheet.acesso)

      return Linking.openURL(sheet.sheets[0])
    }

    ActionSheet.show({
      options: sheet.sheets.map(value => value.split('/').pop()),
      title: sheet.name
    }, value => {
      if (value !== undefined) {

        acessoResource.post(sheet.acesso)

        Linking.openURL(sheet.sheets[value])
      }
    })
  }

  gallery({ acesso, name, photos }) {
    acessoResource.post(acesso)
    this.props.gallery({ name, photos })
  }

  onPressPlantas(empreendimento) {
    this.gallery({
      name: empreendimento.nome,
      photos: empreendimento.galeria_plantas,
      acesso: {
        acao: ACAO_PLANTAS,
        imovel_id: empreendimento.id
      }
    })
  }

  onPressPaper(empreendimento) {
    this.actionSheet({
      name: 'PAPER',
      sheets: empreendimento.treinamentos,
      acesso: {
        acao: ACAO_PAPER,
        imovel_id: empreendimento.id
      }
    })
  }

  onPressTourVirtual(empreendimento) {

    if (empreendimento.tourvirtual) {
      acessoResource.post({
        acao: ACAO_TOUR_VIRTUAL,
        imovel_id: empreendimento.id
      })

      Linking.openURL(empreendimento.tourvirtual)
    }

  }

  onPressTabela(empreendimento) {
    this.actionSheet({
      name: 'TABELAS',
      sheets: empreendimento.tabelas,
      acesso: {
        acao: ACAO_TABELA,
        imovel_id: empreendimento.id
      }
    })
  }

  onPressAssessoria(empreendimento) {
    this.actionSheet({
      name: 'ASSESSORIAS',
      sheets: empreendimento.assessorias,
      acesso: {
        acao: ACAO_ASSESSORIAS,
        imovel_id: empreendimento.id
      }
    })
  }

  onPressRegulamento(empreendimento) {
    this.actionSheet({
      name: 'REGULAMENTOS',
      sheets: empreendimento.regulamentos,
      acesso: {
        acao: ACAO_REGULAMENTOS,
        imovel_id: empreendimento.id
      }
    })
  }

  /*onPressVisita (empreendimento) {

      this.props.addVisita({
          empreendimento_id: empreendimento.id,
          empreendimento_nome: empreendimento.nome
      })

      this.props.select(empreendimento.id)
  }

  onPressProposta (empreendimento) {

      this.props.addProposta({
          empreendimento_id: empreendimento.id,
          empreendimento_nome: empreendimento.nome
      })

      this.props.select(empreendimento.id)
  }*/
}

const mapStateToProps = state => ({
  empreendimentos: state.empreendimento.empreendimentos,
  parameters: state.empreendimento.parameters,
  errors: state.empreendimento.errors,
  searched: state.empreendimento.searched,
  scroll: state.system.scroll,
  settings: state.settings.settings
})

const mapDispatchToProps = dispatch => bindActionCreators({
  search,
  detail,
  gallery,
  addVisita,
  addProposta,
  select
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Search)
