import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {changed, ERROR, save, SAVE_SUCCESS} from '../../../actions/proposta'

import {Col, Grid, Row} from 'react-native-easy-grid'

import Default from '../../ui/Default'
import {Button, Form, Input, Item, Label, Text} from 'native-base'

import InputNumber from '../../ui/InputNumber'

import store from '../../../store'

import {SELECT_SUCCESS as EMPREENDIMENTO_SELECT_SUCCESS} from '../../../actions/empreendimento'

import Alert from '../../ui/Alert'
import InputMask from '../../ui/InputMask'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'
import {styleColumnFirst, styleColumnSecond} from '../../../theme/corretoron'
import ImageBackground from "../../ui/ImageBackground";

class Proposta extends React.Component {

    componentDidMount() {
        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case EMPREENDIMENTO_SELECT_SUCCESS:
                    this.props.changed({msg: action.payload.mensagem_proposta})
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break

                case SAVE_SUCCESS:
                    Alert.success('Proposta enviada com sucesso.')
                    this.props.navigation.goBack()
                    break
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render() {
        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goBack />
                ]}
                footer={[
                    <FooterDefault />
                ]}>

                <Separator title={this.props.proposta.empreendimento_nome.toUpperCase()} />

                <Form>

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Nome</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_nome' in this.props.errors}>
                                    <Input
                                        value={this.props.proposta.cliente_nome}
                                        onChangeText={cliente_nome => this.props.changed({cliente_nome})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>RG</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_rg' in this.props.errors}>
                                    <Input
                                        value={this.props.proposta.cliente_rg}
                                        onChangeText={cliente_rg => this.props.changed({cliente_rg})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>CPF</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_rg' in this.props.errors}>
                                    <InputMask
                                        mask='cpf'
                                        value={this.props.proposta.cliente_cpf}
                                        keyboardType='numeric'
                                        onChangeText={cliente_cpf => this.props.changed({cliente_cpf})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Valor R$</Label>
                                <Item
                                    regular
                                    invalidated={'valor' in this.props.errors}>
                                    <InputNumber
                                        value={this.props.proposta.valor}
                                        onChangeText={valor => this.props.changed({valor})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                    </Grid>

                    <Label>Mensagem</Label>
                    <Item
                        regular
                        invalidated={'msg' in this.props.errors}>
                        <Input
                            multiline={true}
                            numberOfLines={8}
                            value={this.props.proposta.msg}
                            onChangeText={msg => this.props.changed({msg})}
                        />
                    </Item>

                    <Button onPress={() => {this.props.save(this.props.proposta)}}>
                        <Text>ENVIAR PROPOSTA</Text>
                    </Button>

                </Form>
            </Default>
        )
    }

}

const mapStateToProps = state => ({
    proposta: state.proposta.proposta,
    errors: state.proposta.errors,
    user: state.auth.auth.user
})

const mapDispatchToProps = dispatch => bindActionCreators({ changed, save }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Proposta)
