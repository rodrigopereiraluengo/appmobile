import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Button, Form, Input, Item, Label, Text} from 'native-base'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment-timezone'

import {Col, Grid, Row} from 'react-native-easy-grid'

import store from '../../../store'
import Default from '../../ui/Default'

import {SELECT_SUCCESS as SELECT_EMPREENDIMENTO_SUCCESS} from '../../../actions/empreendimento'
import {changed, ERROR, save, SAVE_SUCCESS} from '../../../actions/visita'
import Alert from '../../ui/Alert'
import InputMask from '../../ui/InputMask'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'
import {styleColumnFirst, styleColumnSecond} from '../../../theme/corretoron'
import {Appearance} from 'react-native-appearance'

class Visita extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            dateTimeVisible: false,
            dateTimeValue: moment().toDate(),
            dateTimeMode: 'date',
            dateTimeOnConfirm: () => {},
            creciFillable: this.props.user.creci !== ''
        }

        this.isDarkModeEnabled = Appearance.getColorScheme() === 'dark'
    }

    componentDidMount() {

        this.props.changed({creci: this.props.user.creci})

        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case SELECT_EMPREENDIMENTO_SUCCESS:
                    this.props.changed({msg: action.payload.mensagem_visita})
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break

                case SAVE_SUCCESS:
                    Alert.success('Visita solicitada com sucesso.')
                    this.props.navigation.goBack()
                    break
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    showDateTimePicker({dateTimeValue, dateTimeMode, dateTimeOnConfirm}) {

        if (typeof dateTimeValue === 'string') {
            dateTimeValue = moment(dateTimeValue).toDate()
        }

        const now = moment().add(30, 'minutes')
        now.minutes(Math.ceil(now.minutes()/5)*5)

        this.setState({
            dateTimeVisible: true,
            dateTimeValue: dateTimeValue ? dateTimeValue :now.toDate(),
            dateTimeMode,
            dateTimeOnConfirm: argument => {
                dateTimeOnConfirm(moment(argument).format('YYYY-MM-DD HH:mm'))
                this.setState({dateTimeVisible: false})
            }
        })
    }

    render() {
        return (
            <Default header={[
                    <HeaderDefault goBack />
                ]}
                footer={[
                    <FooterDefault />,
                    <Contato />
                ]}>

                <Separator title={this.props.visita.empreendimento_nome.toUpperCase()} />

                <Form>

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Data</Label>
                                <Item
                                    regular
                                    invalidated={'data_visita' in this.props.errors}
                                    onPress={() => this.showDateTimePicker({
                                        dateTimeMode: 'date',
                                        dateTimeValue: this.props.visita.data_visita,
                                        dateTimeOnConfirm: data_visita => this.props.changed({data_visita})
                                    })}>
                                    <Text staticinput>{this.props.visita.data_visita ? moment(this.props.visita.data_visita, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY') : 'Selecione...'}</Text>
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Hora</Label>
                                <Item
                                    regular
                                    invalidated={'data_visita' in this.props.errors}
                                    onPress={() => this.showDateTimePicker({
                                        dateTimeMode: 'time',
                                        dateTimeValue: this.props.visita.data_visita,
                                        dateTimeOnConfirm: data_visita => this.props.changed({data_visita})
                                    })}>
                                    <Text staticinput>{this.props.visita.data_visita ? moment(this.props.visita.data_visita, 'YYYY-MM-DD HH:mm').format('HH:mm') : 'Selecione...'}</Text>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Nome</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_nome' in this.props.errors}>
                                    <Input
                                        value={this.props.visita.cliente_nome}
                                        onChangeText={cliente_nome => this.props.changed({cliente_nome})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>RG</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_rg' in this.props.errors}>
                                    <Input
                                        value={this.props.visita.cliente_rg}
                                        onChangeText={cliente_rg => this.props.changed({cliente_rg})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>CPF</Label>
                                <Item
                                    regular
                                    invalidated={'cliente_rg' in this.props.errors}>
                                    <InputMask
                                        mask='cpf'
                                        value={this.props.visita.cliente_cpf}
                                        keyboardType='numeric'
                                        onChangeText={cliente_cpf => this.props.changed({cliente_cpf})}
                                    />
                                </Item>
                            </Col>
                             <Col style={styleColumnSecond}>
                                 {this.state.creciFillable === false && <React.Fragment>
                                    <Label>CRECI</Label>
                                    <Item
                                        regular
                                        invalidated={'creci' in this.props.errors}>
                                        <Input
                                            value={this.props.visita.creci}
                                            onChangeText={creci => this.props.changed({creci})}
                                        />
                                    </Item>
                                </React.Fragment>}
                            </Col>
                        </Row>
                    </Grid>

                    <Label>Mensagem</Label>
                    <Item
                        regular
                        invalidated={'msg' in this.props.errors}>
                        <Input
                            multiline={true}
                            numberOfLines={8}
                            value={this.props.visita.msg}
                            onChangeText={msg => this.props.changed({msg})}
                        />
                    </Item>

                    <Button onPress={() => {this.props.save(this.props.visita)}}>
                        <Text>SOLICITAR VISITA</Text>
                    </Button>

                </Form>

                <DateTimePicker
                    isVisible={this.state.dateTimeVisible}
                    mode={this.state.dateTimeMode}
                    onConfirm={this.state.dateTimeOnConfirm}
                    onCancel={() => this.setState({dateTimeVisible: false})}
                    date={this.state.dateTimeValue}
                    isDarkModeEnabled={this.isDarkModeEnabled}
                />

            </Default>
        )
    }
}

const mapStateToProps = state => ({
    visita: state.visita.visita,
    errors: state.visita.errors,
    user: state.auth.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({ changed, save }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Visita)
