import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Button, Form, Item, Label, Text} from 'native-base'
import InputInteger from '../../ui/InputInteger'
import ItemPicker from '../../ui/ItemPicker'

import _ from 'lodash'

import store from '../../../store'

import Default from '../../ui/Default'

import {search as cidadeSearch, SEARCH_SUCCESS as CIDADE_SEARCH_SUCCESS} from '../../../actions/cidade'
import {search as categoriaSearch, SEARCH_SUCCESS as CATEGORIA_SEARCH_SUCCESS} from '../../../actions/categoria'
import {changedParameters, ERROR, filtros, search, SEARCH_SUCCESS} from '../../../actions/empreendimento'
import Alert from '../../ui/Alert'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import {Col, Grid, Row} from 'react-native-easy-grid'
import {styleColumnFirst, styleColumnSecond} from '../../../theme/corretoron'
import ImageBackground from "../../ui/ImageBackground";

class Filter extends React.Component {

    componentDidMount() {

        if (this.props.cidades.length === 0) {
            this.props.cidadeSearch()
        }

        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case CIDADE_SEARCH_SUCCESS:
                    this.props.categoriaSearch()
                    break
                case CATEGORIA_SEARCH_SUCCESS:
                    this.props.filtros()
                    break
                case SEARCH_SUCCESS:
                    this.props.navigation.navigate('EmpreendimentoSearch')
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    onPressFiltrar () {
        this.props.search({...this.props.parameters, ...{query: '', page: null}})
    }

    render() {
        return (
            <Default
                header={[
                    <ImageBackground top />,
                    <HeaderDefault goBack />
                ]}
                footer={[
                    <FooterDefault />
                ]}>

                <Text separator>Filtrar</Text>
                <Form>

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Metragem mín.</Label>
                                <Item
                                    regular
                                    invalidated={'area_util_min' in this.props.errors}>
                                    <InputInteger
                                        value={this.props.parameters.area_util_min}
                                        onChangeText={area_util_min => this.props.changedParameters({area_util_min})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Metragem máx.</Label>
                                <Item
                                    regular
                                    invalidated={'area_util_max' in this.props.errors}>
                                    <InputInteger
                                        value={this.props.parameters.area_util_max}
                                        onChangeText={area_util_max => this.props.changedParameters({area_util_max})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Categoria</Label>
                                <ItemPicker
                                    invalidated={'categoria' in this.props.errors}
                                    selectedValue={this.props.parameters.categoria}
                                    onValueChange={categoria => this.props.changedParameters({categoria})}
                                    placeholder='Todas...'
                                    items={this.props.categorias.map(value => ({value: value.id, label: value.title}))}
                                />
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Cidade</Label>
                                <ItemPicker
                                    invalidated={'cidade' in this.props.errors}
                                    selectedValue={this.props.parameters.cidade}
                                    onValueChange={cidade => this.props.changedParameters({cidade})}
                                    placeholder='Todas...'
                                    items={this.props.cidades.map(value => ({value: value.id, label: value.name}))}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Fase</Label>
                                <ItemPicker
                                    invalidated={'fase_construcao' in this.props.errors}
                                    selectedValue={this.props.parameters.fase_construcao}
                                    onValueChange={fase_construcao => this.props.changedParameters({fase_construcao})}
                                    placeholder={false}
                                    items={_.keys(this.props.fase_construcao).map((value, index) => (
                                        {value: value, label: this.props.fase_construcao[value]}
                                    ))}
                                />
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Dormitórios</Label>
                                <ItemPicker
                                    invalidated={'dormitorio' in this.props.errors}
                                    selectedValue={this.props.parameters.dormitorio}
                                    onValueChange={dormitorio => this.props.changedParameters({dormitorio})}
                                    placeholder='Todos...'
                                    items={_.keys(this.props.dormitorio).map(value => (
                                        {value, label: this.props.dormitorio[value]}
                                    ))}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Suites</Label>
                                <ItemPicker
                                    invalidated={'suite' in this.props.errors}
                                    selectedValue={this.props.parameters.suite}
                                    onValueChange={suite => this.props.changedParameters({suite})}
                                    placeholder='Todas...'
                                    items={_.keys(this.props.suite).map(value => (
                                        {value, label: this.props.suite[value]}
                                    ))}
                                />
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Vagas</Label>
                                <ItemPicker
                                    invalidated={'vaga' in this.props.errors}
                                    selectedValue={this.props.parameters.vaga}
                                    onValueChange={vaga => this.props.changedParameters({vaga})}
                                    placeholder='Todas...'
                                    items={_.keys(this.props.suite).map(value => (
                                        {value, label: this.props.vaga[value]}
                                    ))}
                                />
                            </Col>
                        </Row>
                    </Grid>

                    <Button onPress={this.onPressFiltrar.bind(this)}>
                        <Text>FILTRAR</Text>
                    </Button>

                </Form>

            </Default>
        )
    }

}

const mapStateToProps = state => ({
    parameters: state.empreendimento.parameters,
    cidades: state.cidade.cidades,
    categorias: state.categoria.categorias,
    fase_construcao: state.empreendimento.filtros.fase_construcao,
    dormitorio: state.empreendimento.filtros.dormitorio,
    suite: state.empreendimento.filtros.suite,
    vaga: state.empreendimento.filtros.vaga,
    errors: state.empreendimento.errors,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    cidadeSearch,
    categoriaSearch,
    filtros,
    changedParameters,
    search
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Filter)
