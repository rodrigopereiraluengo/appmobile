import React from 'react'

import {connect} from 'react-redux'

import {FlatList, Image, TouchableOpacity, View} from 'react-native'

import Default from '../../ui/Default'
import { Icon } from 'native-base'

import * as FileSystem from 'expo-file-system'
import * as Permissions from 'expo-permissions'
import * as MediaLibrary from 'expo-media-library'

import Alert from '../../ui/Alert'
import HeaderDefault from '../../ui/HeaderDefault'
import FooterDefault from '../../ui/FooterDefault'
import Contato from '../../ui/Contato'
import Separator from '../../ui/Separator'

import { EmpreendimentoGallery as style } from '../../../styles'
import ImageBackground from "../../ui/ImageBackground";

class Gallery extends React.Component {

    constructor(props) {
        super(props)

        this.state = {selected: []}
    }

    render() {
        return (
            <Default header={[
                <ImageBackground top />,
                <HeaderDefault goBack />
            ]}
            footer={[
                <FooterDefault />
            ]}>

            <Separator
                title={this.props.gallery.name.toUpperCase()}
                button='Download'
                onPress={ async () => { this.onPressDownload() }}
            />

                <FlatList data={this.props.gallery.photos.map((value, index) => {
                    return {id: index, src: value, name: value.split('/').pop()}
                })}
                          renderItem={({item}) => (
                              <View style={style.View}>
                                  { this.isSelected(item.name) &&
                                  <Icon type='FontAwesome' name='check' style={style.Icon} /> }
                                  <TouchableOpacity
                                      key={ item.id }
                                      style={style.TouchableOpacity}
                                      onPress={() => this.onPressSelect(item.name)}
                                      activeOpacity={.8}>
                                      <Image
                                          source={{uri: item.src}}
                                          style={[style.Image, {opacity : this.isSelected(item.name) ? .8 : 1}]} />
                                  </TouchableOpacity>
                              </View>
                          )}
                          numColumns={4}
                          keyExtractor={(item, index) => index.toString()}>
                </FlatList>

            </Default>
        )
    }

    onPressSelect (name) {
        if (this.state.selected.includes(name)) {
            this.setState({selected: this.state.selected.filter(value => value !== name)})
        } else {
            this.setState({selected: this.state.selected.concat(name)})
        }
    }

    isSelected (name) {
        return this.state.selected.includes(name)
    }

    async onPressDownload () {

        let cameraPermissions = await Permissions.getAsync(Permissions.CAMERA_ROLL)
        if (cameraPermissions.status !== 'granted') {
            cameraPermissions = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        }

        if (cameraPermissions.status === 'granted') {

            if (this.state.selected.length === 0) {
                return Alert.error('Selecione ao menos uma imagem.')
            }

            let album = await MediaLibrary.getAlbumAsync(this.props.gallery.name)
            this.state.selected.forEach(async name => {
                const image = this.props.gallery.photos.filter(value => value.split('/').pop() === name).pop()
                const { uri } = await FileSystem.downloadAsync(image, FileSystem.documentDirectory.concat(name))
                const asset = await MediaLibrary.createAssetAsync(uri)
                if (album) {
                    await MediaLibrary.addAssetsToAlbumAsync(asset, album)
                } else {
                    album = await MediaLibrary.createAlbumAsync(this.props.gallery.name, asset)
                }
            })

            this.setState({selected: []})

            Alert.success('Download realizado com sucesso')

        } else Alert.error('Permissões não foram concedidas.')
    }
}

const mapStateToProps = state => ({ gallery: state.empreendimento.gallery })

export default connect(mapStateToProps)(Gallery)
