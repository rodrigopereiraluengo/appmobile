import React from 'react'
import { Image, Linking, TouchableOpacity } from 'react-native'
import { ActionSheet, Button, Card, Text } from 'native-base'
import { withNavigation } from 'react-navigation'
import { Col, Grid, Row } from 'react-native-easy-grid'
import { connect } from 'react-redux'
import _ from 'lodash'

import HeaderHome from '../ui/HeaderHome'

import Default from '../ui/Default'
import Separator from '../ui/Separator'
import HeaderSearchEmpreendimento from '../ui/HeaderSearchEmpreendimento'

import ProdutosImage from '../../../assets/images/produtos.png'
import TabelasImage from '../../../assets/images/tabelas.png'
import EspelhoImage from '../../../assets/images/espelho.png'
import AgendaImage from '../../../assets/images/agenda.png'
import HistoriaImage from '../../../assets/images/historia.png'
import ApresentacaoImage from '../../../assets/images/apresentacao.png'

import { HomeStyle as style } from '../../styles'
import { calcHeight, calcWidth } from '../../theme/corretoron'
import ImageBackground from "../ui/ImageBackground";
import { bindActionCreators } from "redux";
import { historico_sht } from '../../actions/content'
import { regiao_arquivos } from '../../actions/user'
import { get as getSettings } from '../../actions/settings'

function CardButton(props) {
  return <TouchableOpacity activeOpacity={.85} onPress={() => { props.onPress() }}>
    <Card style={style.Card}>
      <Image source={props.image} style={[style.CardImage, { width: calcWidth(props.imageWidth), height: calcHeight(40) }]} />
      <Text style={[style.CardText]}>{props.firstLine}</Text>
      <Text style={[style.CardText, style.CardTextSecondLine]}>{props.secondLine}</Text>
      <Button block small style={style.CardButton} onPress={() => { props.onPress() }}>
        <Text style={style.CardButtonText}>Acessar</Text>
      </Button>
    </Card>
  </TouchableOpacity>
}

class Home extends React.Component {

  componentDidMount() {
    this.props.get_historico_sht()
    this.props.regiao_arquivos()
    this.props.getSettings()
  }

  render() {
    return (
      <Default
        lighten
        header={[
          <ImageBackground top />,
          <HeaderHome />,
          <HeaderSearchEmpreendimento />
        ]}

        footer={[

        ]}>

        <Separator
          title={`Olá, ${this.props.user.name}.`}
          button='Editar perfil'
          onPress={() => { this.props.navigation.navigate('Profile') }}
        />

        <Text style={style.TextTudo}>Tudo que você precisa está aqui!</Text>

        <Grid>
          <Row>
            <Col style={style.ColLeft}>
              <CardButton
                onPress={() => { this.props.navigation.navigate('EmpreendimentoSearch') }}
                image={ProdutosImage}
                imageWidth={46}
                firstLine="Produtos"
                secondLine="Helbor"
              />
            </Col>
            <Col style={style.ColRight}>
              <CardButton
                onPress={() => { this.props.navigation.navigate('EmpreendimentoTabelas') }}
                image={TabelasImage}
                imageWidth={35}
                firstLine="Tabelas"
                secondLine="Helbor"
              />
            </Col>
          </Row>
          <Row>
            <Col style={style.ColLeft}>
              <CardButton
                onPress={() => { this.props.navigation.navigate('EspelhoSearch') }}
                image={EspelhoImage}
                imageWidth={52}
                firstLine="Espelho de"
                secondLine="vendas"
              />
            </Col>
            <Col style={style.ColRight}>
              <CardButton
                onPress={() => { this.props.navigation.navigate('Agenda') }}
                image={AgendaImage}
                imageWidth={37}
                firstLine="Agenda"
                secondLine="online"
              />
            </Col>
          </Row>
          {this.props.settings.helbor_botoes && <React.Fragment>
            <Row>
              <Col style={style.ColLeft}>
                <CardButton
                  onPress={() => {
                    this.actionSheet({
                      sheets: this.props.historico_sht,
                      name: 'História SHT'
                    })
                  }}
                  image={HistoriaImage}
                  imageWidth={55}
                  firstLine="História"
                  secondLine="SHT"
                />
              </Col>
              <Col style={style.ColRight}>
                <CardButton
                  onPress={() => {
                    this.actionSheet({
                      sheets: _.get(this.props.user_regiao_arquivos, 'apresentacao'),
                      name: 'Apresentação meeting'
                    })
                  }}
                  image={ApresentacaoImage}
                  imageWidth={45}
                  firstLine="Apresentação"
                  secondLine="meeting"
                />
              </Col>
            </Row>
          </React.Fragment>}
        </Grid>
      </Default>
    )
  }

  actionSheet(sheet) {

    setTimeout(() => {

      if (sheet.sheets.length === 0) {
        return alert('Nenhum arquivo encontrado.')
      }

      if (sheet.sheets.length === 1) {
        return Linking.openURL(sheet.sheets[0])
      }

      ActionSheet.show({
        options: sheet.sheets.map(value => value.split('/').pop()),
        title: sheet.name
      }, value => {
        if (value !== undefined) {
          Linking.openURL(sheet.sheets[value])
        }
      })
    }, 100)
  }
}

const mapStateToProps = state => ({
  user: state.auth.auth.user,
  historico_sht: state.content.historico_sht,
  user_regiao_arquivos: state.user.regiao_arquivos,
  settings: state.settings.settings
})

const mapDispatchToProps = dispatch => bindActionCreators({
  get_historico_sht: historico_sht,
  regiao_arquivos,
  getSettings
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Home))
