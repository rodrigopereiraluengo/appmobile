import ImageBackground from "../../ui/ImageBackground";

const mime = require('react-native-mime-types')

import React from 'react'

import {Alert as RNAlert, TouchableOpacity} from 'react-native'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {withNavigation} from 'react-navigation'

import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import {Button, Form, Input, Item, Label, Text, Thumbnail} from 'native-base'

import {Col, Grid, Row} from 'react-native-easy-grid'

import DateTimePicker from 'react-native-modal-datetime-picker'

import Separator from '../../ui/Separator'
import thumbnail from '../../../../assets/images/thumbnail.png'

import moment from 'moment-timezone'

import store from '../../../store'

import {logout, LOGOUT} from '../../../actions/auth'
import {changed, edit, ERROR, imobiliarias as searchImobiliarias, save, SAVE_SUCCESS} from '../../../actions/user'
import {search as regiaoSearch} from '../../../actions/regiao'
import {cep, CEP_SEARCH_SUCCESS} from '../../../actions/endereco'

import Default from '../../ui/Default'
import InputMask from '../../ui/InputMask'
import Alert from '../../ui/Alert'
import HeaderDefault from '../../ui/HeaderDefault'
import ItemPicker from '../../ui/ItemPicker'

import {inputPlaceholderTextColor, styleColumnFirst, styleColumnSecond} from '../../../theme/corretoron'
import FooterDefault from '../../ui/FooterDefault'

import {ProfileStyle as style} from '../../../styles'

const estados = [
    {value:'AC', label:'Acre'},
    {value:'AL', label:'Alagoas'},
    {value:'AP', label:'Amapá'},
    {value:'AM', label:'Amazonas'},
    {value:'BA', label:'Bahia'},
    {value:'CE', label:'Ceará'},
    {value:'DF', label:'Distrito Federal'},
    {value:'ES', label:'Espirito Santo'},
    {value:'GO', label:'Goiás'},
    {value:'MA', label:'Maranhão'},
    {value:'MS', label:'Mato Grosso do Sul'},
    {value:'MT', label:'Mato Grosso'},
    {value:'MG', label:'Minas Gerais'},
    {value:'PA', label:'Pará'},
    {value:'PB', label:'Paraíba'},
    {value:'PR', label:'Paraná'},
    {value:'PE', label:'Pernambuco'},
    {value:'PI', label:'Piauí'},
    {value:'RJ', label:'Rio de Janeiro'},
    {value:'RN', label:'Rio Grande do Norte'},
    {value:'RS', label:'Rio Grande do Sul'},
    {value:'RO', label:'Rondônia'},
    {value:'RR', label:'Roraima'},
    {value:'SC', label:'Santa Catarina'},
    {value:'SP', label:'São Paulo'},
    {value:'SE', label:'Sergipe'},
    {value:'TO', label:'Tocantins'}
]


class Profile extends React.Component {

    constructor(props) {
        super(props)

        this.state = {dateTimeVisible: false}
    }

    componentDidMount() {

        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case LOGOUT:
                    this.props.navigation.navigate('Login')
                    break
                case CEP_SEARCH_SUCCESS:
                    const { endereco, bairro, cidade, estado } = action.payload.data
                    this.props.changed({
                        endereco,
                        bairro,
                        cidade,
                        uf: estado
                    })
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break
                case SAVE_SUCCESS:
                    Alert.success('Alterações foram salvas com sucesso.')

                    break
            }
        })

        if (this.props.regioes.length === 0) {
            this.props.regiaoSearch()
        }

        if (this.props.user.pertence_imobiliaria && this.props.imobiliarias.length === 0) {
            this.props.searchImobiliarias()
        }

        this.props.edit(this.props.auth.auth.user)
    }

    componentWillUnmount () {
        this.unsubscribe()
    }

    onBlurCep() {
        this.props.cep(this.props.user.cep)
    }

    onChangePertenceImobiliaria(pertence_imobiliaria) {

        this.props.changed({pertence_imobiliaria})

        if (pertence_imobiliaria && this.props.imobiliarias.length === 0) {
            this.props.searchImobiliarias()
        }
    }

    async selectThumbnail() {
        let cameraPermissions = await Permissions.getAsync(Permissions.CAMERA_ROLL)
        if (cameraPermissions.status !== 'granted') {
            cameraPermissions = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        }

        if (cameraPermissions.status === 'granted') {

            let result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3],
                base64: true
            })

            if (result.cancelled) {
                return
            }
            
            const uri = result.uri
            const foto = uri.split('/').pop()
            const type = mime.lookup(uri)
            
            this.props.changed({foto, foto_base64: `data:${type};base64,${result.base64}`})

        } else Alert.error('Permissões não foram concedidas.')
    }

    getThumbnail() {
        if (this.props.user.foto_base64) {
            return {uri: this.props.user.foto_base64}
        }

        if (!this.props.user.foto || mime.lookup(this.props.user.foto) === false) {
            return thumbnail
        }

        if (this.props.user.foto) {
            return {uri: this.props.user.foto}
        }
    }



    logout() {
        RNAlert.alert(
            'Sair',
            'Deseja realmente sair?',
            [
                {
                    text: 'NÃO',
                    onPress: () => {  },
                    style: 'cancel',
                },
                {text: 'SIM', onPress: () => { this.props.logout() }},
            ],
            {cancelable: false},
        )
    }

    render() {
        return (
            <Default header={[
                <ImageBackground top />,
                <HeaderDefault goHome />
            ]}
            footer={[
                <FooterDefault />
            ]}>
                <Separator title='DADOS PESSOAIS' button='Sair' onPress={ () => this.logout() } />

                <TouchableOpacity onPress={this.selectThumbnail.bind(this)}>
                    <Thumbnail
                        large
                        source={this.getThumbnail()}
                        style={style.Thumbnail}
                    />
                </TouchableOpacity>

                <Form>

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Nome</Label>
                                <Item
                                    regular
                                    invalidated={'name' in this.props.errors}>
                                    <Input
                                        value={this.props.user.name}
                                        onChangeText={name => this.props.changed({name})}
                                    />
                                </Item>                   
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Sobrenome</Label>
                                <Item
                                    regular
                                    invalidated={'sobrenome' in this.props.errors}>
                                    <Input
                                        value={this.props.user.sobrenome}
                                        onChangeText={sobrenome => this.props.changed({sobrenome})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Gênero</Label>
                                <ItemPicker
                                    invalidated={'genero' in this.props.errors}
                                    selectedValue={this.props.user.genero}
                                    onValueChange={genero => this.props.changed({genero})}
                                    items={[
                                        {value: 'M', label: 'Masculino'},
                                        {value: 'F', label: 'Feminino'}
                                    ]} />
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Data de nasc.</Label>
                                <Item
                                    regular
                                    invalidated={'nascimento' in this.props.errors}
                                    onPress={() => this.setState({dateTimeVisible: true})}>
                                    <Text staticinput>{this.props.user.nascimento ? moment(this.props.user.nascimento, 'YYYY-MM-DD').format('DD/MM/YYYY') : 'Selecione...'}</Text>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>E-mail</Label>
                                <Item
                                    regular
                                    invalidated={'email' in this.props.errors}>
                                    <Input
                                        keyboardType='email-address'
                                        autoCapitalize='none'
                                        value={this.props.user.email}
                                        onChangeText={email => this.props.changed({email})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Confirmação do E-mail</Label>
                                <Item
                                    regular
                                    invalidated={'email_confirmation' in this.props.errors}>
                                    <Input
                                        keyboardType='email-address'
                                        autoCapitalize='none'
                                        value={this.props.user.email_confirmation}
                                        onChangeText={email_confirmation => this.props.changed({email_confirmation})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Senha</Label>
                                <Item
                                    regular
                                    invalidated={'senha' in this.props.errors}>
                                    <Input
                                        value={this.props.user.senha}
                                        onChangeText={senha => this.props.changed({senha})}
                                        secureTextEntry
                                        placeholder='Mantenha em branco caso queira manter a mesma'
                                        placeholderTextColor={inputPlaceholderTextColor}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Confirmação da Senha</Label>
                                <Item
                                    regular
                                    invalidated={'senha_confirmation' in this.props.errors}>
                                    <Input
                                        value={this.props.user.senha_confirmation}
                                        onChangeText={senha_confirmation => this.props.changed({senha_confirmation})}
                                        secureTextEntry
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>RG</Label>
                                <Item
                                    regular
                                    invalidated={'rg' in this.props.errors}>
                                    <Input
                                        value={this.props.user.rg}
                                        onChangeText={rg => this.props.changed({rg})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>CPF</Label>
                                <Item
                                    regular
                                    invalidated={'cpf' in this.props.errors}>
                                    <InputMask
                                        value={this.props.user.cpf}
                                        mask='cpf'
                                        keyboardType='number-pad'
                                        onChangeText={cpf => this.props.changed({cpf})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Telefone</Label>
                                <Item
                                    regular
                                    invalidated={'telefone' in this.props.errors}>
                                    <InputMask
                                        value={this.props.user.telefone}
                                        mask='phone'
                                        keyboardType='number-pad'
                                        onChangeText={telefone => this.props.changed({telefone})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Telefone celular</Label>
                                <Item
                                    regular
                                    invalidated={'celular' in this.props.errors}>
                                    <InputMask
                                        value={this.props.user.celular}
                                        mask='phone'
                                        keyboardType='number-pad'
                                        onChangeText={celular => this.props.changed({celular})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                    </Grid>

                    <Text separator>ENDEREÇO</Text>

                    <Grid>
                        <Row>
                            <Col style={[styleColumnFirst, {width: '33%'}]}>
                                <Label>CEP</Label>
                                <Item
                                    regular
                                    invalidated={'cep' in this.props.errors}>

                                    <InputMask
                                        value={this.props.user.cep}
                                        mask='zipcode'
                                        keyboardType='number-pad'
                                        onChangeText={cep => this.props.changed({cep})}
                                        onBlur={this.onBlurCep.bind(this)}
                                    />
                                </Item>
                            </Col>
                            <Col style={[styleColumnSecond, {width: '67%'}]}>
                                <Label>Rua / Avenida</Label>
                                <Item
                                    regular
                                    invalidated={'endereco' in this.props.errors}>
                                    <Input
                                        value={this.props.user.endereco}
                                        onChangeText={endereco => this.props.changed({endereco})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Label>Complemento</Label>
                                <Item
                                    regular
                                    invalidated={'complemento' in this.props.errors}>
                                    <Input
                                        value={this.props.user.complemento}
                                        onChangeText={complemento => this.props.changed({complemento})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>N°</Label>
                                <Item
                                    regular
                                    invalidated={'numero' in this.props.errors}>
                                    <Input
                                        value={this.props.user.numero}
                                        onChangeText={numero => this.props.changed({numero})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Bairro</Label>
                                <Item
                                    regular
                                    invalidated={'bairro' in this.props.errors}>
                                    <Input
                                        value={this.props.user.bairro}
                                        onChangeText={bairro => this.props.changed({bairro})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Cidade</Label>
                                <Item
                                    regular
                                    invalidated={'cidade' in this.props.errors}>
                                    <Input
                                        value={this.props.user.cidade}
                                        onChangeText={cidade => this.props.changed({cidade})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Estado</Label>
                                <ItemPicker
                                    invalidated={'uf' in this.props.errors}
                                    selectedValue={this.props.user.uf}
                                    onValueChange={uf => this.props.changed({uf})}
                                    items={estados}
                                />
                            </Col>
                        </Row>
                    </Grid>

                    <Text separator>IMOBILIÁRIA</Text>

                    <Grid>
                        <Row>
                            <Col style={[styleColumnFirst, {width: '39%'}]}>
                                <Label>Tipo de Cadastro</Label>
                                <ItemPicker
                                    invalidated={'tipocadastro' in this.props.errors}
                                    selectedValue={this.props.user.tipocadastro}
                                    onValueChange={tipocadastro => this.props.changed({tipocadastro})}
                                    items={[
                                        {value: 'corretor', label: 'Corretor'},
                                        {value: 'estagiario', label: 'Estagiário'},
                                        {value: 'agency', label: 'Imobiliária'}
                                    ]}
                                />
                            </Col>

                            <Col style={[styleColumnSecond, {width: '61%'}]}>
                                <Label>Nome da Imobiliária</Label>
                                <Item
                                    regular
                                    invalidated={'razaosocial' in this.props.errors}>
                                    <Input
                                        value={this.props.user.razaosocial}
                                        onChangeText={razaosocial => this.props.changed({razaosocial})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        
                        <Row>
                            {this.props.user.tipocadastro === 'agency' && <Col style={styleColumnFirst}>
                                <Label>CNPJ</Label>
                                <Item
                                    regular
                                    invalidated={'cnpj' in this.props.errors}>
                                    <InputMask
                                        value={this.props.user.cnpj}
                                        mask='cnpj'
                                        keyboardType='number-pad'
                                        onChangeText={cnpj => this.props.changed({cnpj})}
                                    />
                                </Item>
                            </Col> }
                            <Col style={this.props.user.tipocadastro === 'agency' ? styleColumnSecond : null}>
                                <Label>Região de atuação</Label>
                                <ItemPicker
                                    invalidated={'cidades_atuacao' in this.props.errors}
                                    selectedValue={this.props.user.cidades_atuacao}
                                    onValueChange={cidades_atuacao => this.props.changed({cidades_atuacao})}
                                    items={ this.props.regioes.map(regiao => ({value: regiao.id, label: regiao.nome})) }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>CRECI</Label>
                                <Item
                                    regular
                                    invalidated={'creci' in this.props.errors}>

                                    <Input
                                        value={this.props.user.creci}
                                        onChangeText={creci => this.props.changed({creci})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>UF CRECI</Label>
                                <ItemPicker
                                    invalidated={'uf' in this.props.errors}
                                    selectedValue={this.props.user.creci_uf}
                                    onValueChange={creci_uf => this.props.changed({creci_uf})}
                                    items={estados} />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={this.props.user.por_onde_nos_conheceu === 'Outros' ? styleColumnFirst : null}>
                                <Label>Onde nos conheceu</Label>
                                <ItemPicker
                                    invalidated={'por_onde_nos_conheceu' in this.props.errors}
                                    selectedValue={this.props.user.por_onde_nos_conheceu}
                                    onValueChange={por_onde_nos_conheceu => this.props.changed({por_onde_nos_conheceu})}
                                    items={[
                                        {value: 'Facebook', label: 'Facebook'},
                                        {value: 'Instagram', label: 'Instagram'},
                                        {value: 'Google', label: 'Google'},
                                        {value: 'E-mail', label: 'E-mail'},
                                        {value: 'Indicação', label: 'Indicação'},
                                        {value: 'Site Helbor', label: 'Site Helbor'},
                                        {value: 'Linkedin', label: 'Linkedin'},
                                        {value: 'Ligação', label: 'Ligação'},
                                        {value: 'Outros', label: 'Outros'}
                                    ]}
                                />

                            </Col>
                            {this.props.user.por_onde_nos_conheceu === 'Outros' && <Col style={styleColumnSecond}>
                                <Label>Outro</Label>
                                 <Item
                                    regular
                                    invalidated={'outros_por_onde_nos_conheceu' in this.props.errors}>

                                    <Input
                                        value={this.props.user.outros_por_onde_nos_conheceu}
                                        onChangeText={outros_por_onde_nos_conheceu => this.props.changed({outros_por_onde_nos_conheceu})}
                                    />
                                </Item>

                            </Col>}
                        </Row>
                    </Grid>

                    <Button onPress={() => { this.props.save(this.props.user) }}>
                        <Text>SALVAR</Text>
                    </Button>

                    <DateTimePicker
                        isVisible={this.state.dateTimeVisible}
                        mode='date'
                        onConfirm={value => {
                            this.props.changed({nascimento: moment(value).format('YYYY-MM-DD')})
                            this.setState({dateTimeVisible: false})
                        }}
                        onCancel={() => this.setState({dateTimeVisible: false})}
                        date={this.props.user.nascimento ? moment(this.props.user.nascimento, 'YYYY-MM-DD').toDate() : moment().toDate()}
                        isDarkModeEnabled={false}
                    />

                </Form>

            </Default>
        )
    }
}

const mapStateToProps = state => ({
    imobiliarias: state.user.imobiliarias,
    user: state.user.user,
    errors: state.user.errors,
    regioes: state.regiao.regioes,
    auth: state.auth
})

const mapDispatchToProps = dispatch => bindActionCreators({
    searchImobiliarias,
    changed,
    edit,
    save,
    logout,
    regiaoSearch,
    cep
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Profile))
