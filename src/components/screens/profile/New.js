import React from 'react'
import {Linking} from 'react-native'

import {Col, Grid, Row} from 'react-native-easy-grid'
import {Button, Form, Input, Item, Label, Text, CheckBox} from 'native-base'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {changed, ERROR, save, SAVE_SUCCESS} from '../../../actions/user'
import {ERROR as AUTH_ERROR, login, LOGIN_SUCCESS} from '../../../actions/auth'
import {search as regiaoSearch} from '../../../actions/regiao'
import acessoResource from '../../../resources/acesso'

import store from '../../../store'

import Default from '../../ui/Default'
import Alert from '../../ui/Alert'
import InputMask from '../../ui/InputMask'
import ItemPicker from '../../ui/ItemPicker'
import HeaderDefault from '../../ui/HeaderDefault'

import {styleColumnFirst, styleColumnSecond, lightIntermediateColor} from '../../../theme/corretoron'
import ImageBackground from "../../ui/ImageBackground";

import {NewStyle as style} from '../../../styles'
import { termos_uso } from '../../../actions/content'
import { TouchableOpacity } from 'react-native-gesture-handler'

class New extends React.Component {

    componentDidMount() {

        if (this.props.regioes.length === 0) {
            this.props.regiaoSearch()
        }

        if (this.props.termos_uso === null) {
          this.props.termosUso()
        }
        
        this.unsubscribe = store.subscribe(action => {

            switch (action.type) {
                case SAVE_SUCCESS:
                    Alert.success('Cadastro realizado com sucesso.')
                    const {email, senha} = this.props.user

                    acessoResource.cadastrar({
                        corretor_id: action.payload.id
                    })

                    this.props.login({email, password: senha})
                    break
                case LOGIN_SUCCESS:
                    this.props.navigation.navigate('Home')
                    break
                case ERROR:
                    Alert.error(action.payload)
                    break
                case AUTH_ERROR:
                    console.log(action.payload)
                    break
            }

        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render() {
        return (
            <Default lighten header={[
                <ImageBackground top />,
                <HeaderDefault goBack />
            ]}>

                <Text separator>DADOS PESSOAIS</Text>

                <Form>

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Nome</Label>
                                <Item
                                    regular
                                    invalidated={'name' in this.props.errors}>
                                    <Input
                                        value={this.props.user.name}
                                        onChangeText={name => this.props.changed({name})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Sobrenome</Label>
                                <Item
                                    regular
                                    invalidated={'sobrenome' in this.props.errors}>
                                    <Input
                                        value={this.props.user.sobrenome}
                                        onChangeText={sobrenome => this.props.changed({sobrenome})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>E-mail</Label>
                                <Item
                                    regular
                                    invalidated={'email' in this.props.errors}>
                                    <Input
                                        keyboardType="email-address"
                                        autoCapitalize="none"
                                        value={this.props.user.email}
                                        onChangeText={email => this.props.changed({email})}
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Telefone</Label>
                                <Item
                                    regular
                                    invalidated={'telefone' in this.props.errors}>
                                    <InputMask
                                        value={this.props.user.telefone}
                                        mask='phone'
                                        keyboardType='number-pad'
                                        onChangeText={telefone => this.props.changed({telefone})}
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Região de atuação</Label>
                                <ItemPicker
                                    label='Região de atuação'
                                    invalidated={'cidades_atuacao' in this.props.errors}
                                    selectedValue={this.props.user.cidades_atuacao}
                                    onValueChange={cidades_atuacao => this.props.changed({cidades_atuacao})}
                                    items={this.props.regioes.map(regiao =>({value: regiao.id, label: regiao.nome}))} />
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Onde nos conheceu</Label>
                                <ItemPicker
                                    invalidated={'por_onde_nos_conheceu' in this.props.errors}
                                    selectedValue={this.props.user.por_onde_nos_conheceu}
                                    onValueChange={por_onde_nos_conheceu => this.props.changed({por_onde_nos_conheceu})}
                                    items={[
                                        {value: 'Facebook', label: 'Facebook'},
                                        {value: 'Instagram', label: 'Instagram'},
                                        {value: 'Google', label: 'Google'},
                                        {value: 'E-mail', label: 'E-mail'},
                                        {value: 'Indicação', label: 'Indicação'},
                                        {value: 'Site Helbor', label: 'Site Helbor'},
                                        {value: 'Linkedin', label: 'Linkedin'},
                                        {value: 'Ligação', label: 'Ligação'},
                                        {value: 'Outros', label: 'Outros'}
                                    ]}
                                />
                            </Col>
                        </Row>
                    </Grid>

                    {this.props.user.por_onde_nos_conheceu === 'Outros' && <Label>Outro</Label>}
                    {this.props.user.por_onde_nos_conheceu === 'Outros' && <Item
                        regular
                        invalidated={'outros_por_onde_nos_conheceu' in this.props.errors}>
                        <Input
                            value={this.props.user.outros_por_onde_nos_conheceu}
                            onChangeText={outros_por_onde_nos_conheceu => this.props.changed({outros_por_onde_nos_conheceu})}
                        />
                    </Item>}

                    <Grid>
                        <Row>
                            <Col style={styleColumnFirst}>
                                <Label>Senha</Label>
                                <Item
                                    regular
                                    invalidated={'senha' in this.props.errors}>
                                    <Input
                                        value={this.props.user.senha}
                                        onChangeText={senha => this.props.changed({senha})}
                                        secureTextEntry
                                    />
                                </Item>
                            </Col>
                            <Col style={styleColumnSecond}>
                                <Label>Confirme sua Senha</Label>
                                <Item
                                    regular
                                    invalidated={'senha_confirmation' in this.props.errors}>
                                    <Input
                                        value={this.props.user.senha_confirmation}
                                        onChangeText={senha_confirmation => this.props.changed({senha_confirmation})}
                                        secureTextEntry
                                    />
                                </Item>
                            </Col>
                        </Row>
                    </Grid>
                    
                    {this.props.termos_uso && <Grid style={style.termosGrid}>
                      <Row>
                        <Col style={style.termosFirstColumn}>
                          <CheckBox 
                            checked={this.props.user.termos_uso_aceito} 
                            color={lightIntermediateColor} 
                            onPress={() => this.props.changed({termos_uso_aceito: !this.props.user.termos_uso_aceito})}
                          />
                        </Col>
                        <Col>
                          <TouchableOpacity activeOpacity={.85} onPress={this.onPressTermosUso.bind(this)}>
                            <Label style={style.termosLabel}>Li e aceito os termos de uso e política de privacidade</Label>
                          </TouchableOpacity>
                        </Col>
                      </Row>
                    </Grid> }
                    
                    <Button onPress={() => { this.props.save(this.props.user) }}>
                        <Text>CADASTRAR</Text>
                    </Button>

                </Form>

            </Default>
        )
    }

    onPressTermosUso() {
      Linking.openURL(this.props.termos_uso.url)
    }
}

const mapStateToProps = state => ({ 
  user: state.user.user, 
  errors: state.user.errors, 
  regioes: state.regiao.regioes,
  termos_uso: state.content.termos_uso
})
const mapDispatchToProps = dispatch => bindActionCreators({ 
  changed, 
  save, 
  regiaoSearch, 
  login,
  termosUso: termos_uso 
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(New)
