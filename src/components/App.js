import React, { useEffect } from 'react'
import NetInfo from '@react-native-community/netinfo'
import { Root, StyleProvider } from 'native-base'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { AppLoading } from 'expo'

import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import store, { persistor } from '../store'
import theme from '../theme/corretoron'


/* Routes */
import Login from './screens/Login'

import Home from './screens/Home'

import NewProfile from './screens/profile/New'
import Profile from './screens/profile/Profile'
import NotificationSearch from './screens/notification/Search'

import EmpreendimentoSearch from './screens/empreendimento/Search'
import EmpreendimentoFilter from './screens/empreendimento/Filter'
import EmpreendimentoGallery from './screens/empreendimento/Gallery'
import EmpreendimentoDetail from './screens/empreendimento/Detail'
import EmpreendimentoTabelas from './screens/empreendimento/Tabelas'
import EspelhoSearch from './screens/espelho/Search'
import EspelhoBlocos from './screens/espelho/Blocos'
import EspelhoUnidades from './screens/espelho/Unidades'

import Visita from './screens/empreendimento/Visita'
import Proposta from './screens/empreendimento/Proposta'
import Agenda from './screens/agenda/Agenda'
import Compromisso from './screens/agenda/Compromisso'

import PickerList from './ui/PickerList'

/* End Routes */

import Navigation from '../services/Navigation'

import AppStatus from './ui/AppStatus'

function AuthNavigation (props) {

    useEffect(() => {

        props.navigation.navigate(store.getState().auth.authenticated ? 'App' : 'Auth')
        const unsubscribe = NetInfo.addEventListener(connectionInfo => {
            store.dispatch({type: connectionInfo.isConnected ? 'SYSTEM_ONLINE' : 'SYSTEM_OFFLINE'})
        })

        return () => {
            unsubscribe()
        }
    })

    return <AppLoading />
}

const AppStack = createStackNavigator({
    Home,
    Profile,
    NotificationSearch,

    EmpreendimentoSearch,
    EmpreendimentoDetail,
    EmpreendimentoFilter,
    EmpreendimentoGallery,
    EmpreendimentoTabelas,

    EspelhoSearch,
    EspelhoBlocos,
    EspelhoUnidades,

    Visita,
    Proposta,

    Agenda,
    Compromisso,

    PickerList
}, {
    headerMode: 'none'
})

const AuthStack = createStackNavigator({
    Login,
    NewProfile,
    PickerList
}, {
    headerMode: 'none'
})

const AppContainer = createAppContainer(
    createSwitchNavigator(
        { AuthNavigation, App: AppStack,  Auth: AuthStack },
        { initialRouteName: 'AuthNavigation'  }
    )
)

export default function () {
    return <Provider store={ store }>
    <PersistGate loading={ null } persistor={ persistor }>
        <StyleProvider style={ theme }>
            <Root>
                <AppStatus />
                <AppContainer ref={topLevelNavigator => {
                    Navigation.setTopLevelNavigator(topLevelNavigator)
                }} />
            </Root>
        </StyleProvider>
    </PersistGate>
</Provider>

}
