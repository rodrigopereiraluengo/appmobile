import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity } from 'react-native'
import { Text } from 'native-base'


class LinkButton extends React.Component {

    render() {
        return <TouchableOpacity
                activeOpacity={.93}
                style={{alignSelf: 'flex-'.concat(this.props.align)}}
                onPress={() => { this.props.onPress() }}>
            <Text linkButton style={{...this.props.textStyle}}>{this.props.title} &gt;</Text>
        </TouchableOpacity>
    }

}

LinkButton.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    align: PropTypes.oneOf(['start', 'end'])
}

LinkButton.defaultProps = {
    title: 'Unnamed',
    onPress: () => {  },
    align: 'end'
}

export default LinkButton
