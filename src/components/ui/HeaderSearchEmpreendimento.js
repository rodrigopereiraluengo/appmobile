import React from 'react'
import PropTypes from 'prop-types'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {withNavigation} from 'react-navigation'

import store from '../../store'

import {search, SEARCH_SUCCESS, ERROR} from '../../actions/empreendimento'
import HeaderSearch from './HeaderSearch'

class HeaderSearchEmpreendimento extends React.Component {

    componentDidMount() {
        this.unsubscribe = store.subscribe(({type}) => {
            const { routeName } = this.props.navigation.state
            if ((type === SEARCH_SUCCESS || type === ERROR) && routeName !== 'EmpreendimentoSearch') {
                this.props.navigation.navigate('EmpreendimentoSearch')
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render() {
        return (
            <HeaderSearch
                placeholder='Digite para pesquisar em produtos'
                value={this.props.parameters.query}
                onChangeText={query => this.props.search({query, page: null})}
            />
        )
    }

}

HeaderSearchEmpreendimento.defaultProps = {
    placeholder: 'Digite para pesquisar em produtos'
}

HeaderSearchEmpreendimento.propTypes = {
    placeholder: PropTypes.string
}

const mapStateToProps = state => ({
    parameters: state.empreendimento.parameters
})

const mapDispatchToProps = dispatch => bindActionCreators({ search }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(HeaderSearchEmpreendimento))
