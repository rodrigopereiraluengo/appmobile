import React from 'react'
import {
  Keyboard,
  SafeAreaView,
  ScrollView,
  View,
  Animated
} from 'react-native'

import { StatusBar } from 'expo-status-bar'

import { Container } from 'native-base'
import _ from 'lodash'

import store from '../../store'

import Loading from './Loading'
import OffLine from './OffLine'
import StatusBarColor from './StatusBarColor'
import { SCROLL_TOP, HTTP_REQUEST_BEGIN, HTTP_REQUEST_END } from '../../actions/system'

import {DefaultStyle as style} from '../../styles'
import {darkColor} from '../../theme/corretoron'
import {PLATFORM} from 'native-base/src/theme/variables/commonColor'

class Default extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      isKeyboardShow: false
    }

    this._refs = {}

    this.callOnScrollEnd = 'onScrollEnd' in this.props && typeof this.props.onScrollEnd === 'function'
    this.previousYPosition = 0
    this.scrollDown = false

    this.heightAnimation = new Animated.Value(0)
    this.state = {
      viewScrollTopStyle: {
        top: 0, 
        height: 0 
      },
      styleSafeAreaViewLighten: style.SafeAreaViewLighten
    }
  }

    componentDidMount() {
        
        this.unsubscribe = store.subscribe(action => {

            if (action.type === SCROLL_TOP) {
                this.scrollTop()
            }

            if (action.type === HTTP_REQUEST_BEGIN) {
              this.setState({styleSafeAreaViewLighten: style.SafeAreaViewLoading})
            }

            if (action.type === HTTP_REQUEST_END) {
              this.setState({styleSafeAreaViewLighten: style.SafeAreaViewLighten})
            }
        })

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this))
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this))
    }

    componentWillUnmount() {
        this.unsubscribe()
        this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener.remove()
    }

    render () {
        
        return (
            <React.Fragment>

                <SafeAreaView style={[style.SafeAreaView0, this.state.styleSafeAreaViewLighten]} />
                <SafeAreaView style={style.SafeAreaView1}>
                    <Container>

                        {'header' in this.props && this.props.header.map((HeaderComponent, i) => {
                            return <React.Fragment key={i}>{HeaderComponent}</React.Fragment>
                        })}

                        <Loading />
                        
                        <OffLine />

                        <View style={[style.ViewScrollTop, this.state.viewScrollTopStyle]} /> 

                        <ScrollView
                            ref={ el => this._refs['scrollView'] = el }
                            onScroll={this.onScroll.bind(this)}
                            scrollEventThrottle={200}
                            style={[style.ScrollView]}>
                            
                            <View style={style.View}>
                                {this.props.children}                                
                            </View>
                            
                        </ScrollView>

                        {'footer' in this.props && <View style={this.styleViewFooter()}>
                            {this.props.footer.map((FooterComponent, i) => {
                                return <React.Fragment key={i}>{FooterComponent}</React.Fragment>
                            })}
                        </View>}

                        <StatusBarColor />
                    </Container>
                </SafeAreaView>
                <SafeAreaView style={('lighten' in this.props) ? this.state.styleSafeAreaViewLighten : style.SafeAreaView2} />
            </React.Fragment>
        )
    }

    styleViewFooter() {

        if (this.state.isKeyboardShow) {
            return {
                height: 0
            }
        }

        return {}
    }

    onScroll ({ nativeEvent }) {

        this._refs['scrollView'].measure( (fx, fy, width, height, px, py) => {
        
            this.setState({viewScrollTopStyle: {
                top: Platform.OS === PLATFORM.IOS ? fy : py,
                height: nativeEvent.contentOffset.y > 0 ? .5 : 0
            }})

        })

        if (this.callOnScrollEnd === false) {
            return
        }

        clearTimeout(this.toOnScroll)
        this.toOnScroll = setTimeout(() => {
            const height = parseInt(nativeEvent.contentSize.height)
            const toleranceHeightLayout = height * .1
            const y = parseInt(nativeEvent.contentOffset.y + nativeEvent.layoutMeasurement.height)
            const scrollY = height - (y + toleranceHeightLayout)

            this.scrollDown = nativeEvent.contentOffset.y > this.previousYPosition
            this.previousYPosition = nativeEvent.contentOffset.y

            if (_.isFunction(this.props.onScrollEnd) && scrollY <= 0 && this.scrollDown) {
                this.callOnScrollEnd = false
                this.props.onScrollEnd(nativeEvent)
                this.callOnScrollEnd = true
            }
        }, 300)
    }

    scrollTo (y) {
        this._refs['scrollView'].scrollTo({x: 0, y, animated: true})
    }

    scrollTop () {
        this.scrollTo(0)
    }

    _keyboardDidShow () {
        this.setState({isKeyboardShow: true})
    }

    _keyboardDidHide () {
        this.setState({isKeyboardShow: false})
    }
}

export default Default
