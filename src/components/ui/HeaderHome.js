import React from 'react'
import { Body, Header, Left, Right } from 'native-base'
import { Image } from 'react-native'

import splash from '../../../assets/images/splash.png'

import { HeaderHomeStyle as style } from '../../styles'

export default function HeaderHome () {

    return <Header>
        <Body style={style.Body}>
            <Image source={splash} style={style.Image} />
        </Body>
    </Header>
}
