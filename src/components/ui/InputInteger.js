import React from 'react'
import InputNumber from './InputNumber'

export default class InputInteger extends React.Component {

    render() {
        return (
            <InputNumber {...this.props} decimalPlaces={0} />
        )
    }

}
