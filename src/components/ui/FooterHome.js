import React from 'react'
import {Footer, Left, Right, Body, Text} from 'native-base'
import {Image} from 'react-native'

import helbor from '../../../assets/images/helbor.png'

import {FooterHomeStyle as style} from '../../styles'

export default function FooterHome () {
    return <Footer style={style.Footer}>
            <Body style={style.Body}>
                <Image source={helbor} style={style.Image} />
            </Body>
    </Footer>
}
