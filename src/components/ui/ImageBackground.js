import React from 'react'
import {Image} from 'react-native'
import {ImageBackgroundStyle as style} from '../../styles'
import background from '../../../assets/images/triangulos.png'

export default function ImageBackground (props) {
    return <Image source={background} style={style[(('top' in props) ? 'Top' : 'Image')]} />
}
