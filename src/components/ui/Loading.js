import React from 'react'
import { Spinner, View } from 'native-base'
import { connect } from 'react-redux'

import { LoadingStyle as style } from '../../styles'

class Loading extends React.Component {

    render () {
        if (this.props.loading) {
            return (
                <View style={style.View}>
                    <Spinner color={style.Spinner.color} style={style.Spinner} />
                </View>
            )
        }
        return null
    }

}

const mapStateToProps = state => ({
    loading: state.system.loading
})

export default connect(mapStateToProps)(Loading)
