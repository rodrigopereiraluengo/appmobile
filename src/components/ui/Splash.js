import React from 'react'
import {Image} from 'react-native'
import splash from '../../../assets/images/splash.png'
import {SplashStyle as style} from '../../styles'

export default function Splash (props) {
    return (
        <Image source={splash} style={[style.Image, {...props.style}]} />
    )
}
