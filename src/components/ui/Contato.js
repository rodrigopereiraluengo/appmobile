import React from 'react'
import {Alert, Image, Linking, Platform, TouchableOpacity} from 'react-native'
import {Button, Fab, Icon} from 'native-base'
import {PLATFORM} from 'native-base/src/theme/variables/commonColor'

import {connect} from 'react-redux'

import contato from '../../../assets/images/contato.png'

import {ContatoStyle as style} from '../../styles'
import { calcHeight } from "../../theme/corretoron"

class Contato extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            activated: true,
            buttonMarginTop: Platform.OS === PLATFORM.ANDROID ? 0 : calcHeight(-40),
            buttonWidth: 0,
            buttonHeight: 0
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ activated: false })
        }, 25)
    }

    render () {

        if (this.props.atendente) {
            return <Fab active={this.state.activated} style={[style.Fab]} containerStyle={style.containerStyle} direction="up">

            <TouchableOpacity activeOpacity={.93} onPress={this.collapse.bind(this)} style={style.TouchableOpacity}>
                <Image source={contato} style={style.Thumbnail} />
            </TouchableOpacity>

            <Button style={[style.Button, style.ButtonPhone, {marginBottom: this.state.buttonMarginTop, width: this.state.buttonWidth, height: this.state.buttonHeight}]} onPress={this.phone.bind(this)}>
                <Icon name="phone" type='FontAwesome' />
            </Button>

            <Button style={[style.Button, style.ButtonWhatsApp, {marginBottom: this.state.buttonMarginTop, width: this.state.buttonWidth, height: this.state.buttonHeight}]} onPress={this.whatsApp.bind(this)}>
                <Icon name="logo-whatsapp" />
            </Button>

            <Button style={[style.Button, style.ButtonEmail, {marginBottom: this.state.buttonMarginTop, width: this.state.buttonWidth, height: this.state.buttonHeight}]} onPress={this.email.bind(this)}>
                <Icon name="mail" />
            </Button>

        </Fab>
        } else {
            return null
        }

        
    }

    collapse () {

        let buttonMarginTop = Platform.OS === PLATFORM.ANDROID ? 0 : calcHeight(-40)
        let buttonWidth = 0
        let buttonHeight = 0
        if (Platform.OS === PLATFORM.IOS && !this.state.activated) {
            buttonMarginTop = 0
        }

        if (!this.state.activated) {
            buttonWidth = style.Button.width
            buttonHeight = style.Button.height
        }

        this.setState({
            activated: !this.state.activated,
            buttonMarginTop,
            buttonWidth,
            buttonHeight
        })
    }

    phone () {

        Linking.openURL('tel:' + this.props.atendente.telefone.replace(/[^0-9]/g, ''))

    }

    whatsApp () {

        const URL = Platform.OS === PLATFORM.ANDROID ? 'whatsapp://send?text=Olá ' + this.props.atendente.name.trim().split(' ')[0] + ', &phone=+55' + this.props.atendente.telefone.replace(/[^0-9]/g, '') : 'https://api.whatsapp.com/send?phone=55' + this.props.atendente.telefone.replace(/[^0-9]/g, '') + '&text= Olá ' + this.props.atendente.name.trim().split(' ')[0] + ', &source=&data='
        Linking.canOpenURL(URL).then(supported => {

            if (!supported) {
                return Alert.alert('Whats App', 'Para enviar mensagem direto ao seu atendente, é necessário instalar o Whats App.')
            } else {
                return Linking.openURL(URL)
            }

        })

    }

    email () {

        const URL = 'mailto:' + this.props.atendente.email
        Linking.canOpenURL(URL).then(supported => {

            if (!supported) {
                return Alert.alert('E-mail', 'Para enviar e-mail ao seu atendente, é necessário instalar e configurar um cliente de E-mail.')
            }

            return Linking.openURL(URL)

        })

    }
}

const mapStateToProps = state => ({
    atendente: state.auth.auth.user.atendente
})

export default connect(mapStateToProps)(Contato)
