import React from 'react'
import * as Location from 'expo-location'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { position } from '../../actions/user'
import resource from '../../resources/user'

import * as TaskManager from 'expo-task-manager'

const GEOLOCATION_TASKNAME = 'br.com.corretoron.geolocation'

class GeolocationPosition extends React.Component {

    async componentDidMount() {

        let permission = await Location.getPermissionsAsync()
        const hasServiceEnabled = await Location.hasServicesEnabledAsync()

        if (permission.granted === false && permission.canAskAgain === true) {
            permission = await Location.requestPermissionsAsync()
        }

        if (permission.granted === true && hasServiceEnabled === false) {
            await Location.startLocationUpdatesAsync(GEOLOCATION_TASKNAME, {
                accuracy: Location.Accuracy.Low,
                timeInterval: 20000
            })
        }
        
        if (permission.granted) {
            await Location.watchPositionAsync({
                accuracy: Location.Accuracy.Low,
                timeInterval: 20000
            }, response => {
                this.props.position(response)
            })
        }
    }

    render () {
        return <React.Fragment />
    }
}

const mapStateToProps = state => ({
    user: state.auth.auth.user
})

const mapDispatchToProps = dispatch => bindActionCreators({ position }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(GeolocationPosition)

TaskManager.defineTask(GEOLOCATION_TASKNAME, ({data, error}) => {
    
    if (error) {
        return
    }

    if (data) {
        const { locations } = data
        resource.position(locations)
    }
})
