import React from 'react'
import { Vibration, Linking } from 'react-native'
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants'
import * as Notifications from 'expo-notifications'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    notification_token,
    notification_receive,
    notification_action,
    notifications,
    total_notifications
} from '../../actions/user'
import { detail } from '../../actions/empreendimento'
import Navigation from '../../services/Navigation'

import AsyncStorage from '@react-native-async-storage/async-storage'

class Notification extends React.Component {
    
    registerForPushNotificationsAsync = async () => {

        if (Constants.isDevice) {
            const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
            let finalStatus = existingStatus

            if (existingStatus !== 'granted') {
                const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
                finalStatus = status
            }

            if (finalStatus !== 'granted') {
                return
            }
            
            const { data: token } = await Notifications.getExpoPushTokenAsync()
            
            this.props.notification_token({notification_token: token})
        }
        
        Notifications.setNotificationHandler({
            handleNotification: async notification => {
                Vibration.vibrate()

                this.props.notification_receive(notification.request.content)
                this.props.total_notifications()
                this.props.notifications({q:this.props.q, page: 1})
                return {
                    shouldShowAlert: true,
                    shouldPlaySound: true,
                    shouldSetBadge: true,
                }
            }
        })

        Notifications.addNotificationResponseReceivedListener(async response => {
            this._handleNotification(response.notification.request.content)
        })
    }

    _handleNotification (notification) {

        this.props.notification_action(notification)
        
        if ('data' in notification && 'action' in notification.data && notification.data.action) {
            switch (notification.data.action) {
                case 'screen':
                    Navigation.navigate(notification.data.screen)
                    break
                case 'link':
                    Linking.openURL(notification.data.link)
                    break
                default:
                    setTimeout(() => this.props.detail(notification.data.imovel_id), 500)
            }
        }

        setTimeout(async () => {
            await AsyncStorage.removeItem('handle_notification')
        }, 2000)

        
    }

    async componentDidMount() {
        await this.registerForPushNotificationsAsync()
        this.verifyNofifications()
    }
    
    verifyNofifications() {
        
        AsyncStorage.getItem('handle_notification', async (error, result) => {

            if (!error && result) {
                const notification = JSON.parse(result)
                this._handleNotification(notification)
            }
        })
    }
    
    render () {
        return null
    }
}

const mapStateToProps = state => ({
    state: state.system.state,
    q: state.user.notifications.q
})

const mapDispatchToProps = dispatch => bindActionCreators({
    notification_token,
    notification_receive,
    notification_action,
    notifications,
    total_notifications,
    detail
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Notification)
