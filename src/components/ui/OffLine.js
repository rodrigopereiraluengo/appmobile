import React from 'react'
import {Icon, Text, View} from 'native-base'
import {connect} from 'react-redux'

import {OffLineStyle as style} from '../../styles'

class OffLine extends React.Component {

    render() {

        if (this.props.offline) {
            return <View style={style.View}>
                <Icon type='FontAwesome' name='wifi' style={style.Icon} />
                <Text style={style.Text}>Você está desconectado!</Text>
            </View>
        }

        return null
    }
}

const mapStateToProps = state => ({
    offline: state.system.offline
})

export default connect(mapStateToProps)(OffLine)
