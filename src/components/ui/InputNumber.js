import React from 'react'
import { Input } from 'native-base'

import PropTypes from 'prop-types'

import _ from 'lodash'

import Numeral from './Numeral'


class InputNumber extends React.Component {

    constructor(props) {
        super(props)

        this.onChangeText = this.onChangeText.bind(this)
        this.onBlur = this.onBlur.bind(this)
        this.state = {value: null, formatted: ''}
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        const value = _.get(nextProps, 'value')
        this.setState({value, formatted: value ? this.formattedValue(value) : ''})
    }

    render() {

        return (
            <Input
                {...this.props}
                keyboardType='number-pad'
                value={this.state.value ? this.state.formatted : ''}
                onChangeText={this.onChangeText}
                onBlur={this.onBlur} />
        )
    }

    formattedValue(value) {
        const decimalPlaces = _.get(this.props, 'decimalPlaces', 2)
        const numeral = Numeral(value)
        return numeral
                .format('0,0'.concat(decimalPlaces > 0 ? '.'.concat('0'.repeat(decimalPlaces)) : ''))
    }

    onChangeText(value) {
        this.setState({
            value: _.isEmpty(value) ? null : Numeral(this.formattedValue(value)).value(),
            formatted: value
        })
    }

    onBlur() {
        const formatted = this.state.value ? this.formattedValue(this.state.value) : ''
        this.setState({formatted})
        if (_.get(this.props, 'onChangeText') && _.isFunction(this.props.onChangeText)) {
            this.props.onChangeText(this.state.value)
        }
    }

}

InputNumber.propTypes = {
    decimalPlaces: PropTypes.number,
    value: PropTypes.number,
    onChangeText: PropTypes.func
}

export default InputNumber
