import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { Body, Button, Header, Left, Right } from 'native-base'

import { withNavigation } from 'react-navigation'

import splash from '../../../assets/images/splash.png'
import Menu from '../../../assets/images/menu.png'

import { HeaderDefaultStyle as style } from '../../styles'

import LeftArrow from './LeftArrow'

function HeaderDefault(props) {

    return <Header>
            <Left style={style.Left}>
                { ('goBack' in props) && <TouchableOpacity activeOpacity={.85} style={style.Button} onPress={ () => props.navigation.goBack() }>
                    <LeftArrow />
                </TouchableOpacity>}
                { ('goHome' in props) && <TouchableOpacity activeOpacity={.85} style={style.Button} onPress={ () => props.navigation.navigate('Home') }>
                    <Image style={style.ImageHome} source={Menu} />
                </TouchableOpacity>}
            </Left>
            <Body style={style.Body}>
            </Body>
            <Right style={style.Right}>
                <Image source={splash} style={style.ImageAppOn} />
            </Right>
        </Header>
}

export default withNavigation(HeaderDefault)
