import React from 'react'
import PropTypes from 'prop-types'
import msk from 'msk'

import { Input } from 'native-base'

class InputMask extends React.Component {

    render () {
        return <Input {...this.props} onChangeText={this.onChangeText.bind(this)} />
    }

    onChangeText(value) {

        let mask = this.props.mask

        if (mask === false) {
            return this.props.onChangeText(value)
        }

        const masks = {
            "phone_14": "(99) 9999-9999",
            "phone_15": "(99) 99999-9999",
            "zipcode": "99999-999",
            "date": "99/99/9999",
            "date-time": "99/99/9999 99:99",
            "cpf": "999.999.999-99",
            "cnpj": "99.999.999/9999-99",
        }

        if (mask in masks) {
            mask = masks[mask]
        }

        if (mask === 'phone') {
            mask = masks[value.length < 15 ? 'phone_14' : 'phone_15']
        }

        this.props.onChangeText(msk.fit(value, mask))
    }
}

InputMask.propTypes = {
    mask: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.bool.isRequired]),
    onChangeText: PropTypes.func
}

export default InputMask
