import React from 'react'
import { Platform, TouchableOpacity } from 'react-native'
import { Body, Header, Text, Icon, Left, Picker as NBPicker, Right, Title } from 'native-base'
import PropTypes from 'prop-types'
import { PLATFORM } from 'native-base/src/theme/variables/commonColor'

import { show } from '../../actions/picker-list'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { HeaderDefaultStyle, NBPickerStyle, PickerStyle } from "../../styles"
import LeftArrow from './LeftArrow'

import { withNavigation } from 'react-navigation'

const dropDownIcon = Platform.OS === PLATFORM.ANDROID ? 'arrow-dropdown' : 'arrow-down'
class Picker extends React.Component {
  

    onPress () {
        this.props.show({
            title: this.props.label,
            items: this.props.items,
            selectedValue: this.props.selectedValue,
            onValueChange: this.props.onValueChange
        })
    }

    getSelectedText() {
        const selectedText = this.props.items.filter(item => item.value === this.props.selectedValue)
        if (selectedText.length) {
            return selectedText[0].label
        }
        
        return this.props.placeholder
    }

    render () {
        
        // if (Platform.OS === PLATFORM.ANDROID) {
            return <TouchableOpacity activeOpacity={.85} style={PickerStyle.TouchableOpacity} onPress={this.onPress.bind(this)}>
                <Text style={PickerStyle.Text}>{ this.getSelectedText() }</Text>
                <Icon name={dropDownIcon} style={PickerStyle.Icon}></Icon>
            </TouchableOpacity>
        // }

        // const style = {
        //     ...this.props.style,
        //     borderRadius: 0,
        //     borderColor: 'transparent'
        // }

        // return (
        //     <NBPicker
        //         textStyle={NBPickerStyle.textStyle}
        //         itemStyle={NBPickerStyle.itemStyle}
        //         itemTextStyle={NBPickerStyle.itemTextStyle}
                  
        //         {...this.props}
        //         style={style}
        //         iosIcon={<Icon name="arrow-down" style={NBPickerStyle.iosIcon} />}
        //         renderHeader={backAction =>
        //             <Header>
        //               <Left style={HeaderDefaultStyle.Left}>
        //                 <TouchableOpacity activeOpacity={.85} onPress={backAction}>
        //                     <LeftArrow />
        //                 </TouchableOpacity>
        //               </Left>
        //               <Body style={NBPickerStyle.Body}>
        //                 <Title style={NBPickerStyle.Title}>{this.props.label}</Title>
        //               </Body>
        //               <Right />
        //             </Header>}>
        //         {this.props.items.map((item, i) => <NBPicker.Item key={i} {...item} />)}
        //     </NBPicker>
        // )

    }
}

Picker.defaultProps = {
    placeholder: 'Selecione...',
    label: 'Selecione uma opção',
    mode: 'dropdown',
    items: [],
    selectedValue: null
}

Picker.propTypes = {
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onValueChange: PropTypes.func,
    items: PropTypes.array
}

const mapDispatchToProps = dispatch => bindActionCreators({
    show
}, dispatch)

export default connect(null, mapDispatchToProps)(withNavigation(Picker))
