import numeral from 'numeral'

// load a locale
numeral.register('locale', 'br', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème'
    },
    currency: {
        symbol: 'R$'
    }
})

// switch between locales
numeral.locale('br')

export default numeral
