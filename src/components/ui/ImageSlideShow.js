import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {Image, ScrollView, View} from 'react-native'
import {Col, Grid} from 'react-native-easy-grid'
import {ImageSlideShowStyle as style} from '../../styles'
import {Spinner, Text} from 'native-base'

function LazyImage (props) {

    const [loaded, setLoaded] = useState(false)

    return <View style={style.View}>
        {
            (props.position in props.dataSource) && (props.dataSource[props.position].load === true) && <Image source={props.source} style={style.Image} onLoad={() => { setTimeout(() => setLoaded(true), 500) }} /> }

        {loaded === false && <Spinner color={style.Spinner.color} style={style.Spinner} />}

    </View>
}

class ImageSlideShow extends React.Component {

    constructor (props) {
        super (props)

        this.toOnScroll = null
        this.isScrollViewOnLayout = false
        this.x = []
        this.state = {dataSource: []}
    }

    render() {

      if (this.props.images.length === 0) {
        return <Text>Nenhuma imagem encontrada.</Text>
      }

        return <ScrollView style={style.ScrollView}
                           horizontal={true}
                           showsHorizontalScrollIndicator={false}
                           scrollEventThrottle={200}
                           decelerationRate='fast'
                           onLayout={this.scrollViewOnLayout.bind(this)}
                           onScroll={this.onScroll.bind(this)} >
            <Grid>
                { this.props.images.map((uri, i) => (
                    <Col
                        key={i}
                        style={style.Col}
                        onLayout={ e => this.x[i] = e.nativeEvent.layout.x }>

                        <LazyImage dataSource={this.state.dataSource} position={i} source={{uri}} />
                    </Col>
                )) }
            </Grid>
        </ScrollView>
    }

    scrollViewOnLayout ({nativeEvent}) {
        if (this.x.length === this.props.images.length && this.isScrollViewOnLayout === false) {
            this.isScrollViewOnLayout = true
            this.setState({ dataSource: this.x.map(x => ({x, load: x <= nativeEvent.layout.width})) })
        }
    }

    onScroll({ nativeEvent }) {

        clearTimeout(this.toOnScroll)
        this.toOnScroll = setTimeout(() => {
            const start = nativeEvent.contentOffset.x + nativeEvent.layoutMeasurement.width
            const end = nativeEvent.contentOffset.x - nativeEvent.layoutMeasurement.width

            this.setState({
                dataSource: this.x.map(x => ({x, load: start >= x && end <= x}))
            })

        }, 300)
    }

}

ImageSlideShow.propTypes = {
    images: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.shape({ uri: PropTypes.string.isRequired })),
        PropTypes.arrayOf(PropTypes.string)
    ]).isRequired
}

export default ImageSlideShow