
import { Toast } from 'native-base'

export default class Alert {

    static error (message) {

        if (typeof message === 'object') {
            for (const key in message) {
                message = message[key]
                if (typeof message === 'array') {
                    message = message[0]
                }
                break
            }
        }

        Toast.show({
            position: 'top',
            text: message,
            buttonText: 'Ok',
            type: 'danger',
            duration: 3000
        })

    }

    static info(message) {
        Toast.show({
            position: 'top',
            text: message,
            buttonText: "Ok"
        })
    }

    static success (message) {
        Toast.show({
            position: 'top',
            text: message,
            buttonText: "Ok",
            type: "success"
        })
    }
}
