import React from 'react';
import { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';

import store from '../../store';
import { HTTP_REQUEST_BEGIN, HTTP_REQUEST_END } from '../../actions/system';

export default function StatusBarColor() {

  const [backgroundColor, setBackgroundColor] = useState('#ffffff');

  useEffect(() => {
    return store.subscribe(action => {

      if (action.type === HTTP_REQUEST_BEGIN) {
        setBackgroundColor('#829cad');
      }

      if (action.type === HTTP_REQUEST_END) {
        setBackgroundColor('#ffffff');
      }

    })
  })

  return <StatusBar style="dark" backgroundColor={backgroundColor} translucent={false} />
}