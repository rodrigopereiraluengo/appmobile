import React from 'react'
import PropTypes from 'prop-types'
import {Col, Grid, Row} from 'react-native-easy-grid'
import {Text} from 'native-base'
import LinkButton from './LinkButton'

class Separator extends React.Component {

    render () {

        let colLeftWith = this.props.titleWidth
        let colRightWith = this.props.buttonWidth
        if (this.props.button === null) {
            colLeftWith = '100%'
        }

        return (
            <Grid>
                <Row>
                    <Col style={{width: colLeftWith}}>
                        <Text separator>{this.props.title}</Text>
                    </Col>
                    {this.props.button !== null && <Col style={{width: colRightWith}}>
                        <LinkButton
                            title={this.props.button}
                            onPress={() => { this.props.onPress() }}
                        />
                    </Col>}
                </Row>
            </Grid>
        )
    }

}

Separator.defaultProps = {
    button: null,
    onPress: function () { },
    titleWidth: '70%',
    buttonWidth: '30%'
}

Separator.propTypes = {
    title: PropTypes.string.isRequired,
    button: PropTypes.string,
    onPress: PropTypes.func,
    titleWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    buttonWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
}

export default Separator
