import React from 'react'
import {Image} from 'react-native'
import {Button, Footer, FooterTab} from 'native-base'
import {withNavigation} from 'react-navigation'

import ImageProdutos from '../../../assets/images/footer_produtos.png'
import ImageTabelas from '../../../assets/images/footer_tabelas.png'
import ImageEspelho from '../../../assets/images/footer_espelho.png'
import ImageAgenda from '../../../assets/images/footer_agenda.png'
import ImageUser from '../../../assets/images/footer_user.png'

import {FooterDefaultStyle as style} from '../../styles'

const FooterDefault = function ({ navigation }) {
    return (
        <Footer style={style.Footer}>
            <FooterTab>

                <Button style={style.ButtonRightWidth} onPress={() => {
                    navigation.navigate('EmpreendimentoSearch')
                } }>
                    <Image source={ImageProdutos} style={style.ImageProdutosStyle} />
                </Button>

                <Button style={[style.ButtonRightWidth, style.ButtonLeftWidth]} onPress={ () => {
                    navigation.navigate('EmpreendimentoTabelas')
                } }>
                    <Image source={ImageTabelas} style={style.ImageTabelasStyle} />
                </Button>

                <Button style={[style.ButtonRightWidth, style.ButtonLeftWidth]} onPress={ () => {
                    navigation.navigate('EspelhoSearch')
                } }>
                    <Image source={ImageEspelho} style={style.ImageEspelhoStyle} />
                </Button>
                <
                    Button style={[style.ButtonRightWidth, style.ButtonLeftWidth]} onPress={ () => {
                    navigation.navigate('Agenda')
                } }>
                    <Image source={ImageAgenda} style={style.ImageAgendaStyle} />
                </Button>

                <Button style={style.ButtonLeftWidth} onPress={ () => {
                    navigation.navigate('Profile')
                } }>
                    <Image source={ImageUser} style={style.ImageUserStyle} />
                </Button>

            </FooterTab>
        </Footer>
    )
}

export default withNavigation(FooterDefault)
