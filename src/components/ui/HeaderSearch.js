import React from 'react'
import PropTypes from 'prop-types'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Body, Header, Icon, Input, Item} from 'native-base'

import {scrollTop} from '../../actions/system'

import {darkInputPlaceholderDarkerTextColor, lightMonoColor} from '../../theme/corretoron'
import {HeaderSearchStyle as style} from '../../styles'

class HeaderSearch extends React.Component {

    constructor(props) {
        super(props)

        this.toOnChangeText = null
    }

    render() {
        return (
            <Header search>
                <Body>
                    <Item green style={style.Item}>
                        <Icon name='md-search' />
                        <Input
                            placeholder={this.props.placeholder}
                            placeholderTextColor={lightMonoColor}
                            value={this.props.value}
                            onChangeText={this.onChangeText.bind(this)}
                        />
                    </Item>
                </Body>
            </Header>
        )
    }

    onChangeText (value) {

        this.props.onChangeText(value)

        clearTimeout(this.toOnChangeText)
        this.toOnChangeText = setTimeout(() => this.props.scrollTop(), 500)

    }

}

HeaderSearch.defaultProps = {
    placeholder: 'Digite para pesquisar',
    value: '',
    onChangeText: function (value) {
        console.log(value)
    }
}

HeaderSearch.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChangeText: PropTypes.func
}

const mapDispatchToProps = dispatch => bindActionCreators({ scrollTop }, dispatch)

export default connect(null, mapDispatchToProps)(HeaderSearch)
