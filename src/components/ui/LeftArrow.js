import React from 'react'
import {Image} from 'react-native'

import LeftArrowImage from '../../../assets/images/left-arrow.png'
import {LeftArrowStyle as style} from '../../styles'

export default class LeftArrow extends React.Component {

    render () {
        return <Image style={style.Image} source={LeftArrowImage} />
    }

}
