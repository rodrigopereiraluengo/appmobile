import React from 'react'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {withNavigation} from 'react-navigation'

import {search, ERROR, SEARCH_SUCCESS} from '../../actions/espelho-empreendimento'

import HeaderSearch from './HeaderSearch'

import store from '../../store'

class HeaderSearchEspelho extends React.Component {

    componentDidMount() {
        this.unsubscribe = store.subscribe(({type}) => {
            const { routeName } = this.props.navigation.state
            if ((type === SEARCH_SUCCESS || type === ERROR) && routeName !== 'EspelhoSearch') {
                this.props.navigation.navigate('EspelhoSearch')
            }
        })
    }

    componentWillUnmount() {
        this.unsubscribe()
    }

    render() {
        return (
            <HeaderSearch
                placeholder='Pesquisa por Empreendimento(s)'
                value={this.props.query}
                onChangeText={query => { this.props.search({query, page: null}) }}
            />
        )
    }

}

const mapStateToProps = state => ({
    query: state.espelhoEmpreendimento.parameters.query
})

const mapDispatchToProps = dispatch => bindActionCreators({ search }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(HeaderSearchEspelho))
