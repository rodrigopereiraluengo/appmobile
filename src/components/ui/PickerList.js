import React from 'react'
import { TouchableOpacity } from 'react-native'
import { View, Container, Header, Title, Content, List, ListItem, Text, Left, Body, Icon } from 'native-base'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { PickerListStyle as style, HeaderDefaultStyle } from '../../styles'
import LeftArrow from './LeftArrow'
import { withNavigation } from 'react-navigation'
import StatusBarColor from './StatusBarColor'

import { select } from '../../actions/picker-list'

class PickerList extends React.Component {

    onSelect (value) {
        this.props.select(value)
        this.props.onValueChange(value)
    }

    render () {
        
        return <View style={style.View}>
            <Container style={style.Container}>
                <Header>
                    <Left style={HeaderDefaultStyle.Left}>
                        <TouchableOpacity style={HeaderDefaultStyle.Button} activeOpacity={.85} onPress={ () => { this.props.navigation.goBack() } }>
                            <LeftArrow />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={style.Title}>{this.props.title}</Title>
                    </Body>
                </Header>
                <Content>
                    <List>
                        {this.props.items.map((item, i) => {
                            return <ListItem 
                                        key={i} 
                                        style={style.ListItem} 
                                        expanded button onPress={ () => { this.onSelect(item.value) } }>
                            <Text>{item.label}</Text>
                            { this.props.selectedValue === item.value && <Icon name='checkmark' style={style.Icon} /> }
                        </ListItem>
                        })}
                    </List>
                </Content>
            </Container>
            <StatusBarColor />
        </View>
    }

}

const mapStateToProps = state => ({
    title: state.pickerList.title,
    items: state.pickerList.items,
    selectedValue: state.pickerList.selectedValue,
    onValueChange: state.pickerList.onValueChange
})

const mapDispatchToProps = dispatch => bindActionCreators({
    select
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(PickerList))
