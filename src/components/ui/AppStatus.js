import React from 'react'
import { AppState } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { changeState } from '../../actions/system'
import { get as getSettings } from '../../actions/settings'
import { ThemeColors } from 'react-navigation'

class AppStatus extends React.Component {

  componentDidMount() {
    this.props.changeState(AppState.currentState)
    AppState.addEventListener('change', this.props.changeState)
    this.props.getSettings()
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.props.changeState)
  }

  render() {
    return <React.Fragment />
  }

}

const mapStateToProps = state => ({
  settings: state.settings.settings
})

const mapDispatchToProps = dispatch => bindActionCreators({
  changeState,
  getSettings
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AppStatus)
