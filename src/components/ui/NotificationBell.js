import React, { useEffect } from 'react'
import { NotificationBellStyle as style } from '../../styles'
import { Image, TouchableOpacity } from 'react-native'
import bell from '../../../assets/images/bell.png'
import { Badge, Text } from 'native-base'
import { bindActionCreators } from 'redux'
import { total_notifications, NOTIFICATION_ACTION } from '../../actions/user'
import { connect } from 'react-redux'
import { withNavigation } from 'react-navigation'
import store from '../../store'

function NotificationBell ({ total_notifications, total, navigation, authenticated }) {

    useEffect(() => {

        if (authenticated) {
            total_notifications()
        }
        
        return store.subscribe(action => {
            if (action.type === NOTIFICATION_ACTION) {
                total_notifications()
            }
        },[total_notifications])
    })

    const onPress = () => navigation.navigate('NotificationSearch')

    if (!authenticated) {
        return null
    }

    return <TouchableOpacity style={style.TouchableOpacity} onPress={onPress}>
        <Image source={bell} style={style.ImageBell} />
        { total > 0 && <Badge style={style.Badge}>
            <Text style={style.BadgeText}>{ total > 99 ? '99+' : total }</Text>
        </Badge> }
    </TouchableOpacity>
}

const mapStateToProps = state => ({
    total: state.user.total_notifications,
    authenticated: state.auth.authenticated
})

const mapDispatchToProps = dispatch => bindActionCreators({
    total_notifications
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(NotificationBell))