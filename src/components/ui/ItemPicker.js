import React from 'react'
import { Item } from 'native-base'
import PropTypes from 'prop-types'

import Picker from './Picker'

class ItemPicker extends React.Component { 

    constructor(props) {
        super(props)

        this.state = {
            pickerWidth: '100%'
        }
    }

    render () {
        return (
            <Item onLayout={event => { this.setState({pickerWidth: event.nativeEvent.layout.width}) }}
                regular
                picker
                invalidated={this.props.invalidated}>
                    
                <Picker {...this.props} style={{width: this.state.pickerWidth}} />

            </Item>
        )
    }
}

ItemPicker.defaultProps = {
    label: 'Selecione uma opção'
}

ItemPicker.propTypes = {
    label: PropTypes.string
}

export default ItemPicker
