import {Platform, StyleSheet} from 'react-native'

import {PLATFORM} from 'native-base/src/theme/variables/commonColor'
import Color from 'color'

import {
  darkColor,
  darkIntermediateColor,
  darkMonoColor,
  defaultGreen,
  defaultLeftRightMargin,
  defaultRed,
  defaultTopBottomMargin,
  fontFamily,
  fontFamilyBold,
  fontSize,
  fontSizeBadge,
  fontSizeBase,
  fontSizeSmall,
  intermediateMonoColor,
  lightColor,
  lightMonoColor,
  deviceHeight,
  deviceWidth,
  calcHeight,
  calcWidth,
  intermediateColor,
  lightIntermediateColor,

} from '../theme/corretoron'

import theme from '../theme/corretoron'

export const LoginStyle = StyleSheet.create({

  Content: {
    marginLeft: calcWidth(35),
    marginRight: calcWidth(35),
    paddingBottom: calcHeight(40)
  },

  TextBemVindo: {
    fontFamily: fontFamilyBold,
    fontSize: calcWidth(34),
    color: lightColor
  },

  TextFacaLogin: {
    fontSize: calcWidth(19),
    marginBottom: calcHeight(33),
    color: lightColor
  },

  ButtonEntrar: {
    height: calcHeight(55)
    
  },

  TextEntrar: {
    fontSize: calcWidth(24),
    paddingTop: Platform.OS === PLATFORM.IOS ? calcHeight(5) : calcHeight(3)
  },

  TextEsqueci: {

    fontStyle: 'italic',
    color: darkMonoColor,
    textAlign: 'center',
    marginTop: calcHeight(5),
    marginBottom: calcHeight(35)
  },

  ButtonCriar: {
    fontSize: calcWidth(20),
    fontFamily
  },

  TextCriar: {
    fontSize: calcWidth(20),
    fontFamily
  }

})


export const NewStyle = StyleSheet.create({

  termosFirstColumn: {
    width: calcWidth(40)
  },

  termosGrid: {
    marginTop: calcWidth(25),
    marginBottom: calcWidth(40)
  },

  termosLabel: {
    fontSize: calcWidth(11)
  }

});


export const HomeStyle = StyleSheet.create({

  TextTudo: {
    marginBottom: calcHeight(7)
  },

  ColLeft: {
    marginTop: calcHeight(7),
    marginRight: calcHeight(7),
    marginBottom: calcHeight(7),
  },

  ColRight: {
    marginTop: calcHeight(7),
    marginLeft: calcWidth(7),
    marginBottom: calcHeight(7),
  },

  Card: {
    paddingLeft: calcWidth(12),
    paddingRight: calcWidth(12),
    paddingTop: calcHeight(18),
    paddingBottom: calcHeight(6),
    maxHeight: calcHeight(200)
  },

  CardImage: {
    alignSelf: 'center',
    marginBottom: calcHeight(7)
  },

  CardButton: {
    margin: 0,
    padding: 0,
  },

  CardText: {
    fontSize: fontSize * calcWidth(1.2),
    alignSelf: 'center',
  },

  CardButtonText: {
    fontSize: fontSize * calcWidth(.8),
    margin: 0,
    paddingTop: PLATFORM.IOS === Platform.OS ? calcHeight(5) : 0,
    padding: 0,
    textTransform: 'uppercase'
  },

  CardTextSecondLine: {
    fontFamily: fontFamilyBold
  }

})


export const EmpreendimentoSearchStyle = StyleSheet.create({

  Card: {
    marginBottom: defaultTopBottomMargin * calcHeight(1.5)
  },

  Image: {
    flex: 1,
    height: calcHeight(100),
    resizeMode: 'cover'
  },

  ColLeft: {
    width: calcWidth(100)
  },

  ColDescricoes: {
    marginLeft: calcWidth(8)
  },

  TextNome: {
    fontFamily: fontFamilyBold,
    fontSize: fontSize * calcWidth(.9),
    marginTop: calcHeight(10),
    textTransform: 'uppercase'
  },

  ColIcon: {
    width: calcWidth(10)
  },

  IconCidadeEstado: {
    marginTop: calcHeight(5),
    fontSize,
  },

  TextCidadeEstado: {
    marginTop: calcHeight(5),
    fontSize: fontSizeSmall,
  },

  TextMetragem: {
    marginTop: calcHeight(10),
    fontSize: fontSizeSmall,
    fontFamily: fontFamilyBold,
    textTransform: 'uppercase'
  },

  LinkButtonTextStyle: {
    marginTop: calcHeight(15)
  },

  Button: {
    marginTop: calcHeight(5),
    marginBottom: calcHeight(5)
  },

  ButtonLeft: {
    marginLeft: calcWidth(10),
    marginRight: calcWidth(5)
  },

  ButtonRight: {
    marginRight: calcWidth(10),
    marginLeft: calcWidth(5)
  },

  ButtonTop: {
    marginTop: calcHeight(12),
  },

  ButtonBottom: {
    marginBottom: calcHeight(12)
  }

})


export const EmpreendimentoDetailStyle = StyleSheet.create({

  ColLeft: {
    width: '30%',
    paddingTop: calcHeight(4)
  },

  ColRight: {
    width: '70%',
    paddingTop: calcHeight(4)
  },

  TextDescription: {
    color: darkColor
  },

  GridButton: {
    marginTop: defaultTopBottomMargin
  },

  GridIncorporadora: {
    marginTop: defaultTopBottomMargin,
    borderTopWidth: 1,
    borderTopColor: intermediateMonoColor
  },

  MapView: {
    width: '100%',
    height: calcHeight(200)
  },

  ViewMore: {
    marginTop: calcHeight(5),
    fontFamily: fontFamilyBold,
    color: lightColor
  }

})


export const EmpreendimentoGallery = StyleSheet.create({

  View: {
    flex: 1,
    flexDirection: 'column',
    margin: 1
  },

  Icon: {
    color: intermediateColor,
    marginTop: calcHeight(10),
    marginLeft: calcWidth(10),
    fontSize: calcWidth(15),
    position: 'absolute',
    zIndex: 2,
    borderWidth: .5,
    backgroundColor: lightMonoColor,
    borderColor: intermediateColor,
    padding: calcWidth(4)
  },

  TouchableOpacity: {
    flex: 1
  },

  Image: {
    height: calcHeight(120),
    width: '100%',
  }

})


export const EspelhoSearchStyle = StyleSheet.create({

  ColRight: {
    width: '18%',
    justifyContent: 'center'
  },

  TextNome: {
    alignSelf: 'flex-start',
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBadge
  },

  TextUnidadesLivres: {
    alignSelf: 'flex-start',
    fontSize: fontSizeBase * calcWidth(1.3)
  },

  TextUnidadesDisponiveis: {
    alignSelf: 'flex-end',
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBase * calcWidth(2),
    textAlignVertical: 'bottom',
    color: defaultGreen
  },

  TextUnidadesNaoDisponiveis: {
    color: defaultRed
  }

})


export const EspelhoBlocoStyle = StyleSheet.create({

  TextEmpreendimentoNome: {
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBase * calcWidth(1.4)
  },

  TextUnidadesDisponivels: {
    fontSize: fontSizeBase * calcWidth(1.4),
    marginTop: defaultTopBottomMargin * calcHeight(.5)
  },

  ListItem: {
    backgroundColor: defaultGreen,
    marginTop: defaultTopBottomMargin * calcWidth(1.5),
    paddingLeft: defaultLeftRightMargin * calcHeight(.5)
  },

  ListItemUnidadesNaoDisponiveis: {
    backgroundColor: defaultRed,
  }

})


export const EspelhoUnidadesStyle = StyleSheet.create({

  Grid: {
    marginTop: defaultTopBottomMargin
  },

  TextEmpreendimentoNome: {
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBase * calcWidth(1.4)
  },

  TextUnidadesDisponivels: {
    fontSize: fontSizeBase * calcWidth(1.4),
    marginTop: defaultTopBottomMargin * calcHeight(.5)
  },

  GridUnidades: {
    marginTop: defaultTopBottomMargin * calcHeight(1.5),
  },

  ColUnidade: {
    paddingTop: defaultTopBottomMargin * calcHeight(1.5),
    paddingBottom: defaultTopBottomMargin * calcHeight(1.5),
    borderBottomWidth: 1,
    borderBottomColor: lightMonoColor
  },

  ColUnidadeNome: {
    paddingLeft: defaultLeftRightMargin * calcHeight(.5),
    width: calcWidth(50)
  },

  TextUnidade: {
    fontSize
  },

  ColUnidadeStatus: {
    paddingLeft: defaultLeftRightMargin * calcWidth(.5),
  },

  ColUnidadeArea: {
    paddingRight: defaultLeftRightMargin * calcWidth(.5),
    width: calcWidth(70)
  },

  TextUnidadeArea: {
    alignSelf: 'flex-end'
  }

})


export const ProfileStyle = StyleSheet.create({
  Thumbnail: {
    marginTop: calcHeight(25),
    marginBottom: calcHeight(30),
    alignSelf: 'center',
    backgroundColor: intermediateMonoColor
  }
})


export const FooterDefaultStyle = StyleSheet.create({

  Footer: {
    paddingLeft: calcWidth(25),
    paddingRight: calcWidth(25),
    paddingTop: calcHeight(25),
    paddingBottom: calcHeight(25),
    height: calcHeight(70)
  },

  ButtonRightWidth: {
    borderRightWidth: 2,
    borderRightColor: darkIntermediateColor
  },

  ButtonLeftWidth: {
    borderLeftWidth: 0,
    borderLeftColor: darkIntermediateColor
  },

  ImageProdutosStyle: {
    width: calcWidth(38),
    height: calcHeight(33)
  },

  ImageTabelasStyle: {
    width: calcWidth(29),
    height: calcHeight(33)
  },

  ImageEspelhoStyle: {
    width: calcWidth(36),
    height: calcHeight(27)
  },

  ImageAgendaStyle: {
    width: calcWidth(30),
    height: calcHeight(33)
  },

  ImageUserStyle: {
    width: calcWidth(24),
    height: calcHeight(27)
  }

})


export const AgendaStyle = StyleSheet.create({

  TextCompromissoDia: {
    alignSelf: 'center',
    marginBottom: defaultTopBottomMargin * calcHeight(2)
  },

  List: {
    marginLeft: calcWidth(-20)
  },

  ListItem: {
    backgroundColor: intermediateMonoColor,
    paddingTop: defaultTopBottomMargin,
    paddingBottom: defaultTopBottomMargin,
    paddingLeft: defaultLeftRightMargin,
    marginBottom: defaultTopBottomMargin
  },

  ViewLine: {
    borderTopWidth: 1,
    borderTopColor: intermediateMonoColor
  }

})


export const CompromissoStyle = StyleSheet.create({

  Button: {
    marginTop: calcHeight(5),
    marginBottom: calcWidth(5)
  },

  ButtonLeft: {
    marginLeft: calcWidth(10),
    marginRight: calcWidth(5)
  },

  ButtonRight: {
    marginRight: calcWidth(10),
    marginLeft: calcWidth(5)
  }

})


export const TabelaStyle = StyleSheet.create({

  Card: {
    marginBottom: defaultTopBottomMargin * calcHeight(1.5),
    paddingLeft: calcWidth(10),
    paddingRight: calcWidth(10),
    paddingBottom: calcHeight(10)
  },

  ColRight: {
    width: '30%'
  }

})


export const DefaultStyle = StyleSheet.create({

  SafeAreaView0: {
    flex: 0,
    backgroundColor: '#ffffff'
  },

  SafeAreaView1: {
    flex: 1,
  },

  SafeAreaView2: {
    backgroundColor: darkColor
  },

  SafeAreaViewLighten: {
    backgroundColor: '#ffffff'
  },

  SafeAreaViewLoading: {
    backgroundColor: '#829cad'
  },

  ViewScrollTop: {
    position: 'absolute', 
    width: '100%', 
    backgroundColor: lightColor, 
    zIndex: 1001
  },

  ScrollView: {
    paddingLeft: defaultLeftRightMargin,
    paddingRight: defaultLeftRightMargin,
  },

  View: {
    paddingBottom: calcHeight(40),
  }

})


export const ContatoStyle = StyleSheet.create({

  Fab: {
    backgroundColor: 'transparent',
    width: calcWidth(41),
    height: calcHeight(41),
    bottom: calcHeight(5)
  },

  containerStyle: {
    marginBottom: calcHeight(40)
  },

  TouchableOpacity: {
    marginBottom: PLATFORM.IOS === Platform.OS ? calcHeight(13) : 0
  },

  Thumbnail: {
    width: calcWidth(72),
    height: calcHeight(72),
  },

  Button: {
    width: calcWidth(41),
    height: calcHeight(41),
  },

  ButtonWhatsApp: {
    backgroundColor: '#34A34F'
  },

  ButtonPhone: {
    backgroundColor: '#4ACFFF'
  },

  FontAwesomeIcon: {
    color: lightMonoColor
  },

  ButtonEmail: {
    backgroundColor: '#DD5144'
  }

})


export const HeaderDefaultStyle = StyleSheet.create({

  Left: {
    paddingLeft: calcWidth(10),
    maxWidth: calcWidth(60)
  },

  Body: {
    alignItems: 'center'
  },

  Right: {
    paddingRight: calcWidth(0),
    maxWidth: calcWidth(60)
  },

  ImageHome: {
    height: calcHeight(20),
    width: calcWidth(20)
  },

  ImageAppOn: {
    height: calcHeight(60),
    width: calcWidth(115)
  },

  Button: {
    paddingTop: calcHeight(10),
    paddingBottom: calcHeight(10),
    paddingLeft: calcHeight(10),
    paddingRight: calcHeight(35),
  }

})

export const LeftArrowStyle = StyleSheet.create({
  
  Image: {
    height: calcHeight(20),
    width: calcWidth(20)
  }

})


export const FooterHomeStyle = StyleSheet.create({

  Footer: {
    height: calcHeight(80)
  },

  Body: {
    flexDirection: 'column',
    alignItems: 'center'
  },

  Text: {
    color: lightMonoColor,
    paddingRight: calcWidth(10),
    fontSize: fontSize * calcWidth(.8)
  },
  
  Image: {
    width: calcWidth(116),
    height: calcHeight(31)    
  }

})


export const HeaderHomeStyle = StyleSheet.create({

  Left: {
    height: calcHeight(50),
    maxWidth: calcWidth(65),
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: calcWidth(20)
  },

  Image: {
    height: calcHeight(60),
    width: calcWidth(115)
  },

  Body: {
    height: calcHeight(65),
    justifyContent: 'center',
    alignItems: 'center'
  },

  Text: {
    fontSize: calcWidth(9),
    alignSelf: 'flex-end',
    marginRight: calcWidth(20)
  }
})

export const NotificationBellStyle = StyleSheet.create({

  ImageBell: {
    height: calcHeight(22),
    width: calcWidth(19)
  },

  TouchableOpacity: {
    paddingRight: 13,
    paddingLeft: 13,
    paddingTop: 10,
    paddingBottom: 10
  },

  Badge: {
    width: calcHeight(24), 
    height: calcHeight(24), 
    position: 'absolute', 
    padding: 0
  },

  BadgeText: {
    width: calcHeight(24), 
    position: 'absolute',
    fontFamily: fontFamilyBold,
    paddingTop: PLATFORM.IOS === Platform.OS ? calcWidth(2.7) : 0,
    fontSize: PLATFORM.IOS === Platform.OS ? calcWidth(10) : calcWidth(9)
  }

})

export const HeaderSearchStyle = StyleSheet.create({

  Item: {
    height: calcHeight(36),
    paddingLeft: calcWidth(9),
    borderRadius: calcWidth(25),
    marginLeft: calcWidth(8),
    marginRight: calcWidth(8),
    marginBottom: calcHeight(25)
  },

  Image: {
    height: calcHeight(20),
    width: calcWidth(55)
  },

  Text: {
    fontSize: calcWidth(9)
  }

})


export const LoadingStyle = StyleSheet.create({

  View: {
    height: deviceHeight,
    width: '100%',
    right: 0,
    top: 0,
    position: 'absolute',
    backgroundColor: 'rgba('.concat(Color(darkColor).rgb().color.join(', '), ', 0.5)'),
    zIndex: 1012
  },

  Spinner: {
    color: lightMonoColor,
    marginTop: calcHeight(100)
  }

})

export const OffLineStyle = StyleSheet.create({

  View: {
    height: deviceHeight,
    width: deviceWidth,
    right: 0,
    top: 0,
    position: 'absolute',
    backgroundColor: darkColor,
    zIndex: 1,
    justifyContent: 'center'
  },

  Icon: {
    alignSelf: 'center',
    color: darkMonoColor
  },

  Text: {
    marginTop: defaultTopBottomMargin,
    textAlign: 'center',
    color: lightMonoColor
  }

})


export const RNPickerSelectStyle = StyleSheet.create({

  inputIOS: {

  },

  inputAndroid: {
    fontSize,
    fontFamily,
    //paddingHorizontal: calcWidth(10),
    //paddingVertical: calcHeight(8),
    color: darkColor,
    //paddingRight: calcWidth(30),
    //width: '60%'
  },

  AntDesign: {
    marginTop: calcHeight(15),
    marginRight: calcWidth(9)
  }

})

export const RNPickerSelectStyleAntDesignSize = calcWidth(10)

export const NBPickerStyle = StyleSheet.create({

  textStyle: { 
    color: darkMonoColor,
    fontSize,
    fontWeight: 'normal',
    marginTop: 0,
    marginLeft: calcWidth(8),
    paddingTop: 0
  },

  itemStyle: {
    backgroundColor: lightMonoColor,
    color: darkColor,
    marginLeft: 0,
    paddingLeft: calcWidth(10),
    paddingBottom: calcHeight(3),
    width: deviceWidth
  },

  itemTextStyle: { 
    color: darkColor, 
    borderWidth: 0, 
    marginTop: 0, 
    marginBottom: 0 
  },

  iosIcon: {
    right: calcWidth(-8),
    position: 'absolute', 
    color: darkColor
  },

  Body: { 
    flex: 3 
  },

  Title: { 
    color: darkMonoColor 
  }

})


export const ImageSlideShowStyle = StyleSheet.create({

  ScrollView: {
    height: calcWidth(200)
  },

  Col: {
    width: calcWidth(200),
    marginRight: calcWidth(10),
    height: calcHeight(150)
  },

  Image: {
    height: calcHeight(150),
    maxWidth: calcWidth(200)
  },

  View: {
    height: calcHeight(150),
    width: calcWidth(200),
    backgroundColor: intermediateMonoColor
  },

  Spinner: {
    color: intermediateColor,
    position: 'absolute',
    alignSelf: 'center'
  }

})


export const SplashStyle = StyleSheet.create({

  Image: {
    width: deviceWidth > 496 ? 496 : deviceWidth,
    resizeMode:'contain',
    alignSelf: 'center',
    marginTop: calcHeight(Platform.OS === PLATFORM.IOS ? 60 : 30),
    marginBottom: calcHeight(30),
    paddingBottom: calcHeight(10),
    flex: 1
  }
})


export const ImageBackgroundStyle = StyleSheet.create({

  Image: {
    position: 'absolute',
    top: calcHeight(210),
    left: calcHeight(-148),
    bottom: 0,
    right: 0,
    width: calcWidth(300),
    height: calcWidth(500),
    opacity: .3,
  },

  Top: {
    position: 'absolute',
    top: calcHeight(40),
    left: calcHeight(-35),
    bottom: 0,
    right: 0,
    width: calcWidth(110),
    height: calcWidth(250),
    opacity: .2
  }

})

export const PickerStyle = StyleSheet.create({

  TouchableOpacity: {
    width: '100%'
  },

  Text: {
    paddingLeft: theme['NativeBase.Item']['.regular']['NativeBase.Text']['.staticinput'].paddingLeft,
    paddingRight: theme['NativeBase.Item']['.regular']['NativeBase.Text']['.staticinput'].paddingRight,
    height: theme['NativeBase.Item']['.picker'].height,
    lineHeight: calcHeight(42)
  },

  Icon: {
    position: 'absolute',
    right: 0,
    lineHeight: calcHeight(42)
  }
  

})

export const PickerListStyle = StyleSheet.create({

  View: {
    ...StyleSheet.absoluteFillObject,
    width: deviceWidth,
    height: deviceHeight,
    zIndex: 3
  },

  Container: {
    width: '100%'
  },

  Title: {
    fontSize: fontSizeBadge,
    paddingTop: 3,
    color: darkColor
  },

  Icon: {
    position: 'absolute',
    right: 15,
    color: darkColor
  },

  ListItem: {
    paddingLeft: 10
  },

  Text: {

  },

})

export const NotificationSearchStyle = StyleSheet.create({

  ColIcon: {
    width: '4%',
    justifyContent: 'center'
  },

  Icon: {
    fontSize: 8,
    color: lightColor
  },

  ColRight: {
    width: '24%',
    justifyContent: 'center'
  },

  TextTitle: {
    alignSelf: 'flex-start',
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBadge
  },

  TextBody: {
    alignSelf: 'flex-start',
    fontSize: fontSizeBase * calcWidth(1.3)
  },

  TextDate: {
    alignSelf: 'flex-end',
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBase * calcWidth(1.3),
    textAlignVertical: 'bottom',
    color: darkMonoColor
  }

})
