import {Dimensions, Platform} from 'react-native'

import themeComponents from 'native-base/src/theme/components'
import commonColor, {PLATFORM} from 'native-base/src/theme/variables/commonColor'
import Color from 'color'

export const deviceWidth                        = Dimensions.get('window').width
export const deviceHeight                       = Dimensions.get("window").height

const fixedWidth                                = 411.42857142857144
const fixedHeight                               = 683.4285714285714

const widths                                    = {}

export function calcWidth (width) {

    if (deviceWidth > fixedWidth) {
        return width
    }

    let result = widths[width]
    if (typeof result === 'undefined') {
        const percent = deviceWidth / fixedWidth
        result = width * deviceWidth / fixedWidth
        widths[width] = result
    }

    return result
}

export function calcHeight (height) {
    return calcWidth(height)
}

export const darkColor                              = '#073B59'
export const darkIntermediateColor                  = Color(darkColor).darken(.15).hex()
export const lightColor                             = '#85BBBA'
export const lightIntermediateColor                 = Color(lightColor).darken(.15).hex()//'#2d6775'
export const intermediateColor                      = Color(lightColor).darken(.25).hex()// '#0f59ee'

export const darkMonoColor                          = '#434051'
export const intermediateMonoColor                  = '#efefef'
export const lightMonoColor                         = '#ffffff'

export const defaultGreen                           = '#1ced7f'
export const defaultRed                             = '#ed2a32'

export const calendarYellow                         = '#f1c731'

export const inputPlaceholderTextColor              = darkMonoColor
export const darkInputPlaceholderTextColor          = intermediateMonoColor
export const darkInputPlaceholderDarkerTextColor    = lightIntermediateColor

export const defaultRadius                          = calcWidth(8)
export const defaultLeftRightMargin                 = calcWidth(23)
export const defaultTopBottomMargin                 = calcHeight(9)

export const fontFamily                             = 'Axiforma'
export const fontFamilyBold                         = 'Axiforma_bold'
export const fontSizeSmall                          = calcWidth(9.5)
export const fontSize                               = calcWidth(11.5)
export const fontSizeBase                           = calcWidth(10.5)
export const fontSizeBadge                          = calcWidth(16.5)

export const styleColumnFirst = {
    paddingRight: calcWidth(15)
}

export const styleColumnSecond = {
    paddingLeft: calcWidth(15)
}

const customCommomColor = {
    ...commonColor,

    fontFamily,
    DefaultFontSize: fontSize,
    fontSizeBase,
    titleFontfamily: fontFamilyBold,
    buttonFontFamily: fontFamilyBold,

    containerBgColor: lightMonoColor,
    textColor: darkMonoColor,
    footerDefaultBg: darkColor,
    iconStyle: lightMonoColor,
    tabBarTextColor: intermediateMonoColor,
    inverseTextColor: intermediateMonoColor,
    radioBtnLineHeight: calcHeight(30),
    radioBtnSize: calcWidth(30),
    radioColor: darkColor
}

const theme = themeComponents(customCommomColor)


// Containers
theme['NativeBase.Container']['.dark'] = {
    backgroundColor: darkColor,
    'NativeBase.Content': {
        'NativeBase.Text': {
            color: lightMonoColor
        }
    }
}


// Contents
theme['NativeBase.Content'].paddingLeft = defaultLeftRightMargin
theme['NativeBase.Content'].paddingRight = defaultLeftRightMargin


// Header
theme['NativeBase.Header'].borderColor = darkColor
theme['NativeBase.Header'].borderBottomWidth = null
theme['NativeBase.Header'].shadowOffset = null
theme['NativeBase.Header'].shadowOpacity = null
theme['NativeBase.Header'].elevation = null


// Text
theme['NativeBase.Text']['.title'] = {
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBadge,
    textTransform: 'uppercase',
    marginTop: defaultTopBottomMargin * calcHeight(2),
}

theme['NativeBase.Text']['.subtitle'] = {
    fontFamily: fontFamilyBold,
    fontSize: fontSize,
    textTransform: 'uppercase',
    marginTop: defaultTopBottomMargin * calcHeight(2),
    marginBottom: defaultTopBottomMargin,
    color: darkColor
}

theme['NativeBase.Text']['.separator'] = {
    marginTop: defaultTopBottomMargin * calcHeight(2),
    marginBottom: defaultTopBottomMargin,
    color: darkColor,
    fontFamily: fontFamilyBold,
    fontSize: fontSizeBadge
}

theme['NativeBase.Text']['.linkButton'] = {
    marginTop: defaultTopBottomMargin * calcHeight(2),
    marginBottom: defaultTopBottomMargin,
    fontFamily: fontFamilyBold,
    color: darkColor
}

theme['NativeBase.Text']['.registerNotFound'] = {
    marginTop: defaultTopBottomMargin,
    marginBottom: defaultTopBottomMargin,
    fontFamily: fontFamilyBold,
    alignSelf: 'center'
}


// Items
theme['NativeBase.Item'].marginTop = defaultTopBottomMargin
theme['NativeBase.Item'].marginBottom = defaultTopBottomMargin

theme['NativeBase.Item']['.regular'].borderColor = 'transparent'
theme['NativeBase.Item']['.regular'].borderWidth = 1
theme['NativeBase.Item']['.regular'].marginTop = defaultTopBottomMargin / calcHeight(2)

theme['NativeBase.Item']['NativeBase.Input'].fontFamily = fontFamily
theme['NativeBase.Item']['NativeBase.Input'].fontSize = fontSize
theme['NativeBase.Item']['NativeBase.Input'].color = darkMonoColor

theme['NativeBase.Item']['.regular']['NativeBase.Input'].backgroundColor = intermediateMonoColor
theme['NativeBase.Item']['.regular']['NativeBase.Input'].height = calcHeight(40)
theme['NativeBase.Item']['.invalidated'] = {
    borderTopColor: defaultRed
}
theme['NativeBase.Item']['.regular']['NativeBase.Text'] = {
    '.staticinput' : {
        backgroundColor: intermediateMonoColor,
        height: calcHeight(40),
        flex: 1,
        paddingLeft: calcWidth(8),
        paddingRight: calcWidth(8),
        lineHeight: commonColor.inputLineHeight,
        paddingTop: calcHeight(8)
    }
}

theme['NativeBase.Input']['.multiline'].minHeight = calcHeight(80)

theme['NativeBase.Item']['.rounded'].borderRadius = defaultRadius
theme['NativeBase.Item']['.rounded'].borderWidth = 1
theme['NativeBase.Item']['.rounded'].borderColor = 'transparent'

theme['NativeBase.Item']['.dark'] = {
    backgroundColor: darkIntermediateColor,
    borderWidth: 1,
    borderColor: darkIntermediateColor,
    'NativeBase.Input': {
        color: lightMonoColor,
        backgroundColor: 'transparent'
    },

    'NativeBase.Icon': {
        color: lightMonoColor
    }
}

theme['NativeBase.Item']['.green'] = {
    backgroundColor: lightColor,
    borderWidth: 1,
    borderColor: lightColor,
    'NativeBase.Input': {
        color: lightMonoColor,
        backgroundColor: 'transparent'
    },

    'NativeBase.Icon': {
        color: lightMonoColor
    }
}

theme['NativeBase.Item']['.picker'] = {
    backgroundColor: intermediateMonoColor,
    borderColor: 'transparent',
    height: calcHeight(42),
    'NativeBase.Button': {
        color: 'red'
    }
}


// Forms
theme['NativeBase.Form']['NativeBase.Label'] = {
    fontFamily: fontFamilyBold,
    fontSize,
    color: darkColor,
    paddingTop: defaultTopBottomMargin,
    paddingLeft: defaultTopBottomMargin
}


// Picker
theme['NativeBase.Button']['.picker'] = {
    borderWidth: 0,
    marginTop: 0,
    marginBottom: 0,
    'NativeBase.Text': {
        fontWeight: 'normal',
        fontFamily
    }
}

// Footer
theme['NativeBase.Footer']['NativeBase.Text'] = {
    color: lightMonoColor,
}

theme['NativeBase.FooterTab']['NativeBase.Button'].borderRadius = null
theme['NativeBase.FooterTab']['NativeBase.Button'].height = calcHeight(44)

// Card
theme['NativeBase.Card'].backgroundColor = intermediateMonoColor
theme['NativeBase.Card'].borderColor = intermediateMonoColor
theme['NativeBase.Card'].shadowColor = null
theme['NativeBase.Card'].shadowOffset = null
theme['NativeBase.Card'].shadowOpacity = null
theme['NativeBase.Card'].elevation = null

// Buttons
theme['NativeBase.Button'].borderRadius = defaultRadius
theme['NativeBase.Button'].backgroundColor = lightColor
theme['NativeBase.Button'].marginTop = defaultTopBottomMargin * calcHeight(1.3)
theme['NativeBase.Button'].marginBottom = defaultTopBottomMargin * calcHeight(1.3)
theme['NativeBase.Button'].alignSelf = 'center'

theme['NativeBase.Button']['NativeBase.Text'] = {
    color: lightMonoColor,
    fontFamily: fontFamilyBold,
    marginLeft: defaultLeftRightMargin,
    marginRight: defaultLeftRightMargin,
    paddingTop: PLATFORM.IOS === Platform.OS ? calcHeight(5) : 0
}

theme['NativeBase.Button']['.small']['NativeBase.Text'].paddingTop = PLATFORM.IOS === Platform.OS ? calcHeight(5) : 0

theme['NativeBase.Button']['.dark'] = {
    backgroundColor: darkColor,
    borderWidth: 1,
    'NativeBase.Text': {
        color: lightColor,
        fontFamily: fontFamilyBold
    },
    borderColor: lightColor
}

theme['NativeBase.Button']['.transparent'] = {
    backgroundColor: 'transparent',
    borderWidth: 1,
    'NativeBase.Text': {
        color: lightColor,
        fontFamily: fontFamilyBold
    },
    borderColor: lightColor,
    shadowColor: null,
    shadowOffset: null,
    shadowOpacity: null,
    elevation: null
}

theme['NativeBase.Button']['.small']['NativeBase.Text'].fontSize = fontSize * calcWidth(.9)

theme['NativeBase.Button']['.small']['NativeBase.Text'].marginLeft = 0
theme['NativeBase.Button']['.small']['NativeBase.Text'].marginRight = 0


// Header
theme['NativeBase.Header'].backgroundColor = 'transparent'
theme['NativeBase.Header']['NativeBase.Body']['NativeBase.Text'] = {
    color: lightMonoColor
}
theme['NativeBase.Header']['NativeBase.Body']['NativeBase.Title'] = {
    color: lightMonoColor,
    paddingTop: calcHeight(9)
}

//search



// List Item
theme['NativeBase.ListItem']['.expanded'] = {
    marginLeft: 0,
    paddingRight: 0
}

theme['NativeBase.ListItem']['NativeBase.Text'].color = darkMonoColor

theme['NativeBase.Picker.Item.Text'] = {
    color: defaultRed,
    fontFamily: fontFamilyBold
}

theme['NativeBase.CheckBox'].color = 'red'

export default theme
