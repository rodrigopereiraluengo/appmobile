import { createStore, applyMiddleware } from 'redux'
import { persistReducer, persistStore } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'
import thunk from 'redux-thunk'
import _ from 'lodash'

import axios from '../resources/axios'
import reducers from '../reducers'

let storeAction
const storeMiddleware = store => {
    return next => action => {

        storeAction = action
        if (action.type === 'persist/REHYDRATE') {

            if (_.get(action.payload, 'auth.auth.token')) {
                axios.defaults.headers.common['Authorization'] = action.payload.auth.auth.token
            }

            axios.store = store
        }

        // Call the next dispatch method in the middleware chain.
        const nextAction = next(action)

        // This will likely be the action itself, unless
        // a middleware further in chain changed it.
        return nextAction
    }
}

const persistConfig = {
    mergeLevel: 3,
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['auth']
}

const persistedReducer = persistReducer(persistConfig, reducers)

const store = createStore(
    persistedReducer,
    applyMiddleware(
        thunk,
        storeMiddleware
    )
)

const storeSubscribe = store.subscribe
store.subscribe = callable => storeSubscribe(() => {
    return callable(storeAction)
})

export const persistor = persistStore(store)

export default store
