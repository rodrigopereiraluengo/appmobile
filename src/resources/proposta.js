
import axios from './axios'

export default class {

    static END_POINT = 'proposta'

    static post(data) {
        return axios.post(this.END_POINT, data)
    }

}
