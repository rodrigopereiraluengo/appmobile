import {Platform} from 'react-native'
import axios from './axios'
import _ from 'lodash'

export const ACAO_PLANTAS           = 'PLANTAS'
export const ACAO_TOUR_VIRTUAL      = 'TOUR_VIRTUAL'
export const ACAO_PAPER             = 'PAPER'
export const ACAO_EMAIL_MKT         = 'EMAIL_MKT'
export const ACAO_TABELA            = 'TABELA'
export const ACAO_ASSESSORIAS       = 'ASSESSORIAS'
export const ACAO_REGULAMENTOS      = 'REGULAMENTOS'
export const ACAO_FOLHETOS          = 'FOLHETOS'
export const ACAO_DOCUMENTO         = 'DOCUMENTO'
export const ACAO_AGENDAR_VISITA    = 'AGENDAR_VISITA'
export const ACAO_PROPOSTA          = 'PROPOSTA'
export const ACAO_FOTOS             = 'FOTOS'
export const ACAO_VISUALIZAR        = 'VISUALIZAR'
export const ACAO_ESPELHO           = 'ESPELHO'
export const ACAO_CADASTRAR         = 'CADASTRAR'

export default class {

    static END_POINT = 'acesso'

    static post(data) {
        data.app = true
        _.set(data, 'metadata.operating_system', Platform.OS)
        return axios.post(this.END_POINT, data)
    }

    static cadastrar(data) {
        data.app = true
        _.set(data, 'metadata.operating_system', Platform.OS)
        return axios.post(this.END_POINT.concat('/cadastrar'), data)
    }

}
