
import axios from './axios'

export default class {

  static END_POINT = 'auth'

  static post(data) {
    return axios.post(this.END_POINT, data)
  }

  static delete() {
    return axios.delete(this.END_POINT)
  }

  static forgotPassword(data) {
    return axios.post(this.END_POINT.concat('/forgot-password'), data)
  }

}
