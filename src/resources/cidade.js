
import axios from './axios'

export default class {

    static END_POINT = 'cidade'

    static search() {
        return axios.get(this.END_POINT)
    }

}
