import axios from './axios';

const END_POINT = '/settings';

export default class {

  static get() {
    return axios.get(END_POINT);
  }

}