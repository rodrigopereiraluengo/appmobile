const queryString = require('query-string')

import axios from './axios'

export default class {

  static END_POINT = 'content'

  static index(data = {}) {
      return axios.get(this.END_POINT.concat('?', queryString.stringify(data)))
  }

  static historico_sht() {
      return axios.get(this.END_POINT.concat('/historico-sht'))
  }
    
  static termos_uso() {
    return axios.get(this.END_POINT.concat('/termos-uso'))
  }

}
