import axios from './axios'

export default class {

    static END_POINT = 'endereco'

    static cep(cep) {
        return axios.get(this.END_POINT.concat('/cep/', cep))
    }
}
