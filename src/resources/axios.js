import axios from 'axios';
import NetInfo from "@react-native-community/netinfo";

import HttpStatus from '../enums/HttpStatus'

import Navigation from '../services/Navigation'

import Alert from '../components/ui/Alert'

const instance = axios.create({
  baseURL: 'http://v2.corretoron.com.br/'
})

instance.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
instance.defaults.headers.common['Accept'] = 'application/json';
instance.defaults.headers.common['project'] = 'COMPANY NAME';

let toHttpRequestEnd;

/**
 * Request Interceptor
 */
instance.interceptors.request.use(async request => {

  const connectionInfo = await NetInfo.fetch();

  if ('store' in instance) {

    if (!connectionInfo.isConnected) {
      instance.store.dispatch({ type: 'SYSTEM_OFFLINE' });
      return null
    }

    const ignoreUrls = [
      'user/position',
      'user/notification_token',
      'user/notification_receive',
      'user/total_notifications',
      'user/notification_action',
      'settings'
    ]

    if (ignoreUrls.includes(request.url) === false) {
      clearTimeout(toHttpRequestEnd);
      instance.store.dispatch({ type: 'HTTP_REQUEST_BEGIN', payload: request })
    }
  }

  return request
});



/**
 * Response Interceptor
 */
instance.interceptors.response.use(function (response) {

  if ('store' in instance) {
    toHttpRequestEnd = setTimeout(() => {
      instance.store.dispatch({ type: 'HTTP_REQUEST_END', payload: response })
    }, 200)
  }

  return response;

}, async function (error) {

  console.log(error)

  // Verify is http error
  if (error.response) {

    if (error.response.status === HttpStatus.UNAUTHORIZED && 'store' in instance) {
      instance.store.dispatch({ type: 'AUTH_UNAUTHORIZED', payload: error.response });
      Navigation.navigate('Login');
      Alert.error('Acesso não autorizado.')
    }

  } else if (error.request) {

  } else {

  }

  if ('store' in instance) {
    toHttpRequestEnd = setTimeout(() => instance.store.dispatch({ type: 'HTTP_REQUEST_END', payload: error }), 400)
  }

  if (axios.isCancel(error)) {
    console.log('isCancel');
  }

  // Do something with request error
  return Promise.reject(error);
});

export default instance;
