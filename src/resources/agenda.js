const queryString = require('query-string')

import axios from './axios'

export default class {

    static END_POINT = 'agenda'

    static dias(date) {
        return axios.get(this.END_POINT.concat('/dias?date=', date))
    }

    static search(data = {}) {
        return axios.get(this.END_POINT.concat('?', queryString.stringify(data)))
    }

    static post(data) {
        return axios.post(this.END_POINT, data)
    }

    static put(id, data) {
        return axios.put(this.END_POINT.concat('/', id), data)
    }

    static delete(id) {
        return axios.delete(this.END_POINT.concat('/', id))
    }

}
