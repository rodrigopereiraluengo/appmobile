export default {
    data: [],
    meta: {
        current_page: 0,
        from: 0,
        last_page: 0,
        path: null,
        per_page: 0,
        to: 0,
        total: 0
    },
    links: {
        first: null,
        last: null,
        prev: null,
        next: null
    }
}
