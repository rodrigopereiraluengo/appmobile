import axios from './axios'

export default class {

    static END_POINT = 'espelho-bloco'

    static get(id) {
        return axios.get(this.END_POINT.concat('/', id))
    }

}
