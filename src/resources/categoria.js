
import axios from './axios'

export default class {

    static END_POINT = 'categoria'

    static search() {
        return axios.get(this.END_POINT)
    }

}
