
import axios from './axios'

export default class {

    static END_POINT = 'regioes'

    static get() {
        return axios.get(this.END_POINT)
    }
}
