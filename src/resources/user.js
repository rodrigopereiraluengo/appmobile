import { stringify } from 'query-string'
import axios from './axios'

export default class {

    static END_POINT = 'user'

    static post(data) {
        return axios.post(this.END_POINT, data)
    }

    static put(id, data) {
        return axios.put(this.END_POINT.concat('/', id), data)
    }

    static get(id) {
        return axios.get(this.END_POINT.concat('/', id))
    }

    static imobiliarias(data) {
        return axios.get(this.END_POINT.concat('/imobiliarias?'.concat(stringify(data))))
    }

    static position(data) {
        return axios.put(this.END_POINT.concat('/position'), data)
    }

    static notification_token(data) {
        return axios.put(this.END_POINT.concat('/notification_token'), data)
    }

    static notification_receive(data) {
        return axios.post(this.END_POINT.concat('/notification_receive'), data)
    }

    static notification_action(data) {
        return axios.post(this.END_POINT.concat('/notification_action'), data)
    }

    static notifications(data) {
        return axios.get(this.END_POINT.concat('/notifications'), {
            params: data
        })
    }

    static total_notifications() {
        return axios.get(this.END_POINT.concat('/total_notifications'))
    }

    static regiao_arquivos() {
        return axios.get(this.END_POINT.concat('/regiao_arquivos'))
    }

}
