const queryString = require('query-string')

import axios from './axios'
import _axios from 'axios'

const CancelToken = _axios.CancelToken

let source = null

export default class {

    static END_POINT = 'empreendimento'

    static search(data = {}) {
        if(source) {
            source.cancel('Previous Request Cancel')
        }

        source = CancelToken.source()

        return axios.get(this.END_POINT.concat('?', queryString.stringify(data)), {
            cancelToken: source.token
        })
    }

    static get(id) {
        return axios.get(this.END_POINT.concat('/', id))
    }

    static filtros() {
        return axios.get(this.END_POINT.concat('/filtros'))
    }

    static empreendimentos() {
        return axios.get('/empreendimentos')
    }

    static tabelas(data) {
        return axios.get(this.END_POINT.concat('/tabelas?', queryString.stringify(data)))
    }
}
