
import _ from 'lodash'
import resource from '../resources/espelho-empreendimento'
import HttpStatus from '../enums/HttpStatus'

export const SEARCH                     = 'ESPELHO_EMPREENDIMENTO_SEARCH'
export const SEARCH_SUCCESS             = 'ESPELHO_EMPREENDIMENTO_SEARCH_SUCCESS'
export const ERROR                      = 'ESPELHO_EMPREENDIMENTO_ERROR'
export const CHANGED_PARAMETERS         = 'ESPELHO_EMPREENDIMENTO_CHANGED_PARAMETERS'
export const SELECT                     = 'ESPELHO_EMPREENDIMENTO_SELECT'

export const changedParameters = payload => ({type: CHANGED_PARAMETERS, payload})

let toSearch

export const search = payload => dispatch => {

    dispatch({type: SEARCH, payload})

    clearTimeout(toSearch)
    toSearch = setTimeout(() => {
        resource.search(payload)
            .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
            .catch(error => {
                if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                    dispatch({type: ERROR, payload: error.response.data})
                }
            })
    }, 500)
}

export const select = payload => dispatch => dispatch({type: SELECT, payload})
