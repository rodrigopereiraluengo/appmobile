import _ from 'lodash'
import resource from '../resources/endereco'
import HttpStatus from '../enums/HttpStatus'

export const CEP_SEARCH_SUCCESS = 'ENDERECO_CEP_SEARCH_SUCCESS'
export const ERROR              = 'ENDERECO_ERROR'

export const cep = payload => dispatch => {

    resource.cep(payload)
        .then(response => dispatch({type: CEP_SEARCH_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}
