import _ from 'lodash'

import HttpStatus from '../enums/HttpStatus'

import resource from '../resources/visita'
import acessoResource, { ACAO_AGENDAR_VISITA } from '../resources/acesso'
import Navigation from '../services/Navigation'

export const ADD            = 'VISITA_ADD'
export const CHANGED        = 'VISITA_CHANGED'
export const SAVE           = 'VISITA_SAVE'
export const SAVE_SUCCESS   = 'VISITA_SAVE_SUCCESS'
export const ERROR          = 'VISITA_ERROR'

export const add = payload => dispatch => {
    dispatch({type: ADD, payload})
    Navigation.navigate('Visita')
}

export const changed = payload => ({type: CHANGED, payload})

export const save = payload => dispatch => {

    dispatch({type: SAVE, payload})

    resource
        .post(payload)
        .then(response => {
            acessoResource.post({
                acao: ACAO_AGENDAR_VISITA,
                imovel_id: payload.empreendimento_id
            })
            dispatch({type: SAVE_SUCCESS, payload: response.data})
        })
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}
