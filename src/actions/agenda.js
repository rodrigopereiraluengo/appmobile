import _ from 'lodash'

import resource from '../resources/agenda'
import HttpStatus from '../enums/HttpStatus'

export const SEARCH         = 'AGENDA_SEARCH'
export const SEARCH_SUCCESS = 'AGENDA_SEARCH_SUCCESS'

export const DIAS           = 'AGENDA_DIAS'
export const DIAS_SUCCESS   = 'AGENDA_DIAS_SUCCESS'

export const ADD            = 'AGENDA_ADD'
export const EDIT           = 'AGENDA_EDIT'

export const SAVE           = 'AGENDA_SAVE'
export const SAVE_SUCCESS   = 'AGENDA_SAVE_SUCCESS'

export const CHANGED        = 'AGENDA_CHANGED'
export const ERROR          = 'AGENDA_ERROR'

export const REMOVE_SUCCESS = 'AGENDA_REMOVE_SUCCESS'

export const dias = payload => dispatch => {

    dispatch({type: DIAS, payload})

    resource.dias(payload)
        .then(response => dispatch({type: DIAS_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })

}

export const search = payload => dispatch => {

    dispatch({type: SEARCH, payload})

    resource.search(payload)
        .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const add = () => ({type: ADD})

export const edit = payload => ({type: EDIT, payload})

export const changed = payload => ({type: CHANGED, payload})

export const save = payload => dispatch => {

    dispatch({type: SAVE, payload})

    const promise = payload.id === null ? resource.post(payload) : resource.put(payload.id, payload)

    promise
        .then(response => dispatch({type: SAVE_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const remove = payload => dispatch => {

    resource
        .delete(payload.id).then(() => dispatch({type: REMOVE_SUCCESS}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })

}
