import _ from 'lodash'

import resource from '../resources/empreendimento'

import acessoResource, { ACAO_VISUALIZAR } from '../resources/acesso'

import HttpStatus from '../enums/HttpStatus'
import Navigation from '../services/Navigation'

export const CHANGED                    = 'EMPREENDIMENTO_CHANGED'
export const CHANGED_PARAMETERS         = 'EMPREENDIMENTO_CHANGED_PARAMETERS'
export const SEARCH                     = 'EMPREENDIMENTO_SEARCH'
export const SEARCH_SUCCESS             = 'EMPREENDIMENTO_SEARCH_SUCCESS'
export const ERROR                      = 'EMPREENDIMENTO_ERROR'
export const SELECT                     = 'EMPREENDIMENTO_SELECT'
export const SELECT_SUCCESS             = 'EMPREENDIMENTO_SELECT_SUCCESS'
export const GALLERY                    = 'EMPREENDIMENTO_GALLERY'
export const FILTRO_SUCCESS             = 'EMPREENDIMENTO_FILTRO_SUCCESS'
export const EMPREENDIMENTOS_SUCCESS    = 'EMPREENDIMENTO_EMPREENDIMENTOS_SUCCESS'

export const SEARCH_TABELAS             = 'EMPREENDIMENTO_SEARCH_TABELAS'
export const SEARCH_TABELAS_SUCCESS     = 'EMPREENDIMENTO_SEARCH_TABELAS_SUCCESS'

export const changed = payload => ({type: CHANGED, payload})
export const changedParameters = payload => ({type: CHANGED_PARAMETERS, payload})

let toSearch

export const search = payload => dispatch => {

    dispatch({type: SEARCH, payload})

    clearTimeout(toSearch)
    toSearch = setTimeout(() => {
        resource.search(payload)
            .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
            .catch(error => {
                if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                    dispatch({type: ERROR, payload: error.response.data})
                }
            })
    }, 500)
}

export const select = payload => dispatch => {

    dispatch({type: SELECT, payload})

    resource.get(payload)
        .then(response => {
            dispatch({type: SELECT_SUCCESS, payload: response.data})
        })
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const detail = payload => dispatch => {

    resource.get(payload)
        .then(response => {

            acessoResource.post({
                acao: ACAO_VISUALIZAR,
                imovel_id: payload
            })

            dispatch({type: SELECT_SUCCESS, payload: response.data})
            Navigation.navigate('EmpreendimentoDetail')
        })
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}


export const gallery = payload => dispatch => {
    dispatch({type: GALLERY, payload})
    Navigation.navigate('EmpreendimentoGallery')
}


export const filtros = () => dispatch => {

    resource.filtros()
        .then(response => dispatch({type: FILTRO_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const empreendimentos = payload => dispatch => {

    resource.empreendimentos(payload)
        .then(response => dispatch({type: EMPREENDIMENTOS_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const tabelas = payload => dispatch => {

    dispatch({type: SEARCH_TABELAS, payload})

    clearTimeout(toSearch)
    toSearch = setTimeout(() => {
        resource.tabelas(payload)
            .then(response => dispatch({type: SEARCH_TABELAS_SUCCESS, payload: response.data}))
            .catch(error => {
                if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                    dispatch({type: ERROR, payload: error.response.data})
                }
            })
    }, 500)
}
