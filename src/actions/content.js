
import _ from 'lodash'

import resource from '../resources/content'

import HttpStatus from '../enums/HttpStatus'

export const SEARCH = 'CONTENT_SEARCH'
export const SEARCH_SUCCESS = 'CONTENT_SEARCH_SUCCESS'
export const HISTORICO_SHT = 'CONTENT_HISTORICO_SHT'
export const HISTORICO_SHT_SUCCESS = 'CONTENT_HISTORICO_SHT_SUCCESS'
export const TERMOS_USO = 'CONTENT_TERMOS_USO'
export const TERMOS_USO_SUCCESS = 'CONTENT_TERMOS_USO_SUCCESS'
export const ERROR = 'CONTENT_ERROR'

export const search = payload => dispatch => {

    dispatch({type: SEARCH, payload})

    resource.index(payload)
        .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const historico_sht = () => dispatch => {

    dispatch({type: HISTORICO_SHT})

    resource.historico_sht()
        .then(response => dispatch({type: HISTORICO_SHT_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })

}

export const termos_uso = () => dispatch => {
  resource.termos_uso()
    .then(response => dispatch({type: TERMOS_USO_SUCCESS, payload: response.data}))
    .catch(error => {
        if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
            dispatch({type: ERROR, payload: error.response.data})
        }
    })
}
