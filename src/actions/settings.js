import resource from '../resources/settings';
import HttpStatus from '../enums/HttpStatus';

export const GET_SUCCESS = 'SETTINGS_GET_SUCCESS';
export const ERROR = 'SETTINGS_ERROR';

export const get = () => dispatch => {

  resource
    .get()
    .then(response => dispatch({ type: GET_SUCCESS, payload: response.data }))
    .catch(error => {
      if (error?.response.status === HttpStatus.UNPROCESSABLE_ENTITY) {
        dispatch({ type: ERROR, payload: error.response.data })
      }
    })
}