import _ from 'lodash'

import { UPDATE_USER } from './auth'

import resource from '../resources/user'
import Navigation from '../services/Navigation'
import HttpStatus from '../enums/HttpStatus'

export const ADD                            = 'USER_ADD'
export const EDIT                           = 'USER_EDIT'
export const CHANGED                        = 'USER_CHANGED'
export const SAVE_SUCCESS                   = 'USER_SAVE_SUCCESS'
export const ERROR                          = 'USER_ERROR'
export const IMOBILIARIAS_SUCCESS           = 'USER_IMOBILIARIAS_SUCCESS'
export const POSITION                       = 'USER_POSITION'
export const POSITION_SUCCESS               = 'USER_POSITION_SUCCESS'

export const NOTIFICATIONS                  = 'USER_NOTIFICATIONS'
export const NOTIFICATIONS_SUCCESS          = 'USER_NOTIFICATIONS_SUCCESS'
export const NOTIFICATION_TOTAL             = 'USER_NOTIFICATION_TOTAL'
export const NOTIFICATION_TOTAL_SUCCESS     = 'USER_NOTIFICATION_TOTAL_SUCCESS'
export const NOTIFICATION_TOKEN             = 'USER_NOTIFICATION_TOKEN'
export const NOTIFICATION_TOKEN_SUCCESS     = 'USER_NOTIFICATION_TOKEN_SUCCESS'
export const NOTIFICATION_RECEIVE           = 'USER_NOTIFICATION_RECEIVE'
export const NOTIFICATION_RECEIVE_SUCCESS   = 'USER_NOTIFICATION_RECEIVE_SUCCESS'
export const NOTIFICATION_ACTION            = 'USER_NOTIFICATION_ACTION'
export const NOTIFICATION_ACTION_SUCCESS    = 'USER_NOTIFICATION_ACTION_SUCCESS'

export const REGIAO_ARQUIVOS                = 'USER_REGIAO_ARQUIVOS'
export const REGIAO_ARQUIVOS_SUCCESS        = 'USER_REGIAO_ARQUIVOS_SUCCESS'

export const add = payload => dispatch => {
    dispatch({type: ADD, payload})
    Navigation.navigate('NewProfile')
}

export const edit = payload => ({type: EDIT, payload})

export const changed = payload => ({type: CHANGED, payload})

export const save = payload => dispatch => {
    const promise = payload.id ? resource.put(payload.id, payload) : resource.post(payload)
    promise
        .then(response => {
            dispatch({type: SAVE_SUCCESS, payload: response.data})
            if (payload.id) {
                const user = response.data
                user.senha = payload.senha
                dispatch({type: UPDATE_USER, payload: user})
            }
        })
        .catch(error => {

            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const imobiliarias = payload => dispatch => {
    resource
        .imobiliarias(payload)
        .then(response => dispatch({type: IMOBILIARIAS_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const position = payload => dispatch => {

    dispatch({type: POSITION, payload})

    resource
        .position(payload)
        .then(() => dispatch({type: POSITION_SUCCESS}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const notification_token = payload => dispatch => {
    
    dispatch({type: NOTIFICATION_TOKEN, payload})

    resource
        .notification_token(payload)
        .then(() => dispatch({type: NOTIFICATION_TOKEN_SUCCESS}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const notification_receive = payload => dispatch => {
    
    dispatch({type: NOTIFICATION_RECEIVE, payload})
    resource
        .notification_receive(payload)
        .then(() => dispatch({type: NOTIFICATION_RECEIVE_SUCCESS}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

export const notification_action = payload => dispatch => {
    
    dispatch({type: NOTIFICATION_ACTION, payload})
    resource
        .notification_action(payload)
        .then(() => dispatch({type: NOTIFICATION_ACTION_SUCCESS}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}

let toNotifications
export const notifications = payload => dispatch => { 
    dispatch({type: NOTIFICATIONS, payload})
    clearTimeout(toNotifications)
    toNotifications = setTimeout(() => {
        resource.notifications(payload).then(response => {
            dispatch({type: NOTIFICATIONS_SUCCESS, payload: response.data})
        }).catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
    }, 500)
}

let toTotalNotifications
export const total_notifications = () => dispatch => {

    dispatch({type: NOTIFICATION_TOTAL})
    clearTimeout(toTotalNotifications)
    toTotalNotifications = setTimeout(() => {

        resource.total_notifications().then(response => {
            dispatch({type: NOTIFICATION_TOTAL_SUCCESS, payload: response.data})
        }).catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
    }, 500)
}

export const regiao_arquivos = () => dispatch => {

    dispatch({type: REGIAO_ARQUIVOS})

    resource.regiao_arquivos().then(response => {
        dispatch({type: REGIAO_ARQUIVOS_SUCCESS, payload: response.data})
    }).catch(error => {
        if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
            dispatch({type: ERROR, payload: error.response.data})
        }
    })

}
