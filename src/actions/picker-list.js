
import Navigation from '../services/Navigation'

export const SHOW   = 'PICKERLIST_SHOW'
export const SELECT = 'PICKERLIST_SELECT'

export const show = payload => dispatch => {
    dispatch({type: SHOW, payload})
    Navigation.navigate('PickerList')
}

export const select = payload => dispatch => {
    dispatch({type: SELECT, payload})
    Navigation.goBack()
}