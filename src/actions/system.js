
export const NAVIGATE           = 'SYSTEM_NAVIGATE'
export const SCROLL             = 'SYSTEM_SCROLL'
export const SCROLL_TOP         = 'SYSTEM_SCROLL_TOP'
export const OFFLINE            = 'SYSTEM_OFFLINE'
export const ONLINE             = 'SYSTEM_ONLINE'
export const HTTP_REQUEST_BEGIN = 'HTTP_REQUEST_BEGIN'
export const HTTP_REQUEST_END   = 'HTTP_REQUEST_END'
export const IMAGE_ONLOADSTART  = 'SYSTEM_IMAGE_ONLOADSTART'
export const IMAGE_ONLOADEND    = 'SYSTEM_IMAGE_ONLOADEND'
export const STATE_CHANGE       = 'SYSTEM_STATE_CHANGE'

export const scroll = payload => ({type: SCROLL, payload})
export const scrollTop = () => ({type: SCROLL_TOP})
export const navigate = payload => ({type: NAVIGATE, payload})
export const offline = () => ({type: OFFLINE})
export const online = () => ({type: ONLINE})
export const imgOnLoadStart = () => ({type: IMAGE_ONLOADSTART})
export const imgOnLoadEnd = () => ({type: IMAGE_ONLOADEND})
export const changeState = payload => ({type: STATE_CHANGE, payload})
