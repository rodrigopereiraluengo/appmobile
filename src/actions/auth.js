import _ from 'lodash'

import HttpStatus from '../enums/HttpStatus'

import axios from '../resources/axios'
import resource from '../resources/auth'

export const LOGIN                  = 'AUTH_LOGIN'
export const TOKEN_REFRESH          = 'AUTH_TOKEN_REFRESH'
export const CHANGED                = 'AUTH_CHANGED'
export const LOGIN_SUCCESS          = 'AUTH_LOGIN_SUCCESS'
export const ERROR                  = 'AUTH_ERROR'
export const UNAUTHORIZED           = 'AUTH_UNAUTHORIZED'
export const LOGOUT                 = 'AUTH_LOGOUT'
export const UPDATE_USER            = 'AUTH_UPDATE_USER'
export const RESET_PASSWORD         = 'AUTH_RESET_PASSWORD'
export const RESET_PASSWORD_SUCCESS = 'AUTH_RESET_PASSWORD_SUCCESS'

export const changed = payload => ({type: CHANGED, payload})

export const login = payload => {
    return dispatch => {

        dispatch({type: LOGIN, payload})

        resource.post(payload)
            .then(response => {
                axios.defaults.headers.common['Authorization'] = response.data.token
                dispatch({type: LOGIN_SUCCESS, payload: response.data})
            })
            .catch(error => {
                if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                    dispatch({type: ERROR, payload: error.response.data})
                }
            })
    }
}

export const logout = () => {
    return dispatch => {
        axios.defaults.headers.common['Authorization'] = null
        dispatch({type: LOGOUT})
    }
}

export const forgotPassword = payload => {
    return dispatch => {

        dispatch({type: RESET_PASSWORD, payload})

        resource.forgotPassword(payload)
            .then(() => dispatch({type: RESET_PASSWORD_SUCCESS}))
            .catch(error => {
                if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                    dispatch({type: ERROR, payload: error.response.data})
                }
            })

    }
}
