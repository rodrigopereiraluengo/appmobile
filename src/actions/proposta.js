import _ from 'lodash'

import HttpStatus from '../enums/HttpStatus'

import resource from '../resources/proposta'
import acessoResource, { ACAO_PROPOSTA } from '../resources/acesso'
import Navigation from '../services/Navigation'

export const ADD            = 'PROPOSTA_ADD'
export const CHANGED        = 'PROPOSTA_CHANGED'
export const SAVE           = 'PROPOSTA_SAVE'
export const SAVE_SUCCESS   = 'PROPOSTA_SAVE_SUCCESS'
export const ERROR          = 'PROPOSTA_ERROR'

export const add = payload => dispatch => {
    dispatch({type: ADD, payload})
    Navigation.navigate('Proposta')
}

export const changed = payload => ({type: CHANGED, payload})

export const save = payload => dispatch => {

    dispatch({type: SAVE, payload})

    resource
        .post(payload)
        .then(response => {
            acessoResource.post({
                acao: ACAO_PROPOSTA,
                imovel_id: payload.empreendimento_id
            })
            dispatch({type: SAVE_SUCCESS, payload: response.data})
        })
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}
