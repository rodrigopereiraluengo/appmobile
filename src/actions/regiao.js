import _ from 'lodash'
import resource from '../resources/regiao'
import HttpStatus from '../enums/HttpStatus'

export const SEARCH_SUCCESS     = 'REGIAO_SEARCH_SUCCESS'
export const ERROR              = 'REGIAO_ERROR'

export const search = () => dispatch => {

    resource
        .get()
        .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}
