
import _ from 'lodash'

import resource from '../resources/categoria'

import HttpStatus from '../enums/HttpStatus'

export const SEARCH = 'CATEGORIA_SEARCH'
export const SEARCH_SUCCESS = 'CATEGORIA_SEARCH_SUCCESS'
export const ERROR = 'CATEGORIA_ERROR'

export const search = payload => dispatch => {

    dispatch({type: SEARCH, payload})

    resource.search(payload)
        .then(response => dispatch({type: SEARCH_SUCCESS, payload: response.data}))
        .catch(error => {
            if (_.get(error, 'response.status') === HttpStatus.UNPROCESSABLE_ENTITY) {
                dispatch({type: ERROR, payload: error.response.data})
            }
        })
}
