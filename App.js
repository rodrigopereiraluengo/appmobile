import React, { useState, useEffect } from 'react'
import { AppLoading } from 'expo'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'

import App from './src/components/App'
import AsyncStorage from '@react-native-async-storage/async-storage'
import * as Notifications from 'expo-notifications'

Notifications.addNotificationResponseReceivedListener(async response => {
  await AsyncStorage.setItem('handle_notification', JSON.stringify(response.notification.request.content))
})

export default function () {

  const [isReady, setIsReady] = useState(false);

  useEffect( () => {

    (async () => {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Axiforma: require('./assets/fonts/Kastelov_Axiforma_Regular.ttf'),
      Axiforma_bold: require('./assets/fonts/Kastelov_Axiforma_Bold.ttf'),
      Axiforma_light: require('./assets/fonts/Kastelov_Axiforma_Light.ttf'),
      Axiforma_black: require('./assets/fonts/Kastelov_Axiforma_Black.ttf'),
      ...Ionicons.font,
    })
      setIsReady(true)
    })()

    return () => {
      //unsetNotificationHandler()
      //removeNotificationResponseReceivedListener()
    }
  })

  if (isReady) {
    return <App />
  }

  return <AppLoading />
}
